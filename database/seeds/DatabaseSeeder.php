<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);
        $this->call(PlanSeeder::class);
    }
}

class PlanSeeder extends Seeder
{

    public function run()
    {
        \App\Plan::create(['name' => 'صنایع']);
        \App\Plan::create(['name' => 'کشاورزی']);
        \App\Plan::create(['name' => 'دیزل ']);
        \App\Plan::create(['name' => 'ادارات']);
        \App\Plan::create(['name' => 'تجاری']);
        \App\Plan::create(['name' => 'CNG']);
    }
}
