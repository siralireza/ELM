<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('off_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable()->comment('آیدی کاربر');
            $table->unsignedBigInteger('off_table_id')->comment('جدول خاموشی');
            $table->foreign('off_table_id')->references('id')->on('off_tables')
                ->onDelete('cascade');
            $table->date('date')->comment('تاریخ');
            $table->time('start_time')->comment('ساعت شروع آیتم');
            $table->integer('predict_tavanir_request')->comment('پیش بینی درخواست توانیر');
            $table->integer('manage_consumption')->comment('مدیریت مصرف');
            $table->integer('allowed_off_value')->comment('مقدار خاموشی مجاز');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('off_items');
    }
}
