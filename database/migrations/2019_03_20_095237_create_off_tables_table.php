<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('off_tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable()->comment('آیدی کاربر');
            $table->text('description')->nullable()->comment('توضیحات');
            $table->date('start_date')->comment('تاریخ شروع خاموشی');
            $table->date('end_date')->comment('تاریخ پایان خاموشی');
            $table->time('start_time')->comment('ساعت شروع خاموشی');
            $table->time('end_time')->comment('ساعت پایان خاموشی');
            $table->unsignedSmallInteger('period')->comment('مدت زمان خاموشی');
            $table->enum('status',['active','deactive','applied','non_applied'])->comment('وضعیت جدول خاموشی');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('off_tables');
    }
}
