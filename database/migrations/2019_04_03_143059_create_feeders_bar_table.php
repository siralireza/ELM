<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedersBarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeders_bar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable()->comment('آیدی کاربر');
            $table->unsignedBigInteger('feeder_id')->comment('فیدر');
            $table->foreign('feeder_id')->references('id')
                ->on('feeders')->onDelete('cascade');
            $table->unsignedInteger('hour_1')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_2')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_3')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_4')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_5')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_6')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_7')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_8')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_9')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_10')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_11')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_12')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_13')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_14')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_15')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_16')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_17')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_18')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_19')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_20')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_21')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_22')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_23')->default(0)->comment('ساعت');
            $table->unsignedInteger('hour_24')->default(0)->comment('ساعت');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeders_bar');
    }
}
