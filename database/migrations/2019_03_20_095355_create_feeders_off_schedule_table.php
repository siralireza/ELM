<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedersOffScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeders_off_schedule', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('off_table_id')->comment('جدول خاموشی');
            $table->foreign('off_table_id')->references('id')->on('off_tables')
                ->onDelete('cascade');;
            $table->unsignedBigInteger('user_id')->nullable()->comment('آیدی کاربر');
            $table->unsignedBigInteger('feeder_id')->comment('فیدر');
            $table->foreign('feeder_id')->references('id')
                ->on('feeders')->onDelete('cascade');
            $table->date('date')->comment('تاریخ');
            $table->time('off_time_start')->comment('زمان شروع مشخص شده برای خاموشی فیدر');
            $table->time('off_time_end')->comment('زمان پایان مشخص شده برای خاموشی فیدر');
            $table->time('off_real_time')->nullable()->comment('زمان شروع واقعی برای خاموشی فیدر');
            $table->time('on_real_time')->nullable()->comment('زمان پایان واقعی برای خاموشی فیدر');
            $table->integer('off_period')->comment('مدت خاموشی');
            $table->integer('off_real_bar')->nullable()->comment('بار واقعی قطع');
            $table->integer('on_real_bar')->nullable()->comment('بار واقعی وصل');
            $table->text('description')->nullable()->comment('توضیحات');
            $table->enum('status', ['on', 'off'])->default('on')->comment('وضعیت فیدر');
            $table->boolean('allowed')->default(true)->nullable()->comment('مجاز برای خاموشی');
            $table->dateTime('last_off_time')->nullable()->comment('تاریخ و زمان خاموشی قبلی فیدر');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeders_log');
    }
}
