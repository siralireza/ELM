<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable()->comment('آیدی کاربر');
            $table->date('date')->comment('تاریخ');
            $table->time('time')->comment('ساعت');
            $table->integer('bar_peak_mw')->nullable()->comment('پیک بار mw');
            $table->integer('tavanir_request')->nullable()->comment('بار خاموشی درخواست توانیر');
            $table->integer('real_cons')->nullable()->comment('مقدار واقعی بار مصرف');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_info');
    }
}
