<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContributersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable()->comment('آیدی کاربر');
            $table->unsignedBigInteger('feeder_id')->comment('فیدر');
            $table->foreign('feeder_id')->references('id')
                ->on('feeders')->onDelete('cascade');
            $table->text('contributer')->nullable()->comment('نام مشترک');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contributers');
    }
}
