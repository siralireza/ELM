<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('global_id')->nullable()->comment('آیدی فیدر');
            $table->date('date')->comment('تاریخ ثبت ویرایش');
            $table->char('name',100)->comment('نام فیدر');
            $table->char('zone_name',100)->comment('نام منطقه');
            $table->unsignedBigInteger('zone_id')->comment('آی دی منطقه');
            $table->char('post',100)->comment('پست فوق توزیع');
            $table->mediumText('address')->nullable()->comment('آدرس');
            $table->mediumText('path',100)->nullable()->comment('مسیر تغذیه');
            $table->boolean('breakable')->comment('امکان قطع دارد');
            $table->enum('break_type',['none','both','no_limit','time_limit','maneuver'])->comment('نوع قطع');
            $table->mediumText('limit')->nullable()->comment('محدودیت');
            $table->mediumText('both')->nullable()->comment('محدودیت از نوع هردو');
            $table->text('description')->nullable()->comment('توضیحات');
            $table->text('guid')->nullable()->comment('برای سرویس');
            $table->enum('bar_type',['system','non_system'])->comment('نحوه بارگیری');
            $table->unsignedInteger('history_version')->comment('ورژن اطلاعات');
            $table->boolean('is_deleted')->default(false)->comment('حذف شده است');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeders');
    }
}
