<?php

namespace App;

class FeederLimitedPartners extends MotherModel
{
    protected $table = 'feeders_limited_partners';

    public function Feeder()
    {
        return $this->belongsTo(Feeder::class, 'feeder_id', 'id');
    }
}
