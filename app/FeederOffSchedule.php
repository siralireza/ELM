<?php

namespace App;


class FeederOffSchedule extends MotherModel
{
    protected $table = 'feeders_off_schedule';

    public function Feeder()
    {
        return $this->belongsTo(Feeder::class, 'feeder_id', 'id');
    }

    public function OffTable()
    {
        return $this->belongsTo(OffTable::class,'off_table_id','id');
    }
}
