<?php

namespace App;

class OffTable extends MotherModel
{
    public function OffItems()
    {
        return $this->hasMany(OffTableItem::class,'off_table_id','id');
    }

    public function FeedersOff()
    {
        return $this->hasMany(FeederOffSchedule::class);
    }
}
