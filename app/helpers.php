<?php

use App\OffTable;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;

function my_curl($url)
{
    // create curl resource
    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, $url);

    // set time limit for connection time out
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100); //timeout in seconds
    set_time_limit(10);

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function tmp_json()
{
    return '{"data":[{"post_id":93057,"post_name":2,"feeder_name":"رزرو","zone_id":843,"zone_name":"اداره برق رضویه","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93038,"post_name":1,"feeder_name":"کامياب","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93038,"post_name":1,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93038,"post_name":1,"feeder_name":"فرهنگ","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93038,"post_name":1,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93038,"post_name":1,"feeder_name":"فارابي","zone_id":842,"zone_name":"اموربرق ناحيه 2 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93038,"post_name":1,"feeder_name":"موزه","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93063,"post_name":2,"feeder_name":"رزرو","zone_id":835,"zone_name":"اموربرق ناحيه 5 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93063,"post_name":2,"feeder_name":"تنديس","zone_id":835,"zone_name":"اموربرق ناحيه 5 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93063,"post_name":2,"feeder_name":"رئوف","zone_id":835,"zone_name":"اموربرق ناحيه 5 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93063,"post_name":2,"feeder_name":"آفرين","zone_id":835,"zone_name":"اموربرق ناحيه 5 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93063,"post_name":2,"feeder_name":"طيب","zone_id":835,"zone_name":"اموربرق ناحيه 5 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93063,"post_name":2,"feeder_name":"صداقت","zone_id":835,"zone_name":"اموربرق ناحيه 5 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93014,"post_name":2,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93014,"post_name":2,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93014,"post_name":2,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93014,"post_name":2,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93014,"post_name":2,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93014,"post_name":2,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93053,"post_name":2,"feeder_name":"معارف","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93053,"post_name":2,"feeder_name":"رواق","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93053,"post_name":2,"feeder_name":"عابد","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93053,"post_name":2,"feeder_name":"توسل","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93053,"post_name":2,"feeder_name":"رزرو","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93053,"post_name":2,"feeder_name":"شاکر","zone_id":848,"zone_name":"اموربرق ناحيه 8 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":105317,"post_name":2,"feeder_name":"کيوان","zone_id":839,"zone_name":"اموربرق ناحيه 9 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":105317,"post_name":2,"feeder_name":"رزرو","zone_id":837,"zone_name":"اموربرق ناحيه 7 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":105317,"post_name":2,"feeder_name":"تاسيسات","zone_id":837,"zone_name":"اموربرق ناحيه 7 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":105317,"post_name":2,"feeder_name":"رزرو","zone_id":837,"zone_name":"اموربرق ناحيه 7 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":105317,"post_name":2,"feeder_name":"صبا","zone_id":837,"zone_name":"اموربرق ناحيه 7 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":105317,"post_name":2,"feeder_name":"نسيم","zone_id":839,"zone_name":"اموربرق ناحيه 9 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93029,"post_name":1,"feeder_name":"شوکت","zone_id":835,"zone_name":"اموربرق ناحيه 5 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""},{"post_id":93029,"post_name":1,"feeder_name":"سلیم","zone_id":835,"zone_name":"اموربرق ناحيه 5 مشهد","address":null,"feeder_bar":0,"sub_zone":"","path":""}],"total":634,"status":200}';
}

function tmp_json_long()
{
    return '{
"data": [
{
"guid": "{0034D09C-B5D3-4AF7-BD62-1E46B15C9B12}",
"post_id": 93038,
"post_name": 1,
"feeder_name": "فارابي",
"feeder_name": "فارابي",
"zone_id": 842,
"feeder_id": 105368,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0034D09C-B5D3-4AF7-BD62-1E46B15C9B12}",
"post_id": 93038,
"post_name": 1,
"feeder_name": "فرهنگ",
"zone_id": 848,
"feeder_id": 93096,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0034D09C-B5D3-4AF7-BD62-1E46B15C9B12}",
"post_id": 93038,
"post_name": 1,
"feeder_name": "موزه",
"zone_id": 848,
"feeder_id": 105373,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0034D09C-B5D3-4AF7-BD62-1E46B15C9B12}",
"post_id": 93038,
"post_name": 1,
"feeder_name": "کامياب",
"zone_id": 848,
"feeder_id": 105372,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0292FC46-8C8B-40D4-A3BE-23B6A473495D}",
"post_id": 93063,
"post_name": 2,
"feeder_name": "صداقت",
"zone_id": 835,
"feeder_id": 93349,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0292FC46-8C8B-40D4-A3BE-23B6A473495D}",
"post_id": 93063,
"post_name": 2,
"feeder_name": "رئوف",
"zone_id": 835,
"feeder_id": 93348,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0292FC46-8C8B-40D4-A3BE-23B6A473495D}",
"post_id": 93063,
"post_name": 2,
"feeder_name": "تنديس",
"zone_id": 835,
"feeder_id": 105658,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0292FC46-8C8B-40D4-A3BE-23B6A473495D}",
"post_id": 93063,
"post_name": 2,
"feeder_name": "طيب",
"zone_id": 835,
"feeder_id": 93344,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0292FC46-8C8B-40D4-A3BE-23B6A473495D}",
"post_id": 93063,
"post_name": 2,
"feeder_name": "آفرين",
"zone_id": 835,
"feeder_id": 93350,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{051192CC-4CDE-43C4-BA75-1232EB9E4584}",
"post_id": 93053,
"post_name": 2,
"feeder_name": "عابد",
"zone_id": 848,
"feeder_id": 105504,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{051192CC-4CDE-43C4-BA75-1232EB9E4584}",
"post_id": 93053,
"post_name": 2,
"feeder_name": "معارف",
"zone_id": 848,
"feeder_id": 93209,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{051192CC-4CDE-43C4-BA75-1232EB9E4584}",
"post_id": 93053,
"post_name": 2,
"feeder_name": "توسل",
"zone_id": 848,
"feeder_id": 93208,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{051192CC-4CDE-43C4-BA75-1232EB9E4584}",
"post_id": 93053,
"post_name": 2,
"feeder_name": "شاکر",
"zone_id": 848,
"feeder_id": 93205,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{051192CC-4CDE-43C4-BA75-1232EB9E4584}",
"post_id": 93053,
"post_name": 2,
"feeder_name": "رواق",
"zone_id": 848,
"feeder_id": 105505,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{05CAAE30-F180-40CD-AB7E-48D979620E4D}",
"post_id": 105317,
"post_name": 2,
"feeder_name": "کيوان",
"zone_id": 839,
"feeder_id": 93291,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{05CAAE30-F180-40CD-AB7E-48D979620E4D}",
"post_id": 105317,
"post_name": 2,
"feeder_name": "نسيم",
"zone_id": 839,
"feeder_id": 93287,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{05CAAE30-F180-40CD-AB7E-48D979620E4D}",
"post_id": 105317,
"post_name": 2,
"feeder_name": "صبا",
"zone_id": 837,
"feeder_id": 105596,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{05CAAE30-F180-40CD-AB7E-48D979620E4D}",
"post_id": 105317,
"post_name": 2,
"feeder_name": "تاسيسات",
"zone_id": 837,
"feeder_id": 105594,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{081A6C80-9BF6-4FB4-AD7B-A311AD853846}",
"post_id": 93029,
"post_name": 1,
"feeder_name": "شهداد",
"zone_id": 835,
"feeder_id": 93345,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{081A6C80-9BF6-4FB4-AD7B-A311AD853846}",
"post_id": 93029,
"post_name": 1,
"feeder_name": "کيميا",
"zone_id": 835,
"feeder_id": 105655,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{081A6C80-9BF6-4FB4-AD7B-A311AD853846}",
"post_id": 93029,
"post_name": 1,
"feeder_name": "شوکت",
"zone_id": 835,
"feeder_id": 93347,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{081A6C80-9BF6-4FB4-AD7B-A311AD853846}",
"post_id": 93029,
"post_name": 1,
"feeder_name": "سلیم",
"zone_id": 835,
"feeder_id": 93346,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{099470BB-90F8-42E5-A280-AB30A53752B4}",
"post_id": 105305,
"post_name": 1,
"feeder_name": "صفی آباد",
"zone_id": 839,
"feeder_id": 93138,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0A38BCFD-94FF-473A-9B3B-62FF3E873475}",
"post_id": 93022,
"post_name": 2,
"feeder_name": "طراح",
"zone_id": 842,
"feeder_id": 93295,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0A38BCFD-94FF-473A-9B3B-62FF3E873475}",
"post_id": 93022,
"post_name": 2,
"feeder_name": "نوين",
"zone_id": 842,
"feeder_id": 93296,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0A38BCFD-94FF-473A-9B3B-62FF3E873475}",
"post_id": 93022,
"post_name": 2,
"feeder_name": "ناوگان",
"zone_id": 842,
"feeder_id": 93294,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0A38BCFD-94FF-473A-9B3B-62FF3E873475}",
"post_id": 93022,
"post_name": 2,
"feeder_name": "مدائن",
"zone_id": 848,
"feeder_id": 105598,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0A38BCFD-94FF-473A-9B3B-62FF3E873475}",
"post_id": 93022,
"post_name": 2,
"feeder_name": "اقتصاد",
"zone_id": 842,
"feeder_id": 93297,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0F0E390A-6957-4A51-93E4-5DAEABCE5040}",
"post_id": 105336,
"post_name": 1,
"feeder_name": "خوارزمي",
"zone_id": 835,
"feeder_id": 105467,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0F0E390A-6957-4A51-93E4-5DAEABCE5040}",
"post_id": 105336,
"post_name": 1,
"feeder_name": "ميثاق",
"zone_id": 835,
"feeder_id": 93171,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0F0E390A-6957-4A51-93E4-5DAEABCE5040}",
"post_id": 105336,
"post_name": 1,
"feeder_name": "آرمان",
"zone_id": 835,
"feeder_id": 105463,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{0F0E390A-6957-4A51-93E4-5DAEABCE5040}",
"post_id": 105336,
"post_name": 1,
"feeder_name": "عارف",
"zone_id": 835,
"feeder_id": 93173,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{11D6E8FE-8312-4293-B8C1-E2DD59E86A0E}",
"post_id": 105304,
"post_name": 1,
"feeder_name": "کبود(گنبد پست)",
"zone_id": 853,
"feeder_id": 93214,
"zone_name": "توس",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12648645-D5B2-47CE-9ADE-06CD596A3DDB}",
"post_id": 93001,
"post_name": 2,
"feeder_name": "دماوند",
"zone_id": 842,
"feeder_id": 105586,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12648645-D5B2-47CE-9ADE-06CD596A3DDB}",
"post_id": 93001,
"post_name": 2,
"feeder_name": "ابوذر",
"zone_id": 842,
"feeder_id": 105588,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12648645-D5B2-47CE-9ADE-06CD596A3DDB}",
"post_id": 93001,
"post_name": 2,
"feeder_name": "بوتان",
"zone_id": 842,
"feeder_id": 93285,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12648645-D5B2-47CE-9ADE-06CD596A3DDB}",
"post_id": 93001,
"post_name": 2,
"feeder_name": "نيروي هوايي",
"zone_id": 842,
"feeder_id": 93282,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12648645-D5B2-47CE-9ADE-06CD596A3DDB}",
"post_id": 93001,
"post_name": 2,
"feeder_name": "خلج",
"zone_id": 842,
"feeder_id": 93283,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12A1BE76-5286-4503-BA4B-CC7DD4228963}",
"post_id": 93059,
"post_name": 2,
"feeder_name": "لشکر",
"zone_id": 842,
"feeder_id": 105612,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12A1BE76-5286-4503-BA4B-CC7DD4228963}",
"post_id": 93059,
"post_name": 2,
"feeder_name": "جاويد",
"zone_id": 842,
"feeder_id": 105614,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12A1BE76-5286-4503-BA4B-CC7DD4228963}",
"post_id": 93059,
"post_name": 2,
"feeder_name": "رضا",
"zone_id": 842,
"feeder_id": 105608,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12A1BE76-5286-4503-BA4B-CC7DD4228963}",
"post_id": 93059,
"post_name": 2,
"feeder_name": "غزال",
"zone_id": 842,
"feeder_id": 93320,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{12A1BE76-5286-4503-BA4B-CC7DD4228963}",
"post_id": 93059,
"post_name": 2,
"feeder_name": "تلويزيون",
"zone_id": 833,
"feeder_id": 105613,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{1962F390-2ED7-4C8C-9578-8604C0289423}",
"post_id": 93048,
"post_name": 2,
"feeder_name": "جوش اکسيژن",
"zone_id": 837,
"feeder_id": 93178,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{1962F390-2ED7-4C8C-9578-8604C0289423}",
"post_id": 93048,
"post_name": 2,
"feeder_name": "البسکو",
"zone_id": 839,
"feeder_id": 105473,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{1962F390-2ED7-4C8C-9578-8604C0289423}",
"post_id": 93048,
"post_name": 2,
"feeder_name": "ايران خودرو",
"zone_id": 839,
"feeder_id": 93179,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{1962F390-2ED7-4C8C-9578-8604C0289423}",
"post_id": 93048,
"post_name": 2,
"feeder_name": "چاهشک",
"zone_id": 837,
"feeder_id": 93180,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{24012B2D-A022-4729-B600-E9AD720E9ADD}",
"post_id": 93016,
"post_name": 2,
"feeder_name": "پرنيان",
"zone_id": 833,
"feeder_id": 105568,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{24012B2D-A022-4729-B600-E9AD720E9ADD}",
"post_id": 93016,
"post_name": 2,
"feeder_name": "موج کوتاه",
"zone_id": 837,
"feeder_id": 105566,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{24012B2D-A022-4729-B600-E9AD720E9ADD}",
"post_id": 93016,
"post_name": 2,
"feeder_name": "شهرکها",
"zone_id": 833,
"feeder_id": 93262,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{24012B2D-A022-4729-B600-E9AD720E9ADD}",
"post_id": 93016,
"post_name": 2,
"feeder_name": "بنفشه",
"zone_id": 833,
"feeder_id": 105567,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{24012B2D-A022-4729-B600-E9AD720E9ADD}",
"post_id": 93016,
"post_name": 2,
"feeder_name": "ابن سينا",
"zone_id": 837,
"feeder_id": 105569,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{254C8D5C-7EFB-4369-8462-181F5D8827AE}",
"post_id": 93042,
"post_name": 1,
"feeder_name": "آذين",
"zone_id": 846,
"feeder_id": 105646,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{254C8D5C-7EFB-4369-8462-181F5D8827AE}",
"post_id": 93042,
"post_name": 1,
"feeder_name": "سولماز",
"zone_id": 846,
"feeder_id": 93340,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{254C8D5C-7EFB-4369-8462-181F5D8827AE}",
"post_id": 93042,
"post_name": 1,
"feeder_name": "مترو",
"zone_id": 846,
"feeder_id": 93341,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{26D5B08F-0290-40C8-96A1-8DC88F056E95}",
"post_id": 105338,
"post_name": 1,
"feeder_name": "دوستان",
"zone_id": 846,
"feeder_id": 93339,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{26D5B08F-0290-40C8-96A1-8DC88F056E95}",
"post_id": 105338,
"post_name": 1,
"feeder_name": "مصلی",
"zone_id": 846,
"feeder_id": 105638,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{26D5B08F-0290-40C8-96A1-8DC88F056E95}",
"post_id": 105338,
"post_name": 1,
"feeder_name": "رامین",
"zone_id": 846,
"feeder_id": 105643,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{26D5B08F-0290-40C8-96A1-8DC88F056E95}",
"post_id": 105338,
"post_name": 1,
"feeder_name": "مترو",
"zone_id": 842,
"feeder_id": 105640,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{26D5B08F-0290-40C8-96A1-8DC88F056E95}",
"post_id": 105338,
"post_name": 1,
"feeder_name": "نوسازي توزیع",
"zone_id": 846,
"feeder_id": 93338,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{274BBE4A-CE2F-455A-8525-003B328CBA21}",
"post_id": 93040,
"post_name": 2,
"feeder_name": "رز",
"zone_id": 848,
"feeder_id": 93101,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{274BBE4A-CE2F-455A-8525-003B328CBA21}",
"post_id": 93040,
"post_name": 2,
"feeder_name": "پويا",
"zone_id": 848,
"feeder_id": 105369,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{274BBE4A-CE2F-455A-8525-003B328CBA21}",
"post_id": 93040,
"post_name": 2,
"feeder_name": "گلچين",
"zone_id": 848,
"feeder_id": 93098,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{274BBE4A-CE2F-455A-8525-003B328CBA21}",
"post_id": 93040,
"post_name": 2,
"feeder_name": "ميهن",
"zone_id": 848,
"feeder_id": 93099,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{27AC0C96-3E3E-4D91-A008-E8E86D93644A}",
"post_id": 105328,
"post_name": 1,
"feeder_name": "سوئیز",
"zone_id": 835,
"feeder_id": 105604,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{27AC0C96-3E3E-4D91-A008-E8E86D93644A}",
"post_id": 105328,
"post_name": 1,
"feeder_name": "مرتفع",
"zone_id": 835,
"feeder_id": 105607,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{27AC0C96-3E3E-4D91-A008-E8E86D93644A}",
"post_id": 105328,
"post_name": 1,
"feeder_name": "سارا",
"zone_id": 835,
"feeder_id": 93317,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2942D0DD-5DE4-47AE-825C-9A6D6F3D15A5}",
"post_id": 93008,
"post_name": 2,
"feeder_name": "زمرد",
"zone_id": 856,
"feeder_id": 105624,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2942D0DD-5DE4-47AE-825C-9A6D6F3D15A5}",
"post_id": 93008,
"post_name": 2,
"feeder_name": "زنبق",
"zone_id": 846,
"feeder_id": 105623,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2942D0DD-5DE4-47AE-825C-9A6D6F3D15A5}",
"post_id": 93008,
"post_name": 2,
"feeder_name": "فرشاد",
"zone_id": 856,
"feeder_id": 105621,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2942D0DD-5DE4-47AE-825C-9A6D6F3D15A5}",
"post_id": 93008,
"post_name": 2,
"feeder_name": "پرستو",
"zone_id": 846,
"feeder_id": 105617,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2942D0DD-5DE4-47AE-825C-9A6D6F3D15A5}",
"post_id": 93008,
"post_name": 2,
"feeder_name": "دهخدا",
"zone_id": 841,
"feeder_id": 105616,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A2D5064-4B56-45FC-9811-574596035E0D}",
"post_id": 105316,
"post_name": 1,
"feeder_name": "مهتاب",
"zone_id": 833,
"feeder_id": 93150,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A2D5064-4B56-45FC-9811-574596035E0D}",
"post_id": 105316,
"post_name": 1,
"feeder_name": "پيام",
"zone_id": 833,
"feeder_id": 105444,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A2D5064-4B56-45FC-9811-574596035E0D}",
"post_id": 105316,
"post_name": 1,
"feeder_name": "نرگس",
"zone_id": 833,
"feeder_id": 105446,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A2D5064-4B56-45FC-9811-574596035E0D}",
"post_id": 105316,
"post_name": 1,
"feeder_name": "اديب",
"zone_id": 833,
"feeder_id": 93149,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A922FD4-5237-47FB-B9D0-8F6BF25A571E}",
"post_id": 93002,
"post_name": 1,
"feeder_name": "شيرين",
"zone_id": 842,
"feeder_id": 93284,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A922FD4-5237-47FB-B9D0-8F6BF25A571E}",
"post_id": 93002,
"post_name": 1,
"feeder_name": "کوهسر",
"zone_id": 842,
"feeder_id": 93281,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A922FD4-5237-47FB-B9D0-8F6BF25A571E}",
"post_id": 93002,
"post_name": 1,
"feeder_name": "سهند",
"zone_id": 842,
"feeder_id": 105589,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A922FD4-5237-47FB-B9D0-8F6BF25A571E}",
"post_id": 93002,
"post_name": 1,
"feeder_name": "محتشم",
"zone_id": 842,
"feeder_id": 93280,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2A922FD4-5237-47FB-B9D0-8F6BF25A571E}",
"post_id": 93002,
"post_name": 1,
"feeder_name": "پرسي",
"zone_id": 842,
"feeder_id": 93286,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2AD20F23-DC2B-485B-9B79-F34F29A02BE8}",
"post_id": 93047,
"post_name": 2,
"feeder_name": "طبرسي",
"zone_id": 841,
"feeder_id": 105545,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2AD20F23-DC2B-485B-9B79-F34F29A02BE8}",
"post_id": 93047,
"post_name": 2,
"feeder_name": "طلاب",
"zone_id": 846,
"feeder_id": 93246,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2AD20F23-DC2B-485B-9B79-F34F29A02BE8}",
"post_id": 93047,
"post_name": 2,
"feeder_name": "اختصاصي سيلو",
"zone_id": 841,
"feeder_id": 105547,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{2AD20F23-DC2B-485B-9B79-F34F29A02BE8}",
"post_id": 93047,
"post_name": 2,
"feeder_name": "حاتم",
"zone_id": 841,
"feeder_id": 105549,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{36C45DD6-801A-4F9F-A2AF-B358654F5DE6}",
"post_id": 93000,
"post_name": 1,
"feeder_name": "کاشف",
"zone_id": 847,
"feeder_id": 105485,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{36C45DD6-801A-4F9F-A2AF-B358654F5DE6}",
"post_id": 93000,
"post_name": 1,
"feeder_name": "امام تقي",
"zone_id": 847,
"feeder_id": 93189,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{38467E9F-56DA-4A0F-843D-A47A6CFA11BE}",
"post_id": 93064,
"post_name": 1,
"feeder_name": "نگارش",
"zone_id": 838,
"feeder_id": 93132,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{38467E9F-56DA-4A0F-843D-A47A6CFA11BE}",
"post_id": 93064,
"post_name": 1,
"feeder_name": "ساعي",
"zone_id": 838,
"feeder_id": 105414,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{38467E9F-56DA-4A0F-843D-A47A6CFA11BE}",
"post_id": 93064,
"post_name": 1,
"feeder_name": "رشد",
"zone_id": 835,
"feeder_id": 93133,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{38467E9F-56DA-4A0F-843D-A47A6CFA11BE}",
"post_id": 93064,
"post_name": 1,
"feeder_name": "فاخر",
"zone_id": 838,
"feeder_id": 105416,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{38990E9D-9BB3-4F68-AA8E-7498DF2B30AE}",
"post_id": 105330,
"post_name": 2,
"feeder_name": "مهرآباد",
"zone_id": 846,
"feeder_id": 105647,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{38990E9D-9BB3-4F68-AA8E-7498DF2B30AE}",
"post_id": 105330,
"post_name": 2,
"feeder_name": "بهزيستي",
"zone_id": 846,
"feeder_id": 105648,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{38990E9D-9BB3-4F68-AA8E-7498DF2B30AE}",
"post_id": 105330,
"post_name": 2,
"feeder_name": "کشاورز",
"zone_id": 846,
"feeder_id": 105651,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{39E93268-24C6-4660-AC33-992984F95C62}",
"post_id": 93036,
"post_name": 2,
"feeder_name": "دانش",
"zone_id": 848,
"feeder_id": 105364,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{39E93268-24C6-4660-AC33-992984F95C62}",
"post_id": 93036,
"post_name": 2,
"feeder_name": "لاله",
"zone_id": 848,
"feeder_id": 105362,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{39E93268-24C6-4660-AC33-992984F95C62}",
"post_id": 93036,
"post_name": 2,
"feeder_name": "شمشاد",
"zone_id": 848,
"feeder_id": 93083,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{39E93268-24C6-4660-AC33-992984F95C62}",
"post_id": 93036,
"post_name": 2,
"feeder_name": "حرم مطهر",
"zone_id": 848,
"feeder_id": 105363,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{39E93268-24C6-4660-AC33-992984F95C62}",
"post_id": 93036,
"post_name": 2,
"feeder_name": "اطلس",
"zone_id": 848,
"feeder_id": 93084,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3C8A165B-FF1B-4E57-952A-E6F764994F1C}",
"post_id": 93032,
"post_name": 1,
"feeder_name": "کاج",
"zone_id": 839,
"feeder_id": 105522,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3C8A165B-FF1B-4E57-952A-E6F764994F1C}",
"post_id": 93032,
"post_name": 1,
"feeder_name": "خیزران",
"zone_id": 837,
"feeder_id": 93225,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3C8A165B-FF1B-4E57-952A-E6F764994F1C}",
"post_id": 93032,
"post_name": 1,
"feeder_name": "زيتون",
"zone_id": 836,
"feeder_id": 105520,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3C8A165B-FF1B-4E57-952A-E6F764994F1C}",
"post_id": 93032,
"post_name": 1,
"feeder_name": "توسکا",
"zone_id": 837,
"feeder_id": 93226,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3D46237B-69F8-4EFC-9984-ACFD7A7F8915}",
"post_id": 93015,
"post_name": 1,
"feeder_name": "جهانگیر",
"zone_id": 839,
"feeder_id": 105391,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3D46237B-69F8-4EFC-9984-ACFD7A7F8915}",
"post_id": 93015,
"post_name": 1,
"feeder_name": "شباهنگ",
"zone_id": 839,
"feeder_id": 93109,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3D46237B-69F8-4EFC-9984-ACFD7A7F8915}",
"post_id": 93015,
"post_name": 1,
"feeder_name": "فرامرز",
"zone_id": 839,
"feeder_id": 93111,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3D46237B-69F8-4EFC-9984-ACFD7A7F8915}",
"post_id": 93015,
"post_name": 1,
"feeder_name": "رستم",
"zone_id": 839,
"feeder_id": 93110,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{3D46237B-69F8-4EFC-9984-ACFD7A7F8915}",
"post_id": 93015,
"post_name": 1,
"feeder_name": "کیومرث",
"zone_id": 839,
"feeder_id": 93108,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{412F2951-435C-496F-A907-18946937CEE5}",
"post_id": 130661,
"post_name": 1,
"feeder_name": "ییلاق",
"zone_id": 836,
"feeder_id": 130663,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{412F2951-435C-496F-A907-18946937CEE5}",
"post_id": 130661,
"post_name": 1,
"feeder_name": "چمنزار",
"zone_id": 836,
"feeder_id": 130664,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{46C9600A-B439-4B67-B901-13F8A17F2894}",
"post_id": 93039,
"post_name": 2,
"feeder_name": "بهداشتکاران",
"zone_id": 846,
"feeder_id": 105642,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{46C9600A-B439-4B67-B901-13F8A17F2894}",
"post_id": 93039,
"post_name": 2,
"feeder_name": "عزت",
"zone_id": 846,
"feeder_id": 105644,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{46C9600A-B439-4B67-B901-13F8A17F2894}",
"post_id": 93039,
"post_name": 2,
"feeder_name": "عدالت",
"zone_id": 842,
"feeder_id": 105641,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{49501CF0-1BF7-46CA-8C0E-B37C8BF5583B}",
"post_id": 93058,
"post_name": 2,
"feeder_name": "محقق",
"zone_id": 838,
"feeder_id": 93165,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{49501CF0-1BF7-46CA-8C0E-B37C8BF5583B}",
"post_id": 93058,
"post_name": 2,
"feeder_name": "باباطاهر",
"zone_id": 838,
"feeder_id": 93166,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{49501CF0-1BF7-46CA-8C0E-B37C8BF5583B}",
"post_id": 93058,
"post_name": 2,
"feeder_name": "غزنوي",
"zone_id": 837,
"feeder_id": 105460,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{49501CF0-1BF7-46CA-8C0E-B37C8BF5583B}",
"post_id": 93058,
"post_name": 2,
"feeder_name": "ابوريحان",
"zone_id": 838,
"feeder_id": 105459,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{49501CF0-1BF7-46CA-8C0E-B37C8BF5583B}",
"post_id": 93058,
"post_name": 2,
"feeder_name": "قطار شهری 2",
"zone_id": 838,
"feeder_id": 105457,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{49501CF0-1BF7-46CA-8C0E-B37C8BF5583B}",
"post_id": 93058,
"post_name": 2,
"feeder_name": "تعليم",
"zone_id": 837,
"feeder_id": 93167,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{53B2AEB7-C999-4690-8414-0076C4ADFE62}",
"post_id": 105308,
"post_name": 2,
"feeder_name": "دلاور",
"zone_id": 833,
"feeder_id": 93234,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{53B2AEB7-C999-4690-8414-0076C4ADFE62}",
"post_id": 105308,
"post_name": 2,
"feeder_name": "وفادار",
"zone_id": 833,
"feeder_id": 105536,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{53B2AEB7-C999-4690-8414-0076C4ADFE62}",
"post_id": 105308,
"post_name": 2,
"feeder_name": "مروت",
"zone_id": 833,
"feeder_id": 93232,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{53C782A1-9679-40C3-9DC2-5A5D6B0D693E}",
"post_id": 93045,
"post_name": 1,
"feeder_name": "فياض",
"zone_id": 848,
"feeder_id": 93207,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{53C782A1-9679-40C3-9DC2-5A5D6B0D693E}",
"post_id": 93045,
"post_name": 1,
"feeder_name": "کرامت",
"zone_id": 848,
"feeder_id": 105502,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{53C782A1-9679-40C3-9DC2-5A5D6B0D693E}",
"post_id": 93045,
"post_name": 1,
"feeder_name": "محراب",
"zone_id": 848,
"feeder_id": 105501,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{53C782A1-9679-40C3-9DC2-5A5D6B0D693E}",
"post_id": 93045,
"post_name": 1,
"feeder_name": "حاجت",
"zone_id": 848,
"feeder_id": 93206,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{53C782A1-9679-40C3-9DC2-5A5D6B0D693E}",
"post_id": 93045,
"post_name": 1,
"feeder_name": "سيادت",
"zone_id": 848,
"feeder_id": 105503,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{582E1E9C-04F4-446C-AB5A-2767C75FDD32}",
"post_id": 93021,
"post_name": 3,
"feeder_name": "سعدآباد",
"zone_id": 833,
"feeder_id": 105448,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{582E1E9C-04F4-446C-AB5A-2767C75FDD32}",
"post_id": 93021,
"post_name": 3,
"feeder_name": "شهر جديد",
"zone_id": 833,
"feeder_id": 105449,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{582E1E9C-04F4-446C-AB5A-2767C75FDD32}",
"post_id": 93021,
"post_name": 3,
"feeder_name": "احمد آباد",
"zone_id": 833,
"feeder_id": 93148,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{582E1E9C-04F4-446C-AB5A-2767C75FDD32}",
"post_id": 93021,
"post_name": 3,
"feeder_name": "احسان",
"zone_id": 833,
"feeder_id": 93156,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5CFBD6B8-635F-43E4-AAD5-D1757A65D421}",
"post_id": 93060,
"post_name": 1,
"feeder_name": "سرباز",
"zone_id": 842,
"feeder_id": 105347,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5CFBD6B8-635F-43E4-AAD5-D1757A65D421}",
"post_id": 93060,
"post_name": 1,
"feeder_name": "زيست خاور",
"zone_id": 842,
"feeder_id": 105345,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5CFBD6B8-635F-43E4-AAD5-D1757A65D421}",
"post_id": 93060,
"post_name": 1,
"feeder_name": "پيمان",
"zone_id": 848,
"feeder_id": 93073,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5CFBD6B8-635F-43E4-AAD5-D1757A65D421}",
"post_id": 93060,
"post_name": 1,
"feeder_name": "آسيا",
"zone_id": 848,
"feeder_id": 105344,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5CFBD6B8-635F-43E4-AAD5-D1757A65D421}",
"post_id": 93060,
"post_name": 1,
"feeder_name": "هما",
"zone_id": 833,
"feeder_id": 105346,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5F769E64-76CD-4982-AB47-54FEF2A8EAF0}",
"post_id": 93003,
"post_name": 1,
"feeder_name": "سيماب",
"zone_id": 837,
"feeder_id": 105628,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5F769E64-76CD-4982-AB47-54FEF2A8EAF0}",
"post_id": 93003,
"post_name": 1,
"feeder_name": "خرسند",
"zone_id": 837,
"feeder_id": 93328,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5F769E64-76CD-4982-AB47-54FEF2A8EAF0}",
"post_id": 93003,
"post_name": 1,
"feeder_name": "فن آوري",
"zone_id": 837,
"feeder_id": 105629,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{5F769E64-76CD-4982-AB47-54FEF2A8EAF0}",
"post_id": 93003,
"post_name": 1,
"feeder_name": "مهارت",
"zone_id": 837,
"feeder_id": 93326,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{61C82472-29AE-40A3-BD22-8C09F8B12386}",
"post_id": 93037,
"post_name": 2,
"feeder_name": "کوه سفيد",
"zone_id": 843,
"feeder_id": 105425,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{61C82472-29AE-40A3-BD22-8C09F8B12386}",
"post_id": 93037,
"post_name": 2,
"feeder_name": "دهقان",
"zone_id": 843,
"feeder_id": 93137,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{61C82472-29AE-40A3-BD22-8C09F8B12386}",
"post_id": 93037,
"post_name": 2,
"feeder_name": "بزنگان",
"zone_id": 843,
"feeder_id": 105426,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{64D4D480-B567-48F2-B289-A78C07E2C9F3}",
"post_id": 93057,
"post_name": 2,
"feeder_name": "دانشکده",
"zone_id": 846,
"feeder_id": 105561,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{64D4D480-B567-48F2-B289-A78C07E2C9F3}",
"post_id": 93057,
"post_name": 2,
"feeder_name": "امير اباد",
"zone_id": 843,
"feeder_id": 93258,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{64D4D480-B567-48F2-B289-A78C07E2C9F3}",
"post_id": 93057,
"post_name": 2,
"feeder_name": "رادار",
"zone_id": 843,
"feeder_id": 105562,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{64D4D480-B567-48F2-B289-A78C07E2C9F3}",
"post_id": 93057,
"post_name": 2,
"feeder_name": "مزداوند",
"zone_id": 843,
"feeder_id": 93259,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{64D4D480-B567-48F2-B289-A78C07E2C9F3}",
"post_id": 93057,
"post_name": 2,
"feeder_name": "فرکانس",
"zone_id": 846,
"feeder_id": 93260,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{654049EF-BD14-40A0-9052-60C94BC7AAC9}",
"post_id": 93010,
"post_name": 2,
"feeder_name": "اردشیر",
"zone_id": 839,
"feeder_id": 105396,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{654049EF-BD14-40A0-9052-60C94BC7AAC9}",
"post_id": 93010,
"post_name": 2,
"feeder_name": "آبان",
"zone_id": 853,
"feeder_id": 105394,
"zone_name": "توس",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{654049EF-BD14-40A0-9052-60C94BC7AAC9}",
"post_id": 93010,
"post_name": 2,
"feeder_name": "شهريار",
"zone_id": 839,
"feeder_id": 93115,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{654049EF-BD14-40A0-9052-60C94BC7AAC9}",
"post_id": 93010,
"post_name": 2,
"feeder_name": "اسفندیار",
"zone_id": 839,
"feeder_id": 93117,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{654049EF-BD14-40A0-9052-60C94BC7AAC9}",
"post_id": 93010,
"post_name": 2,
"feeder_name": "آرش",
"zone_id": 839,
"feeder_id": 93114,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{654049EF-BD14-40A0-9052-60C94BC7AAC9}",
"post_id": 93010,
"post_name": 2,
"feeder_name": "کسري",
"zone_id": 839,
"feeder_id": 93116,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{65D6A3ED-45A7-4B5C-8FA1-E8392420A49B}",
"post_id": 105322,
"post_name": 2,
"feeder_name": "شريف",
"zone_id": 848,
"feeder_id": 93356,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{65D6A3ED-45A7-4B5C-8FA1-E8392420A49B}",
"post_id": 105322,
"post_name": 2,
"feeder_name": "مرکزي",
"zone_id": 848,
"feeder_id": 93357,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{65D6A3ED-45A7-4B5C-8FA1-E8392420A49B}",
"post_id": 105322,
"post_name": 2,
"feeder_name": "خادم",
"zone_id": 848,
"feeder_id": 93355,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{65D6A3ED-45A7-4B5C-8FA1-E8392420A49B}",
"post_id": 105322,
"post_name": 2,
"feeder_name": "بهمن",
"zone_id": 848,
"feeder_id": 93352,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{65D6A3ED-45A7-4B5C-8FA1-E8392420A49B}",
"post_id": 105322,
"post_name": 2,
"feeder_name": "مجاور",
"zone_id": 848,
"feeder_id": 105660,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{6644676E-5FF1-4561-9334-9642E2D0E17E}",
"post_id": 105337,
"post_name": 2,
"feeder_name": "پارسا",
"zone_id": 837,
"feeder_id": 105673,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{6644676E-5FF1-4561-9334-9642E2D0E17E}",
"post_id": 105337,
"post_name": 2,
"feeder_name": "ميخک",
"zone_id": 837,
"feeder_id": 93364,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{6644676E-5FF1-4561-9334-9642E2D0E17E}",
"post_id": 105337,
"post_name": 2,
"feeder_name": "سمند",
"zone_id": 838,
"feeder_id": 105672,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{6644676E-5FF1-4561-9334-9642E2D0E17E}",
"post_id": 105337,
"post_name": 2,
"feeder_name": "روشنا",
"zone_id": 837,
"feeder_id": 105671,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{6644676E-5FF1-4561-9334-9642E2D0E17E}",
"post_id": 105337,
"post_name": 2,
"feeder_name": "ارغوان",
"zone_id": 836,
"feeder_id": 93366,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{6E1D0B52-A0BB-48C0-86B8-FA3BE5CA7633}",
"post_id": 105302,
"post_name": 1,
"feeder_name": "دنا",
"zone_id": 847,
"feeder_id": 105554,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{71DE1343-09D8-4BC5-96E5-1F4703B7CAAB}",
"post_id": 105321,
"post_name": 1,
"feeder_name": "نودره",
"zone_id": 835,
"feeder_id": 93069,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{71DE1343-09D8-4BC5-96E5-1F4703B7CAAB}",
"post_id": 105321,
"post_name": 1,
"feeder_name": "ذاکر",
"zone_id": 836,
"feeder_id": 93068,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{71DE1343-09D8-4BC5-96E5-1F4703B7CAAB}",
"post_id": 105321,
"post_name": 1,
"feeder_name": "وثوق",
"zone_id": 835,
"feeder_id": 105339,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{71DE1343-09D8-4BC5-96E5-1F4703B7CAAB}",
"post_id": 105321,
"post_name": 1,
"feeder_name": "ارشاد",
"zone_id": 835,
"feeder_id": 105340,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{795649CA-F0FC-44BB-968C-8C60CE088F2E}",
"post_id": 105333,
"post_name": 1,
"feeder_name": "ارس",
"zone_id": 839,
"feeder_id": 105403,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{795649CA-F0FC-44BB-968C-8C60CE088F2E}",
"post_id": 105333,
"post_name": 1,
"feeder_name": "فدک",
"zone_id": 834,
"feeder_id": 93124,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{795649CA-F0FC-44BB-968C-8C60CE088F2E}",
"post_id": 105333,
"post_name": 1,
"feeder_name": "اروند",
"zone_id": 839,
"feeder_id": 105401,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{795649CA-F0FC-44BB-968C-8C60CE088F2E}",
"post_id": 105333,
"post_name": 1,
"feeder_name": "گلچهر",
"zone_id": 839,
"feeder_id": 93123,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{795649CA-F0FC-44BB-968C-8C60CE088F2E}",
"post_id": 105333,
"post_name": 1,
"feeder_name": "نسترن",
"zone_id": 834,
"feeder_id": 105400,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{7F98C1D7-859E-4EB3-A5DE-1CAF06F99D83}",
"post_id": 105334,
"post_name": 2,
"feeder_name": "سيصد تني",
"zone_id": 843,
"feeder_id": 93199,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{7F98C1D7-859E-4EB3-A5DE-1CAF06F99D83}",
"post_id": 105334,
"post_name": 2,
"feeder_name": "تبادکان",
"zone_id": 856,
"feeder_id": 93201,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{7F98C1D7-859E-4EB3-A5DE-1CAF06F99D83}",
"post_id": 105334,
"post_name": 2,
"feeder_name": "فرخنده",
"zone_id": 856,
"feeder_id": 93202,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{7F98C1D7-859E-4EB3-A5DE-1CAF06F99D83}",
"post_id": 105334,
"post_name": 2,
"feeder_name": "اترک",
"zone_id": 856,
"feeder_id": 93203,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{7F98C1D7-859E-4EB3-A5DE-1CAF06F99D83}",
"post_id": 105334,
"post_name": 2,
"feeder_name": "الکتروموتور",
"zone_id": 843,
"feeder_id": 93200,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{8EB1B9A9-0053-466C-8F9F-18FBC2E3732A}",
"post_id": 93012,
"post_name": 2,
"feeder_name": "پدرام",
"zone_id": 834,
"feeder_id": 105530,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{8EB1B9A9-0053-466C-8F9F-18FBC2E3732A}",
"post_id": 93012,
"post_name": 2,
"feeder_name": "آروين",
"zone_id": 834,
"feeder_id": 105527,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{8EB1B9A9-0053-466C-8F9F-18FBC2E3732A}",
"post_id": 93012,
"post_name": 2,
"feeder_name": "پالیز",
"zone_id": 834,
"feeder_id": 105529,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{8EB1B9A9-0053-466C-8F9F-18FBC2E3732A}",
"post_id": 93012,
"post_name": 2,
"feeder_name": "مجيد",
"zone_id": 834,
"feeder_id": 105526,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{8EB1B9A9-0053-466C-8F9F-18FBC2E3732A}",
"post_id": 93012,
"post_name": 2,
"feeder_name": "مغان",
"zone_id": 834,
"feeder_id": 93227,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{8EB1B9A9-0053-466C-8F9F-18FBC2E3732A}",
"post_id": 93012,
"post_name": 2,
"feeder_name": "شهروند",
"zone_id": 834,
"feeder_id": 105528,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{92B9CC02-F69E-4289-8022-9D8C879234A2}",
"post_id": 93034,
"post_name": 1,
"feeder_name": "سنائي",
"zone_id": 833,
"feeder_id": 105478,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{92B9CC02-F69E-4289-8022-9D8C879234A2}",
"post_id": 93034,
"post_name": 1,
"feeder_name": "رازي",
"zone_id": 848,
"feeder_id": 105483,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{92B9CC02-F69E-4289-8022-9D8C879234A2}",
"post_id": 93034,
"post_name": 1,
"feeder_name": "ارکيده",
"zone_id": 834,
"feeder_id": 105477,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{92B9CC02-F69E-4289-8022-9D8C879234A2}",
"post_id": 93034,
"post_name": 1,
"feeder_name": "ياس",
"zone_id": 834,
"feeder_id": 105480,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{92B9CC02-F69E-4289-8022-9D8C879234A2}",
"post_id": 93034,
"post_name": 1,
"feeder_name": "کاشاني",
"zone_id": 834,
"feeder_id": 93182,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{946D6125-EFA0-415B-A5C6-ECFDB2E150E2}",
"post_id": 105332,
"post_name": 1,
"feeder_name": "جويني",
"zone_id": 834,
"feeder_id": 105451,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{946D6125-EFA0-415B-A5C6-ECFDB2E150E2}",
"post_id": 105332,
"post_name": 1,
"feeder_name": "جواهر",
"zone_id": 841,
"feeder_id": 93157,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{946D6125-EFA0-415B-A5C6-ECFDB2E150E2}",
"post_id": 105332,
"post_name": 1,
"feeder_name": "مينا",
"zone_id": 853,
"feeder_id": 93158,
"zone_name": "توس",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{946D6125-EFA0-415B-A5C6-ECFDB2E150E2}",
"post_id": 105332,
"post_name": 1,
"feeder_name": "فجر",
"zone_id": 841,
"feeder_id": 93159,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{946D6125-EFA0-415B-A5C6-ECFDB2E150E2}",
"post_id": 105332,
"post_name": 1,
"feeder_name": "گرجي",
"zone_id": 856,
"feeder_id": 93162,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "مثنوي",
"zone_id": 834,
"feeder_id": 105376,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "ايرانمهر",
"zone_id": 834,
"feeder_id": 105375,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "يوسف",
"zone_id": 834,
"feeder_id": 105378,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "فرزاد",
"zone_id": 833,
"feeder_id": 105379,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "پاژن",
"zone_id": 833,
"feeder_id": 93105,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "محسن",
"zone_id": 834,
"feeder_id": 105384,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "رشيد",
"zone_id": 833,
"feeder_id": 105377,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "غیور",
"zone_id": 834,
"feeder_id": 105383,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{95CDAE4B-0B2F-48B2-8AFB-5981D639D05B}",
"post_id": 93035,
"post_name": 2,
"feeder_name": "يعقوب",
"zone_id": 834,
"feeder_id": 93104,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{96F86F75-CA57-49ED-B92A-21AC834A47E0}",
"post_id": 105331,
"post_name": 1,
"feeder_name": "هزار تني",
"zone_id": 843,
"feeder_id": 105496,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{96F86F75-CA57-49ED-B92A-21AC834A47E0}",
"post_id": 105331,
"post_name": 1,
"feeder_name": "ميلاد",
"zone_id": 856,
"feeder_id": 105497,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{96F86F75-CA57-49ED-B92A-21AC834A47E0}",
"post_id": 105331,
"post_name": 1,
"feeder_name": "کارده",
"zone_id": 853,
"feeder_id": 93197,
"zone_name": "توس",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{96F86F75-CA57-49ED-B92A-21AC834A47E0}",
"post_id": 105331,
"post_name": 1,
"feeder_name": "قياس آباد",
"zone_id": 843,
"feeder_id": 105498,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{96F86F75-CA57-49ED-B92A-21AC834A47E0}",
"post_id": 105331,
"post_name": 1,
"feeder_name": "سايت",
"zone_id": 856,
"feeder_id": 93198,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{96F86F75-CA57-49ED-B92A-21AC834A47E0}",
"post_id": 105331,
"post_name": 1,
"feeder_name": "همت آباد",
"zone_id": 856,
"feeder_id": 105495,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{9D45FBB0-D623-4DA6-92E2-DFC96801256B}",
"post_id": 105318,
"post_name": 1,
"feeder_name": "میعاد",
"zone_id": 847,
"feeder_id": 93128,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{9D45FBB0-D623-4DA6-92E2-DFC96801256B}",
"post_id": 105318,
"post_name": 1,
"feeder_name": "درخشان",
"zone_id": 847,
"feeder_id": 105404,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{9D45FBB0-D623-4DA6-92E2-DFC96801256B}",
"post_id": 105318,
"post_name": 1,
"feeder_name": "شعله",
"zone_id": 847,
"feeder_id": 93127,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{9D45FBB0-D623-4DA6-92E2-DFC96801256B}",
"post_id": 105318,
"post_name": 1,
"feeder_name": "مشعل",
"zone_id": 847,
"feeder_id": 105411,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{9D45FBB0-D623-4DA6-92E2-DFC96801256B}",
"post_id": 105318,
"post_name": 1,
"feeder_name": "تارا",
"zone_id": 847,
"feeder_id": 105410,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3840661-6CF3-4B74-957B-DBCC0D15D22B}",
"post_id": 93043,
"post_name": 1,
"feeder_name": "هاتف",
"zone_id": 838,
"feeder_id": 93169,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3840661-6CF3-4B74-957B-DBCC0D15D22B}",
"post_id": 93043,
"post_name": 1,
"feeder_name": "فرزانگان",
"zone_id": 837,
"feeder_id": 93164,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3840661-6CF3-4B74-957B-DBCC0D15D22B}",
"post_id": 93043,
"post_name": 1,
"feeder_name": "جاهد",
"zone_id": 838,
"feeder_id": 93168,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3840661-6CF3-4B74-957B-DBCC0D15D22B}",
"post_id": 93043,
"post_name": 1,
"feeder_name": "عنصري",
"zone_id": 838,
"feeder_id": 93170,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3840661-6CF3-4B74-957B-DBCC0D15D22B}",
"post_id": 93043,
"post_name": 1,
"feeder_name": "مفيد",
"zone_id": 838,
"feeder_id": 105456,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3F3172C-812C-44B1-A850-995541DC41DB}",
"post_id": 93062,
"post_name": 2,
"feeder_name": "آموزش",
"zone_id": 835,
"feeder_id": 105412,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3F3172C-812C-44B1-A850-995541DC41DB}",
"post_id": 93062,
"post_name": 2,
"feeder_name": "تمدن",
"zone_id": 838,
"feeder_id": 105418,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3F3172C-812C-44B1-A850-995541DC41DB}",
"post_id": 93062,
"post_name": 2,
"feeder_name": "معرفت",
"zone_id": 838,
"feeder_id": 93130,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3F3172C-812C-44B1-A850-995541DC41DB}",
"post_id": 93062,
"post_name": 2,
"feeder_name": "آیین",
"zone_id": 838,
"feeder_id": 105417,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A3F3172C-812C-44B1-A850-995541DC41DB}",
"post_id": 93062,
"post_name": 2,
"feeder_name": "پرورش",
"zone_id": 838,
"feeder_id": 105415,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A4F6AEC2-5ABE-439C-8711-1CDA66A09086}",
"post_id": 93018,
"post_name": 1,
"feeder_name": "دريا",
"zone_id": 841,
"feeder_id": 93305,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A4F6AEC2-5ABE-439C-8711-1CDA66A09086}",
"post_id": 93018,
"post_name": 1,
"feeder_name": "مرجان",
"zone_id": 841,
"feeder_id": 105602,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A4F6AEC2-5ABE-439C-8711-1CDA66A09086}",
"post_id": 93018,
"post_name": 1,
"feeder_name": "صخره",
"zone_id": 841,
"feeder_id": 93310,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A4F6AEC2-5ABE-439C-8711-1CDA66A09086}",
"post_id": 93018,
"post_name": 1,
"feeder_name": "ساحل",
"zone_id": 841,
"feeder_id": 93307,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A941F020-D5E0-4D13-BBA3-651CE2011B55}",
"post_id": 105301,
"post_name": 1,
"feeder_name": "اکرام",
"zone_id": 842,
"feeder_id": 105439,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A941F020-D5E0-4D13-BBA3-651CE2011B55}",
"post_id": 105301,
"post_name": 1,
"feeder_name": "بيستون",
"zone_id": 842,
"feeder_id": 105436,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A941F020-D5E0-4D13-BBA3-651CE2011B55}",
"post_id": 105301,
"post_name": 1,
"feeder_name": "فرهاد",
"zone_id": 842,
"feeder_id": 105438,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{A941F020-D5E0-4D13-BBA3-651CE2011B55}",
"post_id": 105301,
"post_name": 1,
"feeder_name": "سامان",
"zone_id": 842,
"feeder_id": 105440,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{AA633C39-3570-4555-948F-4B6CFE68EE86}",
"post_id": 93025,
"post_name": 2,
"feeder_name": "قائم",
"zone_id": 833,
"feeder_id": 105349,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{AA633C39-3570-4555-948F-4B6CFE68EE86}",
"post_id": 93025,
"post_name": 2,
"feeder_name": "جرجاني",
"zone_id": 833,
"feeder_id": 105348,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{AA633C39-3570-4555-948F-4B6CFE68EE86}",
"post_id": 93025,
"post_name": 2,
"feeder_name": "چمران",
"zone_id": 848,
"feeder_id": 93074,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{AA633C39-3570-4555-948F-4B6CFE68EE86}",
"post_id": 93025,
"post_name": 2,
"feeder_name": "کانادا",
"zone_id": 833,
"feeder_id": 93076,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{AA633C39-3570-4555-948F-4B6CFE68EE86}",
"post_id": 93025,
"post_name": 2,
"feeder_name": "تقي آباد",
"zone_id": 833,
"feeder_id": 93075,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{AF064DBF-A851-4DCA-BC00-DC5D0E851B48}",
"post_id": 93017,
"post_name": 1,
"feeder_name": "فرزانه",
"zone_id": 839,
"feeder_id": 105670,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{AF064DBF-A851-4DCA-BC00-DC5D0E851B48}",
"post_id": 93017,
"post_name": 1,
"feeder_name": "باقر",
"zone_id": 839,
"feeder_id": 93362,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{AF064DBF-A851-4DCA-BC00-DC5D0E851B48}",
"post_id": 93017,
"post_name": 1,
"feeder_name": "توکل",
"zone_id": 837,
"feeder_id": 93361,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B1025822-6CC4-454D-AEDF-87F575155CA6}",
"post_id": 93041,
"post_name": 2,
"feeder_name": "هنر",
"zone_id": 837,
"feeder_id": 93270,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B1025822-6CC4-454D-AEDF-87F575155CA6}",
"post_id": 93041,
"post_name": 2,
"feeder_name": "الوان",
"zone_id": 837,
"feeder_id": 105580,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B1025822-6CC4-454D-AEDF-87F575155CA6}",
"post_id": 93041,
"post_name": 2,
"feeder_name": "درفش",
"zone_id": 837,
"feeder_id": 93271,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B1025822-6CC4-454D-AEDF-87F575155CA6}",
"post_id": 93041,
"post_name": 2,
"feeder_name": "اقتدار",
"zone_id": 837,
"feeder_id": 93272,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B1025822-6CC4-454D-AEDF-87F575155CA6}",
"post_id": 93041,
"post_name": 2,
"feeder_name": "ميثم",
"zone_id": 836,
"feeder_id": 93269,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B1025822-6CC4-454D-AEDF-87F575155CA6}",
"post_id": 93041,
"post_name": 2,
"feeder_name": "نشاط",
"zone_id": 836,
"feeder_id": 105579,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B1025822-6CC4-454D-AEDF-87F575155CA6}",
"post_id": 93041,
"post_name": 2,
"feeder_name": "الياس",
"zone_id": 837,
"feeder_id": 105577,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B1025822-6CC4-454D-AEDF-87F575155CA6}",
"post_id": 93041,
"post_name": 2,
"feeder_name": "قدرت",
"zone_id": 837,
"feeder_id": 105578,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B309CF95-CE6E-476A-8EAF-8DEA1EC43C5F}",
"post_id": 93028,
"post_name": 2,
"feeder_name": "کوهپایه",
"zone_id": 835,
"feeder_id": 93311,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B3604089-CD9A-4475-ACBC-D2B365FAAA0C}",
"post_id": 105299,
"post_name": 2,
"feeder_name": "نگار",
"zone_id": 847,
"feeder_id": 105488,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B3604089-CD9A-4475-ACBC-D2B365FAAA0C}",
"post_id": 105299,
"post_name": 2,
"feeder_name": "افسانه",
"zone_id": 847,
"feeder_id": 105486,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B44EBE4A-46B4-4833-876F-8204620FC160}",
"post_id": 105313,
"post_name": 1,
"feeder_name": "سنتو",
"zone_id": 839,
"feeder_id": 105565,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B44EBE4A-46B4-4833-876F-8204620FC160}",
"post_id": 105313,
"post_name": 1,
"feeder_name": "فرهنگيان",
"zone_id": 838,
"feeder_id": 105563,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B44EBE4A-46B4-4833-876F-8204620FC160}",
"post_id": 105313,
"post_name": 1,
"feeder_name": "امامت",
"zone_id": 838,
"feeder_id": 93264,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B44EBE4A-46B4-4833-876F-8204620FC160}",
"post_id": 105313,
"post_name": 1,
"feeder_name": "آزادي",
"zone_id": 838,
"feeder_id": 93261,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B44EBE4A-46B4-4833-876F-8204620FC160}",
"post_id": 105313,
"post_name": 1,
"feeder_name": "رسالت",
"zone_id": 833,
"feeder_id": 93263,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B5639611-583F-4E99-81CD-C5F0E5447064}",
"post_id": 93013,
"post_name": 1,
"feeder_name": "بینالود_J16",
"zone_id": 847,
"feeder_id": 105366,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B5639611-583F-4E99-81CD-C5F0E5447064}",
"post_id": 93013,
"post_name": 1,
"feeder_name": "بینالود_J15",
"zone_id": 847,
"feeder_id": 93090,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B5639611-583F-4E99-81CD-C5F0E5447064}",
"post_id": 93013,
"post_name": 1,
"feeder_name": "بینالود_J17",
"zone_id": 847,
"feeder_id": 119738,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B7280A58-855C-4861-99E1-E90C6A1AAC64}",
"post_id": 93052,
"post_name": 1,
"feeder_name": "باستان",
"zone_id": 837,
"feeder_id": 93267,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B7280A58-855C-4861-99E1-E90C6A1AAC64}",
"post_id": 93052,
"post_name": 1,
"feeder_name": "شقايق",
"zone_id": 836,
"feeder_id": 105576,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B7280A58-855C-4861-99E1-E90C6A1AAC64}",
"post_id": 93052,
"post_name": 1,
"feeder_name": "بيژن",
"zone_id": 837,
"feeder_id": 93268,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B7280A58-855C-4861-99E1-E90C6A1AAC64}",
"post_id": 93052,
"post_name": 1,
"feeder_name": "بابک",
"zone_id": 837,
"feeder_id": 105573,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B7280A58-855C-4861-99E1-E90C6A1AAC64}",
"post_id": 93052,
"post_name": 1,
"feeder_name": "آلياژ",
"zone_id": 837,
"feeder_id": 105575,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B7280A58-855C-4861-99E1-E90C6A1AAC64}",
"post_id": 93052,
"post_name": 1,
"feeder_name": "پرتو",
"zone_id": 837,
"feeder_id": 105574,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B7280A58-855C-4861-99E1-E90C6A1AAC64}",
"post_id": 93052,
"post_name": 1,
"feeder_name": "ارم",
"zone_id": 837,
"feeder_id": 105572,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{B7280A58-855C-4861-99E1-E90C6A1AAC64}",
"post_id": 93052,
"post_name": 1,
"feeder_name": "کارآمد",
"zone_id": 837,
"feeder_id": 93266,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BC3B13BB-562C-4027-8D25-5EF358A4878C}",
"post_id": 93009,
"post_name": 1,
"feeder_name": "تعاون",
"zone_id": 846,
"feeder_id": 93323,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BC3B13BB-562C-4027-8D25-5EF358A4878C}",
"post_id": 93009,
"post_name": 1,
"feeder_name": "اقاقيا",
"zone_id": 841,
"feeder_id": 105622,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BC3B13BB-562C-4027-8D25-5EF358A4878C}",
"post_id": 93009,
"post_name": 1,
"feeder_name": "شکوفه",
"zone_id": 846,
"feeder_id": 105619,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BC3B13BB-562C-4027-8D25-5EF358A4878C}",
"post_id": 93009,
"post_name": 1,
"feeder_name": "مهاجر",
"zone_id": 841,
"feeder_id": 105620,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BC3B13BB-562C-4027-8D25-5EF358A4878C}",
"post_id": 93009,
"post_name": 1,
"feeder_name": "کاتب",
"zone_id": 846,
"feeder_id": 105618,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BCA1CDD8-5597-44D6-A12A-65D38E06556F}",
"post_id": 93024,
"post_name": 2,
"feeder_name": "زرينه",
"zone_id": 841,
"feeder_id": 93181,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BCA1CDD8-5597-44D6-A12A-65D38E06556F}",
"post_id": 93024,
"post_name": 2,
"feeder_name": "مطهري",
"zone_id": 834,
"feeder_id": 105481,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BCA1CDD8-5597-44D6-A12A-65D38E06556F}",
"post_id": 93024,
"post_name": 2,
"feeder_name": "وصال",
"zone_id": 841,
"feeder_id": 105476,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BCA1CDD8-5597-44D6-A12A-65D38E06556F}",
"post_id": 93024,
"post_name": 2,
"feeder_name": "عامل",
"zone_id": 834,
"feeder_id": 105475,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BCA1CDD8-5597-44D6-A12A-65D38E06556F}",
"post_id": 93024,
"post_name": 2,
"feeder_name": "شاداب",
"zone_id": 834,
"feeder_id": 105479,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BD95C4AB-2C12-4D11-8C71-DFD11F4AE37F}",
"post_id": 105327,
"post_name": 1,
"feeder_name": "شکیب",
"zone_id": 847,
"feeder_id": 93224,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BEF29EA4-8A0C-4392-8CDC-B8D186D08579}",
"post_id": 105320,
"post_name": 2,
"feeder_name": "نارون",
"zone_id": 837,
"feeder_id": 105523,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BEF29EA4-8A0C-4392-8CDC-B8D186D08579}",
"post_id": 105320,
"post_name": 2,
"feeder_name": "نخل",
"zone_id": 839,
"feeder_id": 105524,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{BEF29EA4-8A0C-4392-8CDC-B8D186D08579}",
"post_id": 105320,
"post_name": 2,
"feeder_name": "راش",
"zone_id": 839,
"feeder_id": 105516,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C573360A-F1FE-4578-AD83-BBD6A7FEE9AA}",
"post_id": 93049,
"post_name": 2,
"feeder_name": "نيلوفر",
"zone_id": 839,
"feeder_id": 93119,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C573360A-F1FE-4578-AD83-BBD6A7FEE9AA}",
"post_id": 93049,
"post_name": 2,
"feeder_name": "اميريه",
"zone_id": 839,
"feeder_id": 93120,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C573360A-F1FE-4578-AD83-BBD6A7FEE9AA}",
"post_id": 93049,
"post_name": 2,
"feeder_name": "کشف",
"zone_id": 839,
"feeder_id": 93121,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C573360A-F1FE-4578-AD83-BBD6A7FEE9AA}",
"post_id": 93049,
"post_name": 2,
"feeder_name": "فيروزه",
"zone_id": 839,
"feeder_id": 93122,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C573360A-F1FE-4578-AD83-BBD6A7FEE9AA}",
"post_id": 93049,
"post_name": 2,
"feeder_name": "حجت",
"zone_id": 839,
"feeder_id": 105399,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C61E6A8C-4E32-4F05-8683-01F6CC0FA7CC}",
"post_id": 93023,
"post_name": 1,
"feeder_name": "دلیجان",
"zone_id": 842,
"feeder_id": 93293,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C61E6A8C-4E32-4F05-8683-01F6CC0FA7CC}",
"post_id": 93023,
"post_name": 1,
"feeder_name": "صادرات",
"zone_id": 842,
"feeder_id": 93299,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C61E6A8C-4E32-4F05-8683-01F6CC0FA7CC}",
"post_id": 93023,
"post_name": 1,
"feeder_name": "نایب",
"zone_id": 842,
"feeder_id": 93301,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C61E6A8C-4E32-4F05-8683-01F6CC0FA7CC}",
"post_id": 93023,
"post_name": 1,
"feeder_name": "امتياز",
"zone_id": 842,
"feeder_id": 93298,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C61E6A8C-4E32-4F05-8683-01F6CC0FA7CC}",
"post_id": 93023,
"post_name": 1,
"feeder_name": "اصناف",
"zone_id": 842,
"feeder_id": 105599,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C81D0D8A-DF5A-4A19-B6FC-63E15F1C8AEC}",
"post_id": 93019,
"post_name": 2,
"feeder_name": "اسفند",
"zone_id": 833,
"feeder_id": 93153,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C81D0D8A-DF5A-4A19-B6FC-63E15F1C8AEC}",
"post_id": 93019,
"post_name": 2,
"feeder_name": "آپادانا",
"zone_id": 833,
"feeder_id": 105450,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C81D0D8A-DF5A-4A19-B6FC-63E15F1C8AEC}",
"post_id": 93019,
"post_name": 2,
"feeder_name": "فلسطين",
"zone_id": 833,
"feeder_id": 93155,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C81D0D8A-DF5A-4A19-B6FC-63E15F1C8AEC}",
"post_id": 93019,
"post_name": 2,
"feeder_name": "آبکوه",
"zone_id": 833,
"feeder_id": 105443,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C81D0D8A-DF5A-4A19-B6FC-63E15F1C8AEC}",
"post_id": 93019,
"post_name": 2,
"feeder_name": "دکترا",
"zone_id": 833,
"feeder_id": 93154,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C9C31EB9-430A-4181-A80B-F2D37177BCDF}",
"post_id": 93011,
"post_name": 1,
"feeder_name": "سخاوت",
"zone_id": 833,
"feeder_id": 105534,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C9C31EB9-430A-4181-A80B-F2D37177BCDF}",
"post_id": 93011,
"post_name": 1,
"feeder_name": "رشادت",
"zone_id": 833,
"feeder_id": 105535,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C9C31EB9-430A-4181-A80B-F2D37177BCDF}",
"post_id": 93011,
"post_name": 1,
"feeder_name": "راوی",
"zone_id": 833,
"feeder_id": 93237,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{C9C31EB9-430A-4181-A80B-F2D37177BCDF}",
"post_id": 93011,
"post_name": 1,
"feeder_name": "اعتماد",
"zone_id": 833,
"feeder_id": 105533,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CD754D07-603B-4F9B-ACC4-629D5FA36D7A}",
"post_id": 105319,
"post_name": 1,
"feeder_name": "دانيال",
"zone_id": 837,
"feeder_id": 93079,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CD754D07-603B-4F9B-ACC4-629D5FA36D7A}",
"post_id": 105319,
"post_name": 1,
"feeder_name": "منور",
"zone_id": 837,
"feeder_id": 105355,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CD754D07-603B-4F9B-ACC4-629D5FA36D7A}",
"post_id": 105319,
"post_name": 1,
"feeder_name": "عقيل",
"zone_id": 837,
"feeder_id": 93081,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CD754D07-603B-4F9B-ACC4-629D5FA36D7A}",
"post_id": 105319,
"post_name": 1,
"feeder_name": "اردلان",
"zone_id": 837,
"feeder_id": 93080,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CD754D07-603B-4F9B-ACC4-629D5FA36D7A}",
"post_id": 105319,
"post_name": 1,
"feeder_name": "سايه",
"zone_id": 836,
"feeder_id": 105354,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CEF7E008-E824-45E5-875C-E06F8A160B9F}",
"post_id": 93055,
"post_name": 1,
"feeder_name": "عدل خميني",
"zone_id": 842,
"feeder_id": 105609,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CEF7E008-E824-45E5-875C-E06F8A160B9F}",
"post_id": 93055,
"post_name": 1,
"feeder_name": "رودکي",
"zone_id": 842,
"feeder_id": 93318,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CEF7E008-E824-45E5-875C-E06F8A160B9F}",
"post_id": 93055,
"post_name": 1,
"feeder_name": "تلاش",
"zone_id": 842,
"feeder_id": 105615,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CEF7E008-E824-45E5-875C-E06F8A160B9F}",
"post_id": 93055,
"post_name": 1,
"feeder_name": "حشمت",
"zone_id": 842,
"feeder_id": 105610,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{CEF7E008-E824-45E5-875C-E06F8A160B9F}",
"post_id": 93055,
"post_name": 1,
"feeder_name": "ناصر",
"zone_id": 833,
"feeder_id": 105611,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D031E6DE-8346-4BB1-93D2-D974F3F8BDDD}",
"post_id": 93056,
"post_name": 1,
"feeder_name": "مهستان",
"zone_id": 843,
"feeder_id": 105559,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D031E6DE-8346-4BB1-93D2-D974F3F8BDDD}",
"post_id": 93056,
"post_name": 1,
"feeder_name": "فاتح",
"zone_id": 843,
"feeder_id": 93255,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D031E6DE-8346-4BB1-93D2-D974F3F8BDDD}",
"post_id": 93056,
"post_name": 1,
"feeder_name": "شهر آباد",
"zone_id": 846,
"feeder_id": 105558,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D031E6DE-8346-4BB1-93D2-D974F3F8BDDD}",
"post_id": 93056,
"post_name": 1,
"feeder_name": "نيروگاه",
"zone_id": 843,
"feeder_id": 93256,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D031E6DE-8346-4BB1-93D2-D974F3F8BDDD}",
"post_id": 93056,
"post_name": 1,
"feeder_name": "ميامي",
"zone_id": 843,
"feeder_id": 105560,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D031E6DE-8346-4BB1-93D2-D974F3F8BDDD}",
"post_id": 93056,
"post_name": 1,
"feeder_name": "کشتارگاه",
"zone_id": 843,
"feeder_id": 93257,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D0AB4C56-D60C-4955-88D9-17447A89F788}",
"post_id": 105329,
"post_name": 2,
"feeder_name": "ديبا",
"zone_id": 837,
"feeder_id": 105356,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D0AB4C56-D60C-4955-88D9-17447A89F788}",
"post_id": 105329,
"post_name": 2,
"feeder_name": "اشکان",
"zone_id": 837,
"feeder_id": 105351,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D0AB4C56-D60C-4955-88D9-17447A89F788}",
"post_id": 105329,
"post_name": 2,
"feeder_name": "آزاده",
"zone_id": 837,
"feeder_id": 93077,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D0AB4C56-D60C-4955-88D9-17447A89F788}",
"post_id": 105329,
"post_name": 2,
"feeder_name": "حرير",
"zone_id": 837,
"feeder_id": 93078,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D0CACE4B-ABE6-4114-9D32-7BF052454C3C}",
"post_id": 93051,
"post_name": 2,
"feeder_name": "صابر",
"zone_id": 847,
"feeder_id": 105509,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D238AD17-E093-4A84-9F77-ED75C00C3A6A}",
"post_id": 93033,
"post_name": 1,
"feeder_name": "وحيد",
"zone_id": 841,
"feeder_id": 93245,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D238AD17-E093-4A84-9F77-ED75C00C3A6A}",
"post_id": 93033,
"post_name": 1,
"feeder_name": "مبين",
"zone_id": 841,
"feeder_id": 93248,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D238AD17-E093-4A84-9F77-ED75C00C3A6A}",
"post_id": 93033,
"post_name": 1,
"feeder_name": "شرکت نفت",
"zone_id": 841,
"feeder_id": 105546,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D238AD17-E093-4A84-9F77-ED75C00C3A6A}",
"post_id": 93033,
"post_name": 1,
"feeder_name": "مجلسي",
"zone_id": 846,
"feeder_id": 105544,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D238AD17-E093-4A84-9F77-ED75C00C3A6A}",
"post_id": 93033,
"post_name": 1,
"feeder_name": "گندم",
"zone_id": 846,
"feeder_id": 105548,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D73E0E14-326B-45A9-A99E-61B0CA14EE15}",
"post_id": 93061,
"post_name": 1,
"feeder_name": "البرز",
"zone_id": 839,
"feeder_id": 93177,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D73E0E14-326B-45A9-A99E-61B0CA14EE15}",
"post_id": 93061,
"post_name": 1,
"feeder_name": "اقبال",
"zone_id": 837,
"feeder_id": 105468,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D73E0E14-326B-45A9-A99E-61B0CA14EE15}",
"post_id": 93061,
"post_name": 1,
"feeder_name": "اميد",
"zone_id": 839,
"feeder_id": 105469,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D73E0E14-326B-45A9-A99E-61B0CA14EE15}",
"post_id": 93061,
"post_name": 1,
"feeder_name": "زاگرس",
"zone_id": 837,
"feeder_id": 93176,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D73E0E14-326B-45A9-A99E-61B0CA14EE15}",
"post_id": 93061,
"post_name": 1,
"feeder_name": "بهاران",
"zone_id": 837,
"feeder_id": 105474,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D9C046E3-3C21-4F12-864A-929B1774A754}",
"post_id": 93054,
"post_name": 2,
"feeder_name": "سرير",
"zone_id": 833,
"feeder_id": 105584,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D9C046E3-3C21-4F12-864A-929B1774A754}",
"post_id": 93054,
"post_name": 2,
"feeder_name": "آبتین",
"zone_id": 833,
"feeder_id": 93279,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D9C046E3-3C21-4F12-864A-929B1774A754}",
"post_id": 93054,
"post_name": 2,
"feeder_name": "عادل",
"zone_id": 833,
"feeder_id": 105582,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D9C046E3-3C21-4F12-864A-929B1774A754}",
"post_id": 93054,
"post_name": 2,
"feeder_name": "فريد",
"zone_id": 833,
"feeder_id": 93277,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D9C046E3-3C21-4F12-864A-929B1774A754}",
"post_id": 93054,
"post_name": 2,
"feeder_name": "برومند",
"zone_id": 833,
"feeder_id": 105585,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{D9C046E3-3C21-4F12-864A-929B1774A754}",
"post_id": 93054,
"post_name": 2,
"feeder_name": "عمران",
"zone_id": 833,
"feeder_id": 93278,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DAF54035-1773-4CDD-8576-D9F9A8B0F0F9}",
"post_id": 105310,
"post_name": 2,
"feeder_name": "رگبار",
"zone_id": 847,
"feeder_id": 93092,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DAF54035-1773-4CDD-8576-D9F9A8B0F0F9}",
"post_id": 105310,
"post_name": 2,
"feeder_name": "بینالود_J23",
"zone_id": 847,
"feeder_id": 93089,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DAF54035-1773-4CDD-8576-D9F9A8B0F0F9}",
"post_id": 105310,
"post_name": 2,
"feeder_name": "پاک",
"zone_id": 847,
"feeder_id": 93093,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DAF54035-1773-4CDD-8576-D9F9A8B0F0F9}",
"post_id": 105310,
"post_name": 2,
"feeder_name": "کولاک",
"zone_id": 847,
"feeder_id": 105367,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DAF54035-1773-4CDD-8576-D9F9A8B0F0F9}",
"post_id": 105310,
"post_name": 2,
"feeder_name": "بینالود_J25",
"zone_id": 847,
"feeder_id": 93088,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DAF54035-1773-4CDD-8576-D9F9A8B0F0F9}",
"post_id": 105310,
"post_name": 2,
"feeder_name": "تگرگ",
"zone_id": 847,
"feeder_id": 93091,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DAF54035-1773-4CDD-8576-D9F9A8B0F0F9}",
"post_id": 105310,
"post_name": 2,
"feeder_name": "ترنم",
"zone_id": 847,
"feeder_id": 119736,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB2E214C-3870-4F35-A170-A669D2F271B1}",
"post_id": 93031,
"post_name": 1,
"feeder_name": "شيرازي",
"zone_id": 848,
"feeder_id": 93353,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB2E214C-3870-4F35-A170-A669D2F271B1}",
"post_id": 93031,
"post_name": 1,
"feeder_name": "گنجينه",
"zone_id": 848,
"feeder_id": 105663,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB2E214C-3870-4F35-A170-A669D2F271B1}",
"post_id": 93031,
"post_name": 1,
"feeder_name": "زائر",
"zone_id": 848,
"feeder_id": 105662,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB2E214C-3870-4F35-A170-A669D2F271B1}",
"post_id": 93031,
"post_name": 1,
"feeder_name": "عتيق",
"zone_id": 848,
"feeder_id": 93354,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB60E46F-9F5E-44CA-BCEE-DC4EB822E33D}",
"post_id": 93027,
"post_name": 2,
"feeder_name": "منیر",
"zone_id": 847,
"feeder_id": 105408,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB60E46F-9F5E-44CA-BCEE-DC4EB822E33D}",
"post_id": 93027,
"post_name": 2,
"feeder_name": "نورانی",
"zone_id": 847,
"feeder_id": 105407,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB60E46F-9F5E-44CA-BCEE-DC4EB822E33D}",
"post_id": 93027,
"post_name": 2,
"feeder_name": "خورشید",
"zone_id": 843,
"feeder_id": 105406,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB60E46F-9F5E-44CA-BCEE-DC4EB822E33D}",
"post_id": 93027,
"post_name": 2,
"feeder_name": "نورافشان",
"zone_id": 847,
"feeder_id": 93129,
"zone_name": "ملک آباد",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DB60E46F-9F5E-44CA-BCEE-DC4EB822E33D}",
"post_id": 93027,
"post_name": 2,
"feeder_name": "تابان",
"zone_id": 843,
"feeder_id": 93126,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "بنیان",
"zone_id": 833,
"feeder_id": 105382,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "نواب",
"zone_id": 833,
"feeder_id": 93103,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "دیانت",
"zone_id": 834,
"feeder_id": 105374,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "عرفان",
"zone_id": 833,
"feeder_id": 105385,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "آرین",
"zone_id": 833,
"feeder_id": 105381,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "افشين",
"zone_id": 833,
"feeder_id": 105386,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "سعيد",
"zone_id": 834,
"feeder_id": 93102,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "حميد",
"zone_id": 834,
"feeder_id": 105380,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DBFFE30E-7DE1-443C-B973-8BA075224CEF}",
"post_id": 105323,
"post_name": 1,
"feeder_name": "بهرام",
"zone_id": 833,
"feeder_id": 93106,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DC9DF94E-840C-4899-A179-A65DA35376A9}",
"post_id": 105326,
"post_name": 1,
"feeder_name": "کوشش",
"zone_id": 837,
"feeder_id": 93363,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DC9DF94E-840C-4899-A179-A65DA35376A9}",
"post_id": 105326,
"post_name": 1,
"feeder_name": "دادگر",
"zone_id": 837,
"feeder_id": 93369,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DC9DF94E-840C-4899-A179-A65DA35376A9}",
"post_id": 105326,
"post_name": 1,
"feeder_name": "ماهان",
"zone_id": 837,
"feeder_id": 105675,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DC9DF94E-840C-4899-A179-A65DA35376A9}",
"post_id": 105326,
"post_name": 1,
"feeder_name": "صدف",
"zone_id": 837,
"feeder_id": 93367,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DC9DF94E-840C-4899-A179-A65DA35376A9}",
"post_id": 105326,
"post_name": 1,
"feeder_name": "دهنو",
"zone_id": 836,
"feeder_id": 105674,
"zone_name": "برق 10",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DC9DF94E-840C-4899-A179-A65DA35376A9}",
"post_id": 105326,
"post_name": 1,
"feeder_name": "ويلا",
"zone_id": 838,
"feeder_id": 93368,
"zone_name": "برق 11",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DD2B1842-6C93-4464-92E3-1E1124F830C8}",
"post_id": 93020,
"post_name": 1,
"feeder_name": "سروش",
"zone_id": 839,
"feeder_id": 105592,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DD2B1842-6C93-4464-92E3-1E1124F830C8}",
"post_id": 93020,
"post_name": 1,
"feeder_name": "فرخ",
"zone_id": 837,
"feeder_id": 105595,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DD2B1842-6C93-4464-92E3-1E1124F830C8}",
"post_id": 93020,
"post_name": 1,
"feeder_name": "چهار فصل",
"zone_id": 839,
"feeder_id": 105593,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{DD2B1842-6C93-4464-92E3-1E1124F830C8}",
"post_id": 93020,
"post_name": 1,
"feeder_name": "شبنم",
"zone_id": 837,
"feeder_id": 105591,
"zone_name": "برق 7",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E0C8A6EC-17F2-4587-B8D0-90A12D7D83D9}",
"post_id": 105325,
"post_name": 1,
"feeder_name": "کهن دشت",
"zone_id": 843,
"feeder_id": 105424,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E0C8A6EC-17F2-4587-B8D0-90A12D7D83D9}",
"post_id": 105325,
"post_name": 1,
"feeder_name": "آبروان",
"zone_id": 843,
"feeder_id": 105423,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E171DEF7-D66F-45B9-A7B3-08326C65F25B}",
"post_id": 105309,
"post_name": 1,
"feeder_name": "ارجمند",
"zone_id": 834,
"feeder_id": 93228,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E171DEF7-D66F-45B9-A7B3-08326C65F25B}",
"post_id": 105309,
"post_name": 1,
"feeder_name": "طنین",
"zone_id": 834,
"feeder_id": 105525,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E171DEF7-D66F-45B9-A7B3-08326C65F25B}",
"post_id": 105309,
"post_name": 1,
"feeder_name": "آذر",
"zone_id": 834,
"feeder_id": 105531,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E171DEF7-D66F-45B9-A7B3-08326C65F25B}",
"post_id": 105309,
"post_name": 1,
"feeder_name": "امين",
"zone_id": 834,
"feeder_id": 93230,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E171DEF7-D66F-45B9-A7B3-08326C65F25B}",
"post_id": 105309,
"post_name": 1,
"feeder_name": "زحل",
"zone_id": 834,
"feeder_id": 93229,
"zone_name": "برق 4",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E171DEF7-D66F-45B9-A7B3-08326C65F25B}",
"post_id": 105309,
"post_name": 1,
"feeder_name": "ناجی",
"zone_id": 853,
"feeder_id": 93231,
"zone_name": "توس",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E2EECFDF-709A-4B05-9451-86D00D804155}",
"post_id": 93046,
"post_name": 1,
"feeder_name": "پژمان",
"zone_id": 833,
"feeder_id": 93275,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E2EECFDF-709A-4B05-9451-86D00D804155}",
"post_id": 93046,
"post_name": 1,
"feeder_name": "اسحاق",
"zone_id": 833,
"feeder_id": 93276,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E2EECFDF-709A-4B05-9451-86D00D804155}",
"post_id": 93046,
"post_name": 1,
"feeder_name": "ايرج",
"zone_id": 833,
"feeder_id": 93273,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E2EECFDF-709A-4B05-9451-86D00D804155}",
"post_id": 93046,
"post_name": 1,
"feeder_name": "همایون",
"zone_id": 833,
"feeder_id": 105581,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E2EECFDF-709A-4B05-9451-86D00D804155}",
"post_id": 93046,
"post_name": 1,
"feeder_name": "مسعود",
"zone_id": 833,
"feeder_id": 93274,
"zone_name": "برق 3",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E85C087D-7143-4955-B798-07A343ACC777}",
"post_id": 105335,
"post_name": 1,
"feeder_name": "صفوي",
"zone_id": 846,
"feeder_id": 93085,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E85C087D-7143-4955-B798-07A343ACC777}",
"post_id": 105335,
"post_name": 1,
"feeder_name": "شهيد صدر",
"zone_id": 842,
"feeder_id": 93086,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E85C087D-7143-4955-B798-07A343ACC777}",
"post_id": 105335,
"post_name": 1,
"feeder_name": "گوهرشاد",
"zone_id": 848,
"feeder_id": 105360,
"zone_name": "برق 8",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E85C087D-7143-4955-B798-07A343ACC777}",
"post_id": 105335,
"post_name": 1,
"feeder_name": "محمد آباد",
"zone_id": 842,
"feeder_id": 105359,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E85C087D-7143-4955-B798-07A343ACC777}",
"post_id": 105335,
"post_name": 1,
"feeder_name": "هفده شهريور",
"zone_id": 842,
"feeder_id": 105361,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{E85C087D-7143-4955-B798-07A343ACC777}",
"post_id": 105335,
"post_name": 1,
"feeder_name": "شيرودي",
"zone_id": 842,
"feeder_id": 105358,
"zone_name": "برق 2",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{EB557716-6088-40F4-8050-C958010AFE24}",
"post_id": 105307,
"post_name": 1,
"feeder_name": "سياوش",
"zone_id": 839,
"feeder_id": 105393,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{EB557716-6088-40F4-8050-C958010AFE24}",
"post_id": 105307,
"post_name": 1,
"feeder_name": "گودرز",
"zone_id": 853,
"feeder_id": 93118,
"zone_name": "توس",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{EB557716-6088-40F4-8050-C958010AFE24}",
"post_id": 105307,
"post_name": 1,
"feeder_name": "جمشيد",
"zone_id": 839,
"feeder_id": 93113,
"zone_name": "برق 9",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{EB557716-6088-40F4-8050-C958010AFE24}",
"post_id": 105307,
"post_name": 1,
"feeder_name": "خسرو",
"zone_id": 853,
"feeder_id": 105398,
"zone_name": "توس",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DC5242-E392-4884-A1D1-712D3001B90A}",
"post_id": 93026,
"post_name": 2,
"feeder_name": "شبديز",
"zone_id": 835,
"feeder_id": 93175,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DC5242-E392-4884-A1D1-712D3001B90A}",
"post_id": 93026,
"post_name": 2,
"feeder_name": "علوم",
"zone_id": 835,
"feeder_id": 105465,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DC5242-E392-4884-A1D1-712D3001B90A}",
"post_id": 93026,
"post_name": 2,
"feeder_name": "آزادگان",
"zone_id": 835,
"feeder_id": 105462,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DC5242-E392-4884-A1D1-712D3001B90A}",
"post_id": 93026,
"post_name": 2,
"feeder_name": "سعدي",
"zone_id": 835,
"feeder_id": 93172,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DC5242-E392-4884-A1D1-712D3001B90A}",
"post_id": 93026,
"post_name": 2,
"feeder_name": "پژوهش",
"zone_id": 835,
"feeder_id": 105461,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DDA92F-17CE-4BD4-94FA-D87A25B544FC}",
"post_id": 105324,
"post_name": 2,
"feeder_name": "بلال",
"zone_id": 841,
"feeder_id": 93163,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DDA92F-17CE-4BD4-94FA-D87A25B544FC}",
"post_id": 105324,
"post_name": 2,
"feeder_name": "جامع الصادق",
"zone_id": 856,
"feeder_id": 105452,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DDA92F-17CE-4BD4-94FA-D87A25B544FC}",
"post_id": 105324,
"post_name": 2,
"feeder_name": "گلشاد",
"zone_id": 841,
"feeder_id": 93160,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DDA92F-17CE-4BD4-94FA-D87A25B544FC}",
"post_id": 105324,
"post_name": 2,
"feeder_name": "بلوارخواجه ربيع",
"zone_id": 841,
"feeder_id": 105453,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F0DDA92F-17CE-4BD4-94FA-D87A25B544FC}",
"post_id": 105324,
"post_name": 2,
"feeder_name": "کریم",
"zone_id": 853,
"feeder_id": 105455,
"zone_name": "توس",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F5CE4444-8C66-4AF4-98CA-C7B60285B702}",
"post_id": 105315,
"post_name": 2,
"feeder_name": "ژرف",
"zone_id": 841,
"feeder_id": 93309,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F5CE4444-8C66-4AF4-98CA-C7B60285B702}",
"post_id": 105315,
"post_name": 2,
"feeder_name": "شناور",
"zone_id": 841,
"feeder_id": 93306,
"zone_name": "برق 1",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F5CE4444-8C66-4AF4-98CA-C7B60285B702}",
"post_id": 105315,
"post_name": 2,
"feeder_name": "برکه",
"zone_id": 856,
"feeder_id": 93308,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{F5CE4444-8C66-4AF4-98CA-C7B60285B702}",
"post_id": 105315,
"post_name": 2,
"feeder_name": "جزیره",
"zone_id": 856,
"feeder_id": 105600,
"zone_name": "مهرگان",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FE0BAAE8-29AB-44DC-BCDD-47EE291BDDBC}",
"post_id": 93044,
"post_name": 2,
"feeder_name": "پامچال",
"zone_id": 835,
"feeder_id": 93067,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FE0BAAE8-29AB-44DC-BCDD-47EE291BDDBC}",
"post_id": 93044,
"post_name": 2,
"feeder_name": "گلديس",
"zone_id": 835,
"feeder_id": 105341,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FE0BAAE8-29AB-44DC-BCDD-47EE291BDDBC}",
"post_id": 93044,
"post_name": 2,
"feeder_name": "چهار چشمه",
"zone_id": 835,
"feeder_id": 93065,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FE0BAAE8-29AB-44DC-BCDD-47EE291BDDBC}",
"post_id": 93044,
"post_name": 2,
"feeder_name": "صحاب",
"zone_id": 835,
"feeder_id": 93071,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FE0BAAE8-29AB-44DC-BCDD-47EE291BDDBC}",
"post_id": 93044,
"post_name": 2,
"feeder_name": "شهرستانی",
"zone_id": 835,
"feeder_id": 105342,
"zone_name": "برق 5",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FF72127D-134B-4FBF-9225-36E77C1579B2}",
"post_id": 105303,
"post_name": 2,
"feeder_name": "تحقق",
"zone_id": 843,
"feeder_id": 93192,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FF72127D-134B-4FBF-9225-36E77C1579B2}",
"post_id": 105303,
"post_name": 2,
"feeder_name": "بهبود",
"zone_id": 843,
"feeder_id": 93193,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FF72127D-134B-4FBF-9225-36E77C1579B2}",
"post_id": 105303,
"post_name": 2,
"feeder_name": "پایداری",
"zone_id": 846,
"feeder_id": 105492,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FF72127D-134B-4FBF-9225-36E77C1579B2}",
"post_id": 105303,
"post_name": 2,
"feeder_name": "عظمت",
"zone_id": 846,
"feeder_id": 105493,
"zone_name": "برق 6",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
},
{
"guid": "{FF72127D-134B-4FBF-9225-36E77C1579B2}",
"post_id": 105303,
"post_name": 2,
"feeder_name": "توسعه",
"zone_id": 843,
"feeder_id": 93194,
"zone_name": "رضویه",
"address": null,
"feeder_bar": 0,
"sub_zone": "",
"path": ""
}
],
"total": 420,
"status": 200
}';
}

function replace_key($arr, $oldkey, $newkey)
{
    if (array_key_exists($oldkey, $arr)) {
        $keys = array_keys($arr);
        $keys[array_search($oldkey, $keys)] = $newkey;
        return array_combine($keys, $arr);
    }
    return $arr;
}

function flash($msg, $status = 0)
{
    if ($status) {
        Session::flash('alert-class', 'alert-success');
        Session::flash('message', $msg);
    } else {
        Session::flash('alert-class', 'alert-danger');
        Session::flash('message', $msg);
    }
}

function verta_date_formatter($date)
{
    if (!$date instanceof Verta)
        $date = Verta::parse($date);

    $replace = str_replace('-', '/', Verta::persianNumbers($date->formatDate()));
    return $replace;
}

function verta_date_time_formatter($datetime)
{
    if (!$datetime instanceof Verta)
        $datetime = Verta::parse($datetime);
    $replace = str_replace('-', '/', Verta::persianNumbers($datetime->formatJalaliDatetime()));
    return $replace;
}

function en2fa($number)
{
    $number = str_replace("1", "۱", $number);
    $number = str_replace("2", "۲", $number);
    $number = str_replace("3", "۳", $number);
    $number = str_replace("4", "۴", $number);
    $number = str_replace("5", "۵", $number);
    $number = str_replace("6", "۶", $number);
    $number = str_replace("7", "۷", $number);
    $number = str_replace("8", "۸", $number);
    $number = str_replace("9", "۹", $number);
    $number = str_replace("0", "۰", $number);
    return $number;
}

function fa2en($string)
{
    return strtr($string, array('۰' => '0', '۱' => '1', '۲' => '2', '۳' => '3', '۴' => '4', '۵' => '5', '۶' => '6', '۷' => '7', '۸' => '8', '۹' => '9', '٠' => '0', '١' => '1', '٢' => '2', '٣' => '3', '٤' => '4', '٥' => '5', '٦' => '6', '٧' => '7', '٨' => '8', '٩' => '9'));
}

function day_name($string)
{
    return strtr($string, array(
        '0' => 'شنبه', '1' => 'یکشنبه', '2' => 'دوشنبه',
        '3' => 'سه شنبه', '4' => 'چهارشنبه', '5' => 'پنجشنبه', '6' => 'جمعه'
    ));
}

function dates_range_array_old($start_date, $end_date, bool $format = true)
{

    $start_date = Verta::parse($start_date);
    $end_date = Verta::parse($end_date);
    $diff = $start_date->diffDays($end_date);
    $dates = [$format ? verta_date_formatter($start_date) : $start_date->formatDate()];
    for ($i = 0; $i < $diff; $i++) {
        array_push(
            $dates,
            $format ? verta_date_formatter($start_date->addDay(1)) : $start_date->addDay(1)->formatDate()
        );
    }

    return $dates;
}

function dates_range_array($start_date, $end_date, bool $format = true)
{

    $period = \Carbon\CarbonPeriod::create($start_date, $end_date);
    $period = $period->toArray();
    foreach ($period as $k => $date) {
        $period[$k] = $date->format('Y-m-d');
    }
    $dates = $period;
    return $dates;
}

function which_day($date)
{
    $date = Carbon::createFromDate($date);
    $v = Verta($date);
    $v = new Verta($v);
    return day_name($v->dayOfWeek);
}

function engReq(Request $request)
{
    $new_request = $request;
    foreach ($new_request->except(['user', '_token']) as $k => $v) {
        $new_request[$k] = fa2en($v);
    }
    return $new_request;
}

function time_for_off_list($period)
{
    $times = str_replace(' ', '', explode('-', $period));
    $start_time = str_replace(' ', '', explode(':', $times[0]));
    $end_time = str_replace(' ', '', explode(':', $times[1]));
    $times_array = ['01', '02', '03', '04', '05', '06', '07', '08', '09'];
    if (in_array($start_time[0], $times_array)) {
        $start_time = array_search($start_time[0], $times_array) + 1;
    } else
        $start_time = $start_time[0];
    if (in_array($end_time[0], $times_array)) {
        $end_time = array_search($end_time[0], $times_array) + 1;
    } else
        $end_time = $end_time[0];
    return [$start_time, $end_time];
}


function time_range($start_time, $end_time, $period)
{
    $start_time = Carbon::createFromTimeString($start_time);
    $times = [];
    $end_time = Carbon::createFromTimeString($end_time);
    while ($start_time->lt($end_time)) {
        $tmp = $start_time->format('H:i');
        $end_period = $start_time->addMinute($period);
        if ($end_period->gt($end_time)) {
            $end_period = $end_time;
            array_push($times, $tmp . ' - ' . $end_period->format('H:i'));
            break;
        }
        array_push($times, $tmp . ' - ' . $end_period->format('H:i'));
    }
    return $times;
}
function time_range_bar_manage($start_time, $end_time)
{
    $start_time = Carbon::createFromTimeString($start_time);
    $times = [];
    $end_time = Carbon::createFromTimeString($end_time);
    while ($start_time->lte($end_time)) {
        $tmp = $start_time->format('H');
        $end_period = $start_time->addHour(1);
        if ($end_period->gt($end_time)) {
            array_push($times, $tmp == '00' ? '24' : $tmp);
            break;
        }
        array_push($times, $tmp == '00' ? '24' : $tmp);
    }
    return $times;
}

function getPercentOfNumber($number, $percent)
{
    return round(($percent / 100) * $number);
}

function change_0_to_24($time)
{
    $time = str_replace(' ', '', explode(':', $time));
    if ($time[0] == '0' or $time == '00') {
        $time[0] = '24';
    }
    $time = implode(':', $time);
    return $time;
}

function persian_status($status)
{
    switch ($status) {
        case 'deactive':
            return 'کامل نشده';
            break;
        case 'active':
            return 'اعمال نشده';
            break;
        case 'applied':
            return 'فعال';
            break;
        case 'non_applied':
            return 'غیر فعال';
            break;
        default:
            return 'تعیین نشده';
    }
}

function status_class($status)
{
    switch ($status) {
        case 'deactive':
            return 'danger';
            break;
        case 'active':
            return 'warning';
            break;
        case 'applied':
            return 'success';
            break;
        case 'non_applied':
            return 'dark';
            break;
        default:
            return 'تعیین نشده';
    }
}

function check_active_table()
{
    $off_tables = OffTable::where('status', '<>', 'deactive')->latest()->get();
    $flag = false;
    foreach ($off_tables as $ot) {
        $dates = dates_range_array($ot->start_date, $ot->end_date);
        $today = Carbon::today('Asia/Tehran')->format('Y-m-d');
        if ($flag and $ot->status == 'applied') {
            $ot->status = 'non_applied';
            $ot->save();
            continue;
        } else if (in_array($today, $dates) and $flag == false) {
            $flag = true;
            $ot->status = 'applied';
            $ot->save();
        } else {
            $ot->status = 'non_applied';
            $ot->save();
        }
    }
    $off_tables = OffTable::latest()->get();
    return $off_tables;
}


function user()
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // set time limit for connection time out
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100); //timeout in seconds
    set_time_limit(10);
    curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        array(
            "Cookie: RAYASESSID=" . (isset($_COOKIE["RAYASESSID"]) ? $_COOKIE["RAYASESSID"] : '')
        )
    );
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    $sys = 'esb';
    $dir = 'raya_portal';
    $url = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . "/$dir/$sys/rest/common/user";
    //var_dump($url);die;
    curl_setopt($ch, CURLOPT_URL, $url);
    $output = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);
    if ($httpcode != 200) {
        return false;
    }
    return $output == 'false' ? false : json_decode($output, true);
}

function tmp_user(Request $request)
{
    //    $cookie = $request->cookie('RAYASESSID');
    $cookie = 'd42d6cd6cee7abaac80523fa02182a317eb2fe8d';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        array(
            "Cookie: RAYASESSID=" . (isset($cookie) ? $cookie : '')
        )
    );

    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    //    $url = 'http://178.236.40.31:3468/develop/rest/common/user';
    $url = 'http://178.236.40.31:3468/develop/ees121/rest/common/user';
    curl_setopt($ch, CURLOPT_URL, $url);
    $output = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);
    if ($httpcode != 200) {
        return false;
    }
    return $output == 'false' ? false : json_decode($output, true);
}

function tmp_access_keys()
{
    return [
        "base_data" => 1,
        "create_off_list" => 1,
        "observe_off_list" => 14,
        "view_map" => 1,
    ];
}

function check_access_list($request)
{
    $uri = $request->getRequestUri();
    $user = $request->user;
    if (!$user->access_keys and $uri == '/') {
        flash('شما به این سرویس دسترسی ندارید.');
        return true;
    } elseif (
        $user->access_keys['base_data'] == 0 and (strpos($uri, '/panel/detParams') !== false or
            strpos($uri, '/panel/feedersStatus') !== false or
            strpos($uri, '/panel/barManageConsumption') !== false)
    ) {
        flash('شما به این بخش دسترسی ندارید.');
        return false;
    } elseif (
        $user->access_keys['create_off_list'] == 0 and
        strpos($uri, '/panel/createOffList') !== false
    ) {
        flash('شما به این بخش دسترسی ندارید.');
        return false;
    } elseif (
        $user->access_keys['observe_off_list'] == 0 and
        strpos($uri, '/panel/viewOffList') !== false
    ) {
        flash('شما به این بخش دسترسی ندارید.');
        return false;
    }

    return true;
}

function call_121($action, $feeder_id, $off_time, $off_brunt, $description = '')
{
    $url = env('121_API');
    if ($action == 'on')
        $fields = [
            'feeder_id ' => $feeder_id,
            'off_time' => $off_time,
            'off_brunt' => $off_brunt,
            'description' => $description
        ];
    else
        $fields = [
            'feeder_id ' => $feeder_id,
            'off_time' => $off_time,
            'off_brunt' => $off_brunt,
        ];

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $fields,
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($response, true);
    if ($result and array_key_exists('success', $result ?? []) and $result['success'] == true)
        return true;
    return false;
}

function get_geometry($f_guid, $data = false)
{
    $url = env('GEOMETRY_URL');
    $fields = [
        'feeder' => $f_guid,
    ];

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $fields,
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($response, true);
    if ($data) {
        return json_encode($result['data']);
    } elseif ($result and $result['status'] == 200 and $result['data'])
        return $result['data']['coordinates'];
    return false;
}

function get_custid($f_guid)
{
    $url = env('GET_CUSTID_URL');
    $fields = [
        'fider_global' => $f_guid,
    ];


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $fields,
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($response, true);
    if ($result and $result['status'] == 200 and $result['data'])
        return $result['data'];
    return false;
}


function get_feeder($cust_id)
{
    $url = env('GET_FEEDER_URL');
    $fields = [
        'cust_id' => $cust_id,
    ];


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $fields,
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($response, true);
    if ($result and $result['status'] == 200 and $result['data'])
        return $result['data'];
    return false;
}


function call_avanak($f_guid, $message_id, $lid)
{
    $url = env('CALL_AVANAK_URL');
    $fields = [
        'feeder' => $f_guid,
        'messageId' => $message_id,
        'localId' => $lid,
    ];


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $fields,
    ));

    $response = curl_exec($curl);
    $result = json_decode($response, true);
    if ($result and $result['status'] == 200 and $result['success'] == 'true')
        return $result['data'];
    return false;
}

function dfa2den($p_date)
{
    $v = Verta::parse($p_date);
    return $v->formatGregorian('Y-m-d');
}

function den2dfa($e_date)
{
    $e_date = Carbon::createFromDate($e_date);
    $v = Verta($e_date);
//    $v = Verta::parse($e_date);
//    $v = Verta::create($v->year, $v->month, $v->day);
    return $v->formatJalaliDate();
}

function change_req_date_to_en(Request $request)
{
    if ($request->has('date')) {
        if (is_array($request->date)) {
//            $dates = [];
//            foreach ($request->date as $d) {
//                array_push($dates, dfa2den(fa2en($d)));
//            }
//            $request['date'] = $dates;
        } else {
            $request['date'] = fa2en($request->date);
            $request['date'] = dfa2den($request->date);
        }

    }
    if ($request->has('start_date')) {
        $request['start_date'] = fa2en($request->start_date);
        $request['start_date'] = dfa2den($request->start_date);
    }
    if ($request->has('end_date')) {
        $request['end_date'] = fa2en($request->end_date);
        $request['end_date'] = dfa2den($request->end_date);
    }
    return $request;
}

function change_req_date_to_fa(Request $request)
{
    if ($request->has('date')) {
        $request['date'] = den2dfa($request->date);
//        $request['date'] = en2fa($request->date);
    }
    if ($request->has('start_date')) {
        $request['start_date'] = den2dfa($request->start_date);
//        $request['start_date'] = en2fa($request->start_date);
    }
    if ($request->has('end_date')) {
        $request['end_date'] = den2dfa($request->end_date);
//        $request['end_date'] = en2fa($request->end_date);
    }
    return $request;
}

function roundToNearestMinuteInterval(\DateTime $dateTime, $minuteInterval = 15)
{
    return $dateTime->setTime(
        $dateTime->format('H'),
        round($dateTime->format('i') / $minuteInterval) * $minuteInterval,
        0
    );
}