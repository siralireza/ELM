<?php

namespace App;

class User
{
    public $user_id;
    public $username;
    public $first_name;
    public $last_name;
    public $code;
    public $rank;
    public $access_keys;
}
