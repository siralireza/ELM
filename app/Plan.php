<?php

namespace App;

class Plan extends MotherModel
{
    public function MC()
    {
        return $this->belongsTo(ManageConsumption::class, 'plan_id', 'id');
    }
}
