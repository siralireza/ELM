<?php

namespace App;

class ManageConsumption extends MotherModel
{
    protected $table = 'manage_consumption';

    public function Plan()
    {
        return $this->hasOne(Plan::class, 'id', 'plan_id');
    }
}
