<?php

namespace App;

class FeederBar extends MotherModel
{
    protected $table = 'feeders_bar';

    public function Feeder()
    {
        return $this->belongsTo(Feeder::class, 'feeder_id', 'id');
    }
}
