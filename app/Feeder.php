<?php

namespace App;

class Feeder extends MotherModel
{
    public function Bar()
    {
        return $this->hasOne(FeederBar::class);
    }
    public function LimitedPartners()
    {
        return $this->hasMany(FeederLimitedPartners::class);
    }
    public function Contributers()
    {
        return $this->hasMany(Contributers::class);
    }
    public function OffSch()
    {
        return $this->hasMany(FeederOffSchedule::class);
    }
}
