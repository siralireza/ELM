<?php

namespace App\Http\Controllers;

use App\Feeder;
use App\FeederBar;
use App\FeederOffSchedule;
use App\ManageConsumption;
use App\OffTable;
use App\OffTableItem;
use App\Setting;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class createOffListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $off_tables = check_active_table();
        return view('panel_items/off_list/create_form_off_list',
            [
                'page_name' => 'ایجاد جدول خاموشی',
                'off_tables' => $off_tables,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $off_table = OffTable::where('id', $request->off_table)->first();
        if (!$off_table) {
            flash('جدول مورد  نظر یافت نشد', 0);
            return redirect('panel/createOffList');
        }
        $off_table->status = 'active';
        $off_table->save();
        OffTable::where('status', 'deactive')->delete();
        Session::flash('alert-class', 'alert-success');
        Session::flash('message', 'جدول خاموشی با موفقیت ساخته شد');
        return redirect('/panel/createOffList');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function createForm(Request $request)
    {
        $new_request = engReq($request);
        $new_request->validate([
            "start_date" => "required",
            "end_date" => "required",
            "start_time" => "required",
            "end_time" => "required",
            "period" => "required|integer",
            "confirm" => "required|boolean",
        ]);
        $s_date = Carbon::createFromDate($new_request->start_date);
        $e_date = Carbon::createFromDate($new_request->end_date);
        $s_time = roundToNearestMinuteInterval(Carbon::createFromTimeString($request->start_time));
        $e_time = roundToNearestMinuteInterval(Carbon::createFromTimeString($request->end_time));
        if ($request->start_time == $request->end_time) {
            flash('ساعت شروع و پایان نمیتواند یکسان باشد', 0);
            return redirect()->back();
        } else if ($s_time > $e_time) {
            flash('ساعت شروع باید از ساعت پایان کمتر باشد', 0);
            return redirect()->back();
        }
        // todo uncomment this later
//        else if ($s_date->lte(Verta::today())) {
//            flash('تاریخ شروع باید از فردا انتخاب شود', 0);
//            return redirect()->back();
//        }
        else if ($e_date->lt($s_date)) {
            flash('تاریخ پایان باید بیشتر از تاریخ شروع و یا برابر با آن باشد', 0);
            return redirect()->back();
        }
        // check in old off tables or not
        if (!$request->confirm) {
            $off_tables_dates_range = [];
            $off_tables = OffTable::latest()->get();
            foreach ($off_tables as $ot) {
                $range = dates_range_array($ot->start_date, $ot->end_date);
//            dd($range);
                $off_tables_dates_range = array_merge($off_tables_dates_range, $range);
            }
            $off_tables_dates_range = array_unique($off_tables_dates_range);
            if (in_array($request->start_date, $off_tables_dates_range) or in_array($request->end_date, $off_tables_dates_range)) {
                return view('panel_items/off_list/confirm_create_off_table', [
                    'page_name' => 'تایید ایجاد جدول',
                ]);
            }
        }
        $s_time = $s_time->format('H:i');
        $e_time = $e_time->format('H:i');
        $times = time_range($s_time, $e_time, $new_request->period);
        $dates = dates_range_array($new_request->start_date, $new_request->end_date);
        if (count($dates) != ManageConsumption::whereIn('date', $dates)->distinct('date')->pluck('date')->count()) {
            flash('اطلاعات مدیریت مصرف برای بازه مشخص شده کامل نیست', 0);
            return redirect()->back();
        }

        $new_request['user_id'] = $request->user->user_id;
        $new_request['status'] = 'deactive';
        $new_request['start_time'] = $s_time;
        $new_request['end_time'] = $e_time;
        $off_table = OffTable::create($new_request->except(['user', 'confirm']));


        $mc = ManageConsumption::whereIn('date', $dates)->get();

        return view('panel_items/off_list/store_form_off_list',
            [
                'page_name' => 'ایجاد جدول خاموشی',
                'times' => $times,
                'dates' => $dates,
                'off_table' => $off_table,
                'mc' => $mc,
            ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'off_table_id' => 'required|exists:off_tables,id',
            'tavanir_req' => 'required|array',
            'tavanir_req.*' => 'integer',
            'date' => 'required|array',
        ]);
        $tav_req = $request->tavanir_req;
        $off_table = OffTable::where('id', $request->off_table_id)->first();
        $times = time_range($off_table->start_time, $off_table->end_time, $off_table->period);
        if ((count($tav_req) / count($request->date)) != count($times)) {
            flash('مشکلی در درخواست وجود دارد', $times);
            return redirect('panel/createOffList');
        }
        $tolerance = Setting::where('name', 'tolerance')->latest()->first();
        $days = Setting::where('name', 'days')->latest()->first();
//        if (!$days and !$tolerance) {
//            Session::flash('alert-class', 'alert-warning');
//            Session::flash('message', 'مقدار تلورانس و تعداد روز ها برای خاموشی فیدرتعیین نشده است و صفر در نظر گرفته شد');
//            $tolerance = 0;
//        }
//        else if (!$days) {
//            Session::flash('alert-class', 'alert-warning');
//            Session::flash('message', 'تعداد روز ها برای خاموشی فیدر تعیید نشده است و صفر در نظر گرفته شد');
//            $days = 0;
//        }
        if (!$tolerance) {
            Session::flash('alert-class', 'alert-warning');
            Session::flash('message', 'مقدار تلورانس تعیین نشده است و صفر در نظر گرفته شد');
            $days = 0;
        } else {

            $tolerance = $tolerance->value;
            $days = $days->value;
        }
        $versions = Feeder::orderBy('history_version')->distinct('history_version')->pluck('history_version');
        $last_version = $versions->last() ?? 0;
        //delete old feeder schedule
        FeederOffSchedule::where('off_table_id', $off_table->id)->delete();
        // chunks the tavanir request to dates
        $chunked = array_chunk($tav_req, count($times));
        foreach ($chunked as $k => $v) {
            $dt = $request->date[$k];
            $mc = ManageConsumption::where('date', $dt);
            // foreach on each tavanir request and calculat the allowed off
            foreach ($v as $t => $r) {
                $tms = time_for_off_list($times[$t]);
                $tms[0] = change_0_to_24($tms[0]);
                $tms[1] = change_0_to_24($tms[1]);
                if ($tms[0] == $tms[1]) {
                    $mc_val = $mc->where('date', $dt)
                        ->pluck('hour_' . $tms[0])->sum();
                } else {
                    $q = $mc->where('date', $dt);
                    $val1 = $q->pluck('hour_' . $tms[0])->sum();
                    $val2 = $q->pluck('hour_' . $tms[1])->sum();
                    $mc_val = max($val1, $val2);
                }

                $allowed_off = $r - $mc_val > 0 ? $r - $mc_val : 0;
                $period = str_replace(' ', '', explode('-', $times[$t]));
                $start_time = $period[0];
                $end_time = $period[1];
                if (!$allowed_off)
                    continue;
                //adding off tables items
                OffTableItem::updateOrCreate([
                    'off_table_id' => $off_table->id,
                    'date' => $dt,
                    'start_time' => $start_time,
                ], [
                    'user_id' => $request->user->user_id,
                    'off_table_id' => $off_table->id,
                    'date' => $dt,
                    'start_time' => $start_time,
                    'predict_tavanir_request' => $r,
                    'manage_consumption' => $mc_val,
                    'allowed_off_value' => $allowed_off,
                ]);

                $tolerance = getPercentOfNumber($allowed_off, $tolerance);
                // $feeders_offed_soon = FeederOffSchedule::where('date', '>', Verta::parse($dt)->subDays($days))
                //     ->where('off_table_id', $off_table->id)
                //     ->pluck('feeder_id');
                // empty array instead of up query (check days in off)
                $feeders_offed_soon = [];
                $feeders_offed_soon = FeederOffSchedule::where('off_table_id', $off_table->id)
                    ->pluck('feeder_id');
                $max_allowed = $allowed_off + $tolerance;
                $feeder_with_good_bar = FeederBar::with('Feeder')->whereNotIn('feeder_id', $feeders_offed_soon)->whereHas('Feeder', function ($q) use ($last_version) {
                    $q->where(['breakable' => true, 'history_version' => $last_version]);
                })->where([['hour_' . $tms[0], '<=', $max_allowed], ['hour_' . $tms[0], '>', 0]])->orderBy('hour_' . $tms[0], 'desc')->get();
                // foreach on feeders with good bar
                $loop = 0;

                if ($feeder_with_good_bar->isEmpty()) {
                    $feeder_with_good_bar = FeederBar::with('Feeder')->whereNotIn('feeder_id', $feeders_offed_soon)->whereHas('Feeder', function ($q) use ($last_version) {
                        $q->where(['breakable' => true, 'history_version' => $last_version]);
                    })->where([['hour_' . $tms[0], '>', $max_allowed], ['hour_' . $tms[0], '>', 0]])->where('hour_' . $tms[0], '>', 0)->orderBy('hour_' . $tms[0], 'desc')->get();
                    $f = $feeder_with_good_bar->first();
                    if (!$feeder_with_good_bar->isEmpty()) {
                        FeederOffSchedule::create([
                            'off_table_id' => $off_table->id,
                            'user_id' => $request->user->user_id,
                            'feeder_id' => $f->Feeder->id,
                            'date' => $dt,
                            'off_time_start' => $start_time,
                            'off_time_end' => $end_time,
                            'off_period' => $off_table->period,
                        ]);
                        continue;
                    }

                }
                if ($feeder_with_good_bar->isEmpty()) {
                    $feeder_with_good_bar = FeederBar::with('Feeder')->whereHas('Feeder', function ($q) use ($last_version) {
                        $q->where(['breakable' => true, 'history_version' => $last_version]);
                    })->where([['hour_' . $tms[0], '<=', $max_allowed], ['hour_' . $tms[0], '>', 0]])->orderBy('hour_' . $tms[0], 'desc')->get();
                }
                if ($feeder_with_good_bar->isEmpty()) {
                    $feeder_with_good_bar = FeederBar::with('Feeder')->whereHas('Feeder', function ($q) use ($last_version) {
                        $q->where(['breakable' => true, 'history_version' => $last_version]);
                    })->where([['hour_' . $tms[0], '>', $max_allowed], ['hour_' . $tms[0], '>', 0]])->where('hour_' . $tms[0], '>', 0)->orderBy('hour_' . $tms[0], 'desc')->get();
                    $f = $feeder_with_good_bar->first();
                    if (!$feeder_with_good_bar->isEmpty()) {
                        FeederOffSchedule::create([
                            'off_table_id' => $off_table->id,
                            'user_id' => $request->user->user_id,
                            'feeder_id' => $f->Feeder->id,
                            'date' => $dt,
                            'off_time_start' => $start_time,
                            'off_time_end' => $end_time,
                            'off_period' => $off_table->period,
                        ]);
                        continue;
                    }
                }
                foreach ($feeder_with_good_bar as $f) {
                    if ($allowed_off == 0)
                        break;
                    // check the time limit dont need more
                    // if ($f->Feeder->break_type == 'time_limit' or $f->Feeder->break_type == 'both') {
                    //     $f->Feeder->limit;
                    //     $limit_times = str_replace(' ', '', explode('-', $f->Feeder->limit));
                    //     $start_time = Carbon::createFromTimeString($start_time);
                    //     $limit_times_start = Carbon::createFromTimeString($limit_times[0]);
                    //     $end_time = Carbon::createFromTimeString($end_time);
                    //     $limit_times_end = Carbon::createFromTimeString($limit_times[1]);
                    //     // this check times have intersection or not
                    //     if (($start_time > $limit_times_start and $start_time < $limit_times_end)
                    //         or ($end_time > $limit_times_start and $end_time < $limit_times_end)
                    //         or ($limit_times_start > $start_time and $limit_times_start < $end_time)
                    //         or ($limit_times_end > $start_time and $limit_times_end < $end_time))
                    //         continue;
                    // }
                    if ($max_allowed == $allowed_off and $loop)
                        break;
                    if ($max_allowed - $f->{'hour_' . $tms[0]} >= 0)
                        $max_allowed -= $f->{'hour_' . $tms[0]};
                    else
                        continue;
                    FeederOffSchedule::create([
                        'off_table_id' => $off_table->id,
                        'user_id' => $request->user->user_id,
                        'feeder_id' => $f->Feeder->id,
                        'date' => $dt,
                        'off_time_start' => $start_time,
                        'off_time_end' => $end_time,
                        'off_period' => $off_table->period,
                    ]);
                    $loop++;
                }
            }
        }
        return view('panel_items/off_list/result_off_list',
            [
                'page_name' => 'تایید جدول خاموشی',
                'times' => $times,
                'dates' => $request->date,
                'dates_unformated' => $request->date,
                'off_table' => $off_table,
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $off_table = OffTable::where('id', $id)->first();
        if (!$off_table) {
            flash('جدول مورد  نظر یافت نشد', 0);
            return redirect('panel/createOffList');
        }
        $times = time_range($off_table->start_time, $off_table->end_time, $off_table->period);
        $dates = dates_range_array($off_table->start_date, $off_table->end_date);
        $mc = ManageConsumption::whereIn('date', $dates)->get();
        return view('panel_items/off_list/store_form_off_list',
            [
                'page_name' => 'ایجاد جدول خاموشی',
                'times' => $times,
                'dates' => $dates,
                'off_table' => $off_table,
                'off_items' => $off_table->OffItems->sortBy('start_time'),
                'mc' => $mc,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $off_table = OffTable::where('id', $id)->first();
        if (!$off_table) {
            flash('جدول مورد  نظر یافت نشد', 0);
            return redirect('panel/createOffList');
        }
        $off_table->delete();
        return redirect()->back();
    }

    public function back($table_id)
    {

        if ($table_id == '-1') {
            flash('جدول خاموشی حذف شده است و بازگشت مقدور نمیباشد', 0);
            return redirect('panel/createOffList');
        }
        $off_table = OffTable::where('id', $table_id)->first();
        if (!$off_table) {
            flash('جدول مورد  نظر یافت نشد', 0);
            return redirect('panel/createOffList');
        }
        FeederOffSchedule::where('off_table_id', $table_id)->delete();
        return $this->show($table_id);
    }


    public function view_off_list($id)
    {
        $off_table = OffTable::where('id', $id)->first();
        if (!$off_table) {
            flash('جدول مورد  نظر یافت نشد', 0);
            return redirect('panel/createOffList');
        }
        $times = time_range($off_table->start_time, $off_table->end_time, $off_table->period);
        $dates_formated = dates_range_array($off_table->start_date, $off_table->end_date);
        $dates_unformated = dates_range_array($off_table->start_date, $off_table->end_date, false);

        return view('panel_items/off_list/show_off_list',
            [
                'page_name' => 'تایید جدول خاموشی',
                'times' => $times,
                'dates' => $dates_formated,
                'dates_unformated' => $dates_unformated,
                'off_table' => $off_table,
            ]);
    }

    public function call_avanak(Request $request)
    {
        $request->validate([
            'off_table_id' => 'required|exists:off_tables,id',
        ]);
        $off_table = OffTable::with('FeedersOff')->find($request->off_table_id);
        $items = $off_table->FeedersOff;
        $err = 0;
        foreach ($items as $f) {
            if (!call_avanak($f->Feeder->guid, env('MESSAGE_ID'), $off_table->id)) {
                flash('در فراخوانی مشکلی بوجود آمده است');
                $err = 1;
                break;
            }
        }
        if (!$err)
            flash('فراخوانی با موفقیت انجام شد.', 1);
        return redirect()->back();
    }
}
