<?php

namespace App\Http\Controllers;

use App\Feeder;
use App\FeederBar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\FeederLimitedPartners;
use Hekmatinasser\Verta\Verta;
use App\Contributers;

class feederStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versions = Feeder::orderBy('history_version')->distinct('history_version')->pluck('history_version','created_at');
        $versions = $versions->unique();

        if ($versions->last())
            return redirect('/panel/feedersStatus/' . $versions->last());
        $feeders_url = env('FEEDERS_URL');
        $json = my_curl($feeders_url);
        // $json = tmp_json();
        $feeders = json_decode($json, true);
        return view(
            'panel_items/feeders_status',
            [
                'page_name' => 'وضعیت فیدرها',
                'feeders' => $feeders,
                'versions' => $versions,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $limit = $request['limit'];
        foreach ($limit as $k => $v) {
            $limit[$k] = fa2en($v);
        }
        $request['limit'] = $limit;
        $request->validate([
//            'date' => 'required',
            'feeder_id' => 'required|array',
            'feeder_id.*' => 'integer',
            'global_feeder_id' => 'required|array',
            'global_feeder_id.*' => 'integer',
            'zone_name' => 'required|array',
            'zone_name.*' => 'string|max:200',
            'zone_id' => 'required|array',
            'zone_id.*' => 'integer',
            'post' => 'required|array',
            'post.*' => 'string|max:200',
            'name' => 'required|array',
            'name.*' => 'string|max:200',
            'guid' => 'required|array',
            'guid.*' => 'string|max:100',
            'address' => 'required|array',
            'address.*' => 'nullable|string|max:300',
            'both' => 'required|array',
            'both.*' => 'max:200',
            'breakable' => 'required|array',
            'breakable.*' => 'boolean',
            'break_type' => 'required|array',
            'break_type.*' => 'in:none,both,no_limit,time_limit,maneuver',
            'limit' => 'required|array',
            'limit.*' => 'nullable|string|max:200',
            'bar_type' => 'required|array',
            'bar_type.*' => 'in:system,non_system',
            'description' => 'required|array',
            'description.*' => 'nullable|string|max:200',
        ]);
        $last_version = Feeder::orderBy('created_at', 'desc')->first()->history_version ?? 0;
        $c = count($request['feeder_id']);
        foreach ($request->except(['user', 'date', '_token']) as $r) {
            if (count($r) < $c) {
                return redirect()->back();
                break;
            }
        }
        foreach ($request['feeder_id'] as $id => $fid) {
            $feeder = Feeder::create([
                'name' => $request['name'][$id],
                'global_id' => $request['global_feeder_id'][$id],
                'date' => Carbon::today()->format('Y-m-d'),
                'zone_name' => $request['zone_name'][$id],
                'zone_id' => $request['zone_id'][$id],
                'post' => $request['post'][$id],
                'address' => $request['address'][$id],
                'guid' => $request['guid'][$id],
//                'path' => $request['path'][$id],
                'path' => $request['address'][$id],
                'breakable' => $request['breakable'][$id],
                'break_type' => $request['break_type'][$id],
                'both' => $request['both'][$id],
                'limit' => $request['limit'][$id],
                'description' => $request['description'][$id],
                'bar_type' => $request['bar_type'][$id],
                'history_version' => $last_version + 1,
            ]);
            $bar = FeederBar::where('feeder_id', $fid)->first();
            if ($bar) {
                $new_bar = $bar->replicate();
                $new_bar->user_id = $request->user->user_id;
                $new_bar->feeder_id = $feeder->id;
                $new_bar->save();
            }
            $limit = FeederLimitedPartners::where('feeder_id', $fid)->first();
            if ($limit) {
                $new_limit = $limit->replicate();
                $new_limit->user_id = $request->user->user_id;
                $new_limit->feeder_id = $feeder->id;
                $new_limit->save();
            }
            $cont = Contributers::where('feeder_id', $fid)->first();
            if ($cont) {
                $new_cont = $cont->replicate();
                $new_cont->user_id = $request->user->user_id;
                $new_cont->feeder_id = $feeder->id;
                $new_cont->save();
            }
        }
        flash('ویرایش جدید با موفقیت ثبت شد', 1);
        return redirect('/panel/feedersStatus/' . ($last_version + 1));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feeders = Feeder::where('history_version', $id)->orderBy('breakable', 'desc')->get();
        if ($feeders->isEmpty()) {
            flash('ویرایش مورد نظر یافت نشد', 0);
            $versions = Feeder::orderBy('history_version')->distinct('history_version')->pluck('history_version');
            if ($versions->last())
                return redirect('/panel/feedersStatus/' . $versions->last());
            return redirect('/panel/feedersStatus');
        }
        $versions = Feeder::orderBy('history_version')->distinct('history_version')->pluck('history_version','created_at');
        $versions = $versions->unique();
        return view(
            'panel_items/feeders_status',
            [
                'page_name' => 'وضعیت فیدرها',
                'feeders' => $feeders,
                'versions' => $versions,
                'date' => $feeders->first()->date,
                'total_feeders' => $feeders->count(),
                'breakable_feeders' => $feeders->where('breakable', true)->count(),
                'unbreakable_feeders' => $feeders->where('breakable', false)->count(),
                'current_version' => $id,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function setFeederLimitedPartners(Request $request){
        // todo test this function later
        $request->validate([
            'feeder_id' => 'required|exists:feeders,id',
            'names' => 'required|array',
            'names.*' => 'max:100',
        ]);
        FeederLimitedPartners::where('feeder_id',$request->feeder_id)->delete();
        foreach($request->names as $n){
            if($n == '')
                continue;
            $lp = new FeederLimitedPartners();
            $lp->limited_partner = $n;
            $lp->feeder_id = $request->feeder_id;
            $lp->user_id = $request->user->user_id;
            $lp->save();
        }
        flash('مشترکین با موفقیت برای فیدر ثبت شدند.',1);
        return redirect()->back();
    }
    public function setFeederBar(Request $request)
    {
        //        dd($request->feeder_id);
        $request->validate([
            'feeder_id' => 'required|exists:feeders,id',
            'hour_1' => 'required|integer',
            'hour_2' => 'required|integer',
            'hour_3' => 'required|integer',
            'hour_4' => 'required|integer',
            'hour_5' => 'required|integer',
            'hour_6' => 'required|integer',
            'hour_7' => 'required|integer',
            'hour_8' => 'required|integer',
            'hour_9' => 'required|integer',
            'hour_10' => 'required|integer',
            'hour_11' => 'required|integer',
            'hour_12' => 'required|integer',
            'hour_13' => 'required|integer',
            'hour_14' => 'required|integer',
            'hour_15' => 'required|integer',
            'hour_16' => 'required|integer',
            'hour_17' => 'required|integer',
            'hour_18' => 'required|integer',
            'hour_19' => 'required|integer',
            'hour_20' => 'required|integer',
            'hour_21' => 'required|integer',
            'hour_22' => 'required|integer',
            'hour_23' => 'required|integer',
            'hour_24' => 'required|integer',
        ]);
        $data = $request->except(['user', '_token']);
        $data['user_id'] = $request->user->user_id;
        FeederBar::updateOrCreate(['feeder_id' => $request->feeder_id], $data);
        flash('بار فیدر با موفقیت تغییر کرد', 1);
        return redirect()->back();
    }
}
