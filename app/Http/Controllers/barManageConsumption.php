<?php

namespace App\Http\Controllers;

use App\Feeder;
use App\ManageConsumption;
use App\Plan;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;

class barManageConsumption extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel_items/bar_manage_consumption', ['page_name' => 'اطلاعات مدیریت مصرف بار‬']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request['start_date'] = fa2en($request->start_date);
        $request['end_date'] = fa2en($request->end_date);
        if ($request->exists('date'))
            $request['date'] = fa2en($request->date);
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
            "start_time" => "required",
            "end_time" => "required",
        ]);
//        $start_date = Verta::parse($request->start_date);
//        $end_date = Verta::parse($request->end_date);
//        $dates = dates_range_array($request->start_date, $request->end_date);
        $request = change_req_date_to_fa($request);
//        $day_name = day_name($day->dayOfWeek);
        $day_info = ManageConsumption::where('date', $request->date)->get();
        $plans = Plan::all();

        $times = time_range_bar_manage($request->start_time,$request->end_time);
        return view('panel_items/create_form_bar_manage_consumption',
            [
                'page_name' => 'ثبت اطلاعات مدیریت مصرف',
                'path' => 'اطلاعات مدیریت مصرف / ثبت اطلاعات',
//                'dates' => $dates,
                'times' => $times,
                'start_time' => $request->start_time,
                'end_time' => $request->end_time,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
//                'date' => isset($date) ? verta_date_formatter($date) : verta_date_formatter($start_date),
//                'day_name' => $day_name,
                'day_info' => $day_info,
                'plans' => $plans,
            ]);
    }

    public function store(Request $request)
    {
        $request['start_date'] = fa2en($request->start_date);
        $request['end_date'] = fa2en($request->end_date);
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
            "start_time" => "required",
            "end_time" => "required",
        ]);
//        dd($request->all());
//        $request = change_req_date_to_fa($request);
        $dates = dates_range_array($request->start_date, $request->end_date);
        foreach ($dates as $dt) {
            $plans = Plan::all();
            foreach ($plans as $plan) {
                $data = ManageConsumption::firstOrNew(
                    ['date' => $dt, 'plan_id' => $plan->id],
                    ['user_id' => $request->user->user_id]
                );
                for ($i = 1; $i < 25; $i++) {
                    $data->{'hour_' . (string)$i} = $request->input((string)$plan->id . '-' . (string)$i)*32 ?? 0;
                }
                $data->save();
            }
        }
        flash('اطلاعات با موفقیت برای بازه مورد نظر ثبت شد.', 1);
        return $this->show($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function old_store(Request $request) //before periodic
    {
        $request['date'] = fa2en($request->date);
        $request->validate([
            'date' => 'required',
        ]);
        $plans = Plan::all();
        foreach ($plans as $plan) {
            $data = ManageConsumption::firstOrNew(
                ['date' => $request->date, 'plan_id' => $plan->id],
                ['user_id' => $request->user->user_id]
            );
            for ($i = 1; $i < 25; $i++) {
                $data->{'hour_' . (string)$i} = $request->input((string)$plan->id . '-' . (string)$i);
            }
            $data->save();
        }
        $date = Verta::parse($request->date);
        flash('اطلاعات با موفقیت برای تاریخ ' . verta_date_formatter($date) . ' ثبت شد.', 1);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $request['start_date'] = fa2en($request->start_date);
        $request['end_date'] = fa2en($request->end_date);
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
//            "start_time" => "required",
//            "end_time" => "required",
        ]);
        $s_date = Verta::parse($request->start_date);
        $e_date = Verta::parse($request->end_date);
        if ($e_date->lt($s_date)) {
            flash('تاریخ پایان باید بیشتر از تاریخ شروع و یا برابر با آن باشد', 0);
            return redirect()->back();
        }
        $dates = dates_range_array($request->start_date, $request->end_date, false);
        $items = ManageConsumption::whereIn('date', $dates)->orderBy('date')->get();
        $exist_dates = $items->pluck('date')->unique();
//        $times = time_range_bar_manage($request->start_time,$request->end_time);
        return view('panel_items/bar_manage_consumption',
            [
                'page_name' => 'اطلاعات مدیریت مصرف بار‬',
                'end_date' => $request->end_date,
                'start_date' => $request->start_date,
//                'start_time' => $request->start_time,
//                'end_time' => $request->end_time,
//                'times' => $times,
                'items' => $items,
                'dates' => $dates,
                'exist_dates' => $exist_dates,
            ]);

    }

    public function subInfo(Request $request)
    {
        $request['date'] = fa2en($request->date);
        $request->validate([
            'date' => 'required',
            'header_format' => 'required|string',
        ]);
        $header = explode('-', $request->header_format);
        $mc_items = ManageConsumption::with('Plan')->where('date', $request->date)->orderBy('plan_id')->get();
        $resp = [];
        $plans = $mc_items->pluck('plan_id');

        foreach ($plans as $plan) {
            $resp_item = [];
            foreach ($header as $h) {
                switch ($h) {
                    case '24':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_24 ? $mc_items->where('plan_id', $plan)->first()->hour_24/32 : 0);
                        break;
                    case '23':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_23 ? $mc_items->where('plan_id', $plan)->first()->hour_23/32 : 0);
                        break;
                    case '22':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_22 ? $mc_items->where('plan_id', $plan)->first()->hour_22/32 : 0);
                        break;
                    case '21':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_21 ? $mc_items->where('plan_id', $plan)->first()->hour_21/32 : 0);
                        break;
                    case '20':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_20 ? $mc_items->where('plan_id', $plan)->first()->hour_20/32 : 0);
                        break;
                    case '19':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_19 ? $mc_items->where('plan_id', $plan)->first()->hour_19/32 : 0);
                        break;
                    case '18':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_18 ? $mc_items->where('plan_id', $plan)->first()->hour_18/32 : 0);
                        break;
                    case '17':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_17 ? $mc_items->where('plan_id', $plan)->first()->hour_17/32 : 0);
                        break;
                    case '16':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_16 ? $mc_items->where('plan_id', $plan)->first()->hour_16/32 : 0);
                        break;
                    case '15':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_15 ? $mc_items->where('plan_id', $plan)->first()->hour_15/32 : 0);
                        break;
                    case '14':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_14 ? $mc_items->where('plan_id', $plan)->first()->hour_14/32 : 0);
                        break;
                    case '13':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_13 ? $mc_items->where('plan_id', $plan)->first()->hour_13/32 : 0);
                        break;
                    case '12':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_12 ? $mc_items->where('plan_id', $plan)->first()->hour_12/32 : 0);
                        break;
                    case '11':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_11 ? $mc_items->where('plan_id', $plan)->first()->hour_11/32 : 0);
                        break;
                    case '10':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_10 ? $mc_items->where('plan_id', $plan)->first()->hour_10/32 : 0);
                        break;
                    case '9':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_9 ? $mc_items->where('plan_id', $plan)->first()->hour_9/32 : 0);
                        break;
                    case '8':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_8 ? $mc_items->where('plan_id', $plan)->first()->hour_8/32 : 0);
                        break;
                    case '7':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_7 ? $mc_items->where('plan_id', $plan)->first()->hour_7/32 : 0);
                        break;
                    case '6':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_6 ? $mc_items->where('plan_id', $plan)->first()->hour_6/32 : 0);
                        break;
                    case '5':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_5 ? $mc_items->where('plan_id', $plan)->first()->hour_5/32 : 0);
                        break;
                    case '4':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_4 ? $mc_items->where('plan_id', $plan)->first()->hour_4/32 : 0);
                        break;
                    case '3':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_3 ? $mc_items->where('plan_id', $plan)->first()->hour_3/32 : 0);
                        break;
                    case '2':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_2 ? $mc_items->where('plan_id', $plan)->first()->hour_2/32 : 0);
                        break;
                    case '1':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->hour_1 ? $mc_items->where('plan_id', $plan)->first()->hour_1/32 : 0);
                        break;
                    case 'طرح':
                        array_push($resp_item, $mc_items->where('plan_id', $plan)->first()->Plan->name);
                        break;
                    default:
                        array_push($resp_item, '');
                }
            }
            array_push($resp, $resp_item);
        }
        return response()->json($resp, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
