<?php

namespace App\Http\Controllers;

use App\FeederOffSchedule;
use App\Http\Resources\offFeederResource;
use App\Http\Resources\partnersFeederResource;
use App\Http\Resources\polyFeederResource;
use App\Http\Resources\publicResource;
use Carbon\Carbon;
use Illuminate\Http\Request;

class apiController extends Controller
{
    public function address(Request $request)
    {
        $request->validate([
            'start_date'=> 'required',
            'end_date'=> 'required',
        ]);
        $dates = dates_range_array($request->start_date,$request->end_date,false);
        $feeders = FeederOffSchedule::with('Feeder')->whereIn('date',$dates)->get();
        $response = offFeederResource::collection($feeders);
        return response()->json($response,200);
    }

    public function polygon(Request $request)
    {
        $request->validate([
            'start_date'=> 'required',
            'end_date'=> 'required',
        ]);
        $dates = dates_range_array($request->start_date,$request->end_date,false);
        $feeders = FeederOffSchedule::with('Feeder')->whereIn('date',$dates)->get();
        $response = polyFeederResource::collection($feeders);
        return response()->json($response,200);
    }

    public function partners(Request $request)
    {
        $request->validate([
            'start_date'=> 'required',
            'end_date'=> 'required',
        ]);
        $dates = dates_range_array($request->start_date,$request->end_date,false);
        $feeders = FeederOffSchedule::with('Feeder')->whereIn('date',$dates)->get();
        $response = partnersFeederResource::collection($feeders);
        return response()->json($response,200);
    }

    public function pass(Request $request)
    {
        $request->validate([
            'custid'=> 'required|numeric',
        ]);
        if ($request->exists('start_date') and $request->exists('end_date')){
            $dates = dates_range_array($request->start_date,$request->end_date,false);
        }else{
            $today = Carbon::today()->toDateString();
            $next_week = Carbon::today()->addWeek()->toDateString();
            $dates = dates_range_array($today,$next_week,false);
        }
        $guid = get_feeder($request->custid);
        $feeders = FeederOffSchedule::whereIn('date',$dates)->whereHas('Feeder',function ($q) use ($guid){
            $q->where('guid',$guid);
        })->get();

        $response = publicResource::collection($feeders);
        return response()->json($response,200);
    }



}
