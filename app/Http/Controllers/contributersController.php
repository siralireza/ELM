<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feeder;
use App\FeederBar;
use App\FeederLimitedPartners;
use Hekmatinasser\Verta\Verta;
use App\Contributers;

class contributersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versions = Feeder::orderBy('history_version')->distinct('history_version')->pluck('history_version');
        if ($versions->last())
            return redirect('/panel/contributers/' . $versions->last());

        flash('لطفا جهت وارد کردن مشترکین همکار ابتدا یک ویرایش از لیست فیدر ها درست نمایید');
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feeders = Feeder::where('history_version', $id)->orderBy('breakable', 'desc')->get();
        if ($feeders->isEmpty()) {
            flash('ویرایش مورد نظر یافت نشد', 0);
            $versions = Feeder::orderBy('history_version')->distinct('history_version')->pluck('history_version');
            if ($versions->last())
                return redirect('/panel/contributers/' . $versions->last());
            return redirect('/panel/contributers');
        }
        $versions = Feeder::orderBy('history_version')->distinct('history_version')->pluck('history_version');
        return view(
            'panel_items/contributers',
            [
                'page_name' => 'اطلاعات مشترکین همکار',
                'feeders' => $feeders,
                'versions' => $versions,
                'date' => $feeders->first()->date,
                'total_feeders' => $feeders->count(),
                'breakable_feeders' => $feeders->where('breakable', true)->count(),
                'unbreakable_feeders' => $feeders->where('breakable', false)->count(),
                'current_version' => $id,
            ]
        );
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function setFeederContributers(Request $request){
        $request->validate([
            'feeder_id' => 'required|exists:feeders,id',
            'names' => 'required|array',
            'names.*' => 'max:100',
        ]);
        Contributers::where('feeder_id',$request->feeder_id)->delete();
        foreach($request->names as $n){
            if($n == '')
                continue;
            $lp = new Contributers();
            $lp->contributer = $n;
            $lp->feeder_id = $request->feeder_id;
            $lp->user_id = $request->user->user_id;
            $lp->save();
        }
        flash('مشترکین با موفقیت برای فیدر ثبت شدند.',1);
        return redirect()->back();
    }
}
