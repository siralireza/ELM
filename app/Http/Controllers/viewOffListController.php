<?php

namespace App\Http\Controllers;

use App\DailyInfo;
use App\DayPeak;
use App\FeederOffSchedule;
use App\OffTable;
use App\Setting;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class viewOffListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = Carbon::today('Asia/Tehran')->format('Y-m-d');
//        $max_off_id = FeederOffSchedule::where('date', $today)->max('off_table_id');
        $today_off_feeders = FeederOffSchedule::where('date', $today)
            //todo uncomment this later
//            ->where('off_time_start', '>=', Carbon::now('Asia/Tehran')->toTimeString())
            ->orderBy('off_time_start')->get();

        check_active_table();
        $active_table = OffTable::where('status', 'applied')->first();
        $off_table = $active_table;
        $today_off_feeders  = $today_off_feeders->where('off_table_id',$active_table->id ?? -1);
        if (!$off_table) {
            flash('درحال حاظر جدول خاموشی فعالی وجود ندارد');
            $times = [];
            return view('panel_items/view_off_list',
                [
                    'page_name' => 'مشاهده جدول خاموشی',
                    'today_feeders' => $today_off_feeders,
                    'times' => $times,
                    'today' => $today,
                    'off_table' => $off_table,
                ]);
        }
        $days = Setting::where('name', 'days')->latest()->first();
        if (!$days) {
            Session::flash('alert-class', 'alert-warning');
            Session::flash('message', 'تعداد روز ها برای خاموشی فیدر تعیید نشده است و صفر در نظر گرفته شد');
            $days = 0;
        } else
            $days = $days->value;

        $carbon_tm = Carbon::now('Asia/Tehran')->toTimeString();
        $now = Verta($carbon_tm);
        $days_ago = $now->subDays($days);

        $off_feeders = 0;
        $still_off_bar = 0;
        $today_offed_bar = 0;
        foreach ($today_off_feeders as $tof) {
            if ($tof->last_off_time) {
                $dt = Carbon::parse($tof->last_off_time);
                if ($dt->gte($days_ago))
                    $tof->allowed = false;
            }
            if ($tof->status == 'off') {
                $off_feeders++;
                $still_off_bar += $tof->off_real_bar;
            } elseif ($tof->off_real_time) {
                $today_offed_bar += $tof->off_real_bar;
            }
        }

        $times = time_range($off_table->start_time, $off_table->end_time, $off_table->period);
        Session::flash('alert-class', 'alert-info');
        Session::flash('message', 'تمامی فیلد ها به مگاوات میباشند');
        $tmp = '';
        $reses = [];
        foreach ($today_off_feeders as $f){
            $g = $f->Feeder->guid;
            if ($tmp == $g){
                continue;
            }else{
                $res = get_geometry($g,true);
                if ($res != "null")
                array_push($reses,$res);
                $tmp = $g;
            }
        }
        $reses = array_unique($reses);
        return view('panel_items/view_off_list',
            [
                'page_name' => 'مشاهده جدول خاموشی',
                'today_feeders' => $today_off_feeders,
                'times' => $times,
                'today' => $today,
                'off_table' => $off_table,
                'off_feeders' => $off_feeders,
                'still_off_bar' => $still_off_bar,
                'today_offed_bar' => $today_offed_bar,
                'reses' => $reses,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function barPeak(Request $request)
    {
        $request->validate([
            'bar_peak' => 'required|array',
            'bar_peak.*' => 'integer',
            'date' => 'required',
            'off_table_id' => 'required|exists:off_tables,id',
        ]);

        // $off_table = OffTable::where('id', $request->off_table_id)->first();
        // $times = time_range($off_table->start_time, $off_table->end_time, $off_table->period);
        $peak_times = [
            "8:00 - 09:00",
            "9:00 - 10:00",
            "10:00 - 11:00",
            "11:00 - 12:00",
            "12:00 - 13:00",
            "13:00 - 14:00",
            "14:00 - 15:00",
            "15:00 - 16:00",
            "16:00 - 17:00",
            "17:00 - 18:00",
        ];
        $times = $peak_times;
        // if (count($request->bar_peak) != count($times)) {
        //     flash('ساعت های ارسال شده با جدول خاموشی  یکسان نیست');
        //     return redirect()->back();
        // }
        foreach ($times as $k => $v) {
            $this_time_value = $request->bar_peak[$k];
            DailyInfo::updateOrCreate([
                'date' => $request->date,
                'time' => str_replace(' ', '', explode('-', $v)[0]),
            ], [
                'user_id' => $request->user->user_id,
                'date' => $request->date,
                'time' => str_replace(' ', '', explode('-', $v)[0]),
                'bar_peak_mw' => $this_time_value*32,
            ]);
        }
        flash('پیک بار با موفقیت ثبت شد', 1);
        return redirect()->back();
    }

    public function realConsumption(Request $request)
    {
        $request->validate([
            'real_cons' => 'required|array',
            'real_cons.*' => 'integer',
            'date' => 'required',
            'off_table_id' => 'required|exists:off_tables,id',

        ]);
        $off_table = OffTable::where('id', $request->off_table_id)->first();
        $times = time_range($off_table->start_time, $off_table->end_time, $off_table->period);
        if (count($request->real_cons) != count($times)) {
            flash('ساعت های ارسال شده با جدول خاموشی  یکسان نیست');
            return redirect()->back();
        }
        foreach ($times as $k => $v) {
            $this_time_value = $request->real_cons[$k];
            DailyInfo::updateOrCreate([
                'date' => $request->date,
                'time' => str_replace(' ', '', explode('-', $v)[0]),
            ], [
                'user_id' => $request->user->user_id,
                'date' => $request->date,
                'time' => str_replace(' ', '', explode('-', $v)[0]),
                'real_cons' => $this_time_value*32,
            ]);
        }
        flash('درخواست توانیر با موفقیت ثبت شد', 1);
        return redirect()->back();
    }

    public function tavRealReq(Request $request)
    {
        $request->validate([
            'tavanir_real_req' => 'required|array',
            'tavanir_real_req.*' => 'integer',
            'date' => 'required',
            'off_table_id' => 'required|exists:off_tables,id',

        ]);
        $off_table = OffTable::where('id', $request->off_table_id)->first();
        $times = time_range($off_table->start_time, $off_table->end_time, $off_table->period);
        if (count($request->tavanir_real_req) != count($times)) {
            flash('ساعت های ارسال شده با جدول خاموشی  یکسان نیست');
            return redirect()->back();
        }
        foreach ($times as $k => $v) {
            $this_time_value = $request->tavanir_real_req[$k];
            DailyInfo::updateOrCreate([
                'date' => $request->date,
                'time' => str_replace(' ', '', explode('-', $v)[0]),
            ], [
                'user_id' => $request->user->user_id,
                'date' => $request->date,
                'time' => str_replace(' ', '', explode('-', $v)[0]),
                'tavanir_request' => $this_time_value*32,
            ]);
        }
        flash('درخواست توانیر با موفقیت ثبت شد', 1);
        return redirect()->back();
    }

    public function dayPeak(Request $request)
    {
        $request->validate([
            'peak' => 'required',
            'date' => 'date',
        ]);
        $request['user_id'] = $request->user->user_id;
        $request['peak'] = $request->peak*32;
        DayPeak::updateOrCreate([
            'date' => $request->date
        ], $request->except('user'));
        flash('پیک روز با موفقیت تعیین شد', 1);
        return redirect()->back();
    }

    public function feeder_on(Request $request, $off_sch_id)
    {
        engReq($request);
        $request->validate([
            // 'on_real_bar' => 'required|integer',
            'on_real_time' => 'required',
            'description' => 'max:500',
        ]);
        $off_sch = FeederOffSchedule::find($off_sch_id);
        if (!$off_sch) {
            flash('فیدر در جدول خاموشی یافت نشد');
            return redirect()->back();
        }
        if ($off_sch->status == 'on') {
            flash('فیدر روشن میباشد');
            return redirect()->back();
        }
        $off_sch->user_id = $request->user->user_id;
        $off_sch->on_real_time = $request->on_real_time;
        // $off_sch->on_real_bar = $request->on_real_bar;
        $off_sch->on_real_bar = $off_sch->off_real_bar;
        $off_sch->status = 'on';
        $off_sch->save();
        $description = $request->description;
//        $carbon_dt = Carbon::now('Asia/Tehran')->toDateTimeString();
        $carbon_dt = Carbon::createFromTimeString($request->on_real_time)->toDateTimeString();
        if (!call_121('on', $off_sch->Feeder->global_id,
            $carbon_dt, $off_sch->on_real_bar, $description)) {
            Session::flash('alert-class', 'alert-warning');
            Session::flash('message', 'ارتباط با ۱۲۱ با مشکل مواجه شد');
            return redirect()->back();
        }
        flash('فیدر با موفقیت روشن شد', 1);
        return redirect()->back();
    }

    public function feeder_off(Request $request, $off_sch_id)
    {
        engReq($request);
        $request->validate([
            'off_real_bar' => 'required|integer',
            'off_real_time' => 'required',
            'description' => 'max:500',
        ]);
        $off_sch = FeederOffSchedule::with('Feeder')->find($off_sch_id);
        if (!$off_sch) {
            flash('فیدر در جدول خاموشی یافت نشد');
            return redirect()->back();
        }
        if ($off_sch->status == 'off') {
            flash('فیدر خاموش میباشد');
            return redirect()->back();
        }
        $off_sch->user_id = $request->user->user_id;
        $off_sch->off_real_bar = $request->off_real_bar*32; // to change to mw
        $off_sch->off_real_time = $request->off_real_time;
        $off_sch->description = $request->description ? $request->description : null;
        $off_sch->status = 'off';
        $carbon_tm = Carbon::now('Asia/Tehran')->toTimeString();
        $carbon_dt = Carbon::now('Asia/Tehran')->toDateTimeString();
//        $verta = new Verta($carbon_tm);
//        $now = $verta->formatJalaliDatetime();
        $off_sch->last_off_time = $carbon_dt;
        $off_sch->save();

        if (!call_121('off', $off_sch->Feeder->global_id,
            $carbon_dt, $request->off_real_bar)) {
            Session::flash('alert-class', 'alert-warning');
            Session::flash('message', 'ارتباط با ۱۲۱ با مشکل مواجه شد');
            return redirect()->back();
        }
        flash('فیدر با موفقیت خاموش شد', 1);
        return redirect()->back();
    }
}
