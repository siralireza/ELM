<?php

namespace App\Http\Controllers;

use App\Feeder;
use App\Setting;
use Hamcrest\Core\Set;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class detParamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $days = Setting::where('name', 'days')->latest()->first();
        $tolerance = Setting::where('name', 'tolerance')->latest()->first();
        return view(
            'panel_items/determine_parameters',
            [
                'page_name' => 'تعیین پارامتر‌های موثر',
                'days' => $days ? $days->value : 'خالی',
                'tolerance' => $tolerance ? $tolerance->value : 'خالی',
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'days' => 'required|numeric|min:1|max:365',
            'tolerance' => 'required|numeric|between:0,100',
        ]);
        $user_id = $request->user->user_id;

        Setting::create([
            'user_id' => $user_id,
            'name' => 'days',
            'value' => $request->days
        ]);
        Setting::create([
            'user_id' => $user_id,
            'name' => 'tolerance',
            'value' => $request->tolerance,
        ]);
        flash('پارامتر ها با موفقیت به روز رسانی شدند', 1);
        return $this->index();
    }


    public function uptodate()
    {
        $versions = Feeder::orderBy('history_version')->distinct('history_version')->pluck('history_version');
        if (!$versions->last())
            return redirect()->back();
        $last_version = $versions->last();
        $feeders_url = env('FEEDERS_URL');
        $json = my_curl($feeders_url);
        $feeders = json_decode($json, true);
        $global_ids = [];
        foreach ($feeders['data'] as $k => $v) {
            array_push($global_ids, $v['feeder_id']);
            if (!Feeder::where([['global_id', $v['feeder_id']], ['history_version', $last_version]])->exists()) {
                Feeder::create([
                    'name' => $v['name'],
                    'global_id' => $v['feeder_id'],
                    'date' => Verta::now()->formatJalaliDatetime(),
                    'zone_name' => $v['zone_name'],
                    'zone_id' => $v['zone_id'],
                    'post' => $v['post'],
                    'address' => $v['address'],
                    'path' => $v['path'],
                    'breakable' => false,
                    'break_type' => 'none',
                    'bar_type' => 'system',
                    'history_version' => $last_version,
                ]);
            }
        }
        $not_in = Feeder::whereNotIn('global_id', $global_ids)->get();
        foreach ($not_in as $f) {
            $f->is_deleted = true;
            $f->save();
        }
        return redirect()->back();
    }
}
