<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class DateChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request = change_req_date_to_en($request);
        $response = $next($request);
        return $response;

    }
}
