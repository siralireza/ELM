<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class AuthChecker
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * after use function if user unauthorized should redirect to login page
         * if not should fill this user object to use it later
         */

        if (env('APP_DEBUG')) {
            //          for test environment
            $is_login = true;
            try {
                // todo uncomment this line later at production
                // $is_login = \tmp_user($request);
                $user = new User();
                $user->user_id = 1;
                $user->first_name = 'محمد';
                $user->last_name = 'محمدی';
                $user->rank = 'مدیر';
                $user->code = '2812';
                $user->access_keys = tmp_access_keys();
                $request['user'] = $user;

                if (!check_access_list($request))
                    return redirect('/');
                return $next($request);
            } catch (\Exception $exception) { }
        } else {
            try {
                $is_login = \user();
            } catch (\Exception $exception) {
                $is_login = false;
            }
        }
        if ($is_login) {
            $user = new User();
            $user->user_id = $is_login['id'];
            $user->username = $is_login['username'];
            $user->first_name = $is_login['fname'];
            $user->last_name = $is_login['lname'];
            $user->code = $is_login['code'];
            $user->rank = $is_login['post_title'];
            $user->access_keys = $is_login['access_keys'];
            $request['user'] = $user;
            if (!check_access_list($request))
                return redirect('/');
        } else {
            return redirect(env('LOGIN_URL'));
        }
        return $next($request);
    }
}
