<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class partnersFeederResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $custs = get_custid($this->Feeder->guid);
        return [
            'date'=>den2dfa($this->date),
            "off_time_start"=> $this->off_time_start,
            "off_time_end"=> $this->off_time_end,
            "custids"=> $custs ? $custs : [],
        ];
    }
}
