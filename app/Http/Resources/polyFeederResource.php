<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class polyFeederResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $geo = get_geometry($this->Feeder->guid);
        return [
            'date'=>den2dfa($this->date),
            "off_time_start"=> $this->off_time_start,
            "off_time_end"=> $this->off_time_end,
            "coordinates"=> $geo ? $geo : [],
        ];
    }
}
