<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contributers extends Model
{
    protected $table = 'contributers';
    public function Feeder()
    {
        return $this->belongsTo(Feeder::class, 'feeder_id', 'id');
    }
}
