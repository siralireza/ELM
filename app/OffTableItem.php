<?php

namespace App;


class OffTableItem extends MotherModel
{
    protected $table = 'off_items';
    public function OffTable()
    {
        return $this->belongsTo(OffTable::class);
    }

}
