<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>پنل مدیریت</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('dist/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{asset('dist/fonts/googleFonts.css')}}">
    <!-- bootstrap rtl -->
    <link rel="stylesheet" href="{{asset('dist/css/bootstrap-rtl.min.css')}}">
    <!-- template rtl version -->
    <link rel="stylesheet" href="{{asset('dist/css/custom-style.css')}}">
    <!-- datatables css -->
    <link rel="stylesheet" type="text/css" href="{{asset('dist/css/datatables.min.css')}}"/>
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <!-- Persian Data Picker -->
    <link rel="stylesheet" href="{{asset('dist/css/persian-datepicker.min.css')}}">
    @yield('styles'))
    <style>
        .hide-disp {
            visibility: hidden;
        }

        .no-disp {
            display: none;
        }

        .pull-left {
            float: left !important;
        }

        .unclickable {
            pointer-events: none;
        }

        .sk-circle {
            margin: 100px auto;
            width: 80px;
            height: 80px;
            position: relative;
        }

        .sk-circle .sk-child {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }

        .sk-circle .sk-child:before {
            content: '';
            display: block;
            margin: 0 auto;
            width: 15%;
            height: 15%;
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
            animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
        }

        .sk-circle .sk-circle2 {
            -webkit-transform: rotate(30deg);
            -ms-transform: rotate(30deg);
            transform: rotate(30deg);
        }

        .sk-circle .sk-circle3 {
            -webkit-transform: rotate(60deg);
            -ms-transform: rotate(60deg);
            transform: rotate(60deg);
        }

        .sk-circle .sk-circle4 {
            -webkit-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
        }

        .sk-circle .sk-circle5 {
            -webkit-transform: rotate(120deg);
            -ms-transform: rotate(120deg);
            transform: rotate(120deg);
        }

        .sk-circle .sk-circle6 {
            -webkit-transform: rotate(150deg);
            -ms-transform: rotate(150deg);
            transform: rotate(150deg);
        }

        .sk-circle .sk-circle7 {
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
        }

        .sk-circle .sk-circle8 {
            -webkit-transform: rotate(210deg);
            -ms-transform: rotate(210deg);
            transform: rotate(210deg);
        }

        .sk-circle .sk-circle9 {
            -webkit-transform: rotate(240deg);
            -ms-transform: rotate(240deg);
            transform: rotate(240deg);
        }

        .sk-circle .sk-circle10 {
            -webkit-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            transform: rotate(270deg);
        }

        .sk-circle .sk-circle11 {
            -webkit-transform: rotate(300deg);
            -ms-transform: rotate(300deg);
            transform: rotate(300deg);
        }

        .sk-circle .sk-circle12 {
            -webkit-transform: rotate(330deg);
            -ms-transform: rotate(330deg);
            transform: rotate(330deg);
        }

        .sk-circle .sk-circle2:before {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }

        .sk-circle .sk-circle3:before {
            -webkit-animation-delay: -1s;
            animation-delay: -1s;
        }

        .sk-circle .sk-circle4:before {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }

        .sk-circle .sk-circle5:before {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }

        .sk-circle .sk-circle6:before {
            -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s;
        }

        .sk-circle .sk-circle7:before {
            -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s;
        }

        .sk-circle .sk-circle8:before {
            -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s;
        }

        .sk-circle .sk-circle9:before {
            -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s;
        }

        .sk-circle .sk-circle10:before {
            -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s;
        }

        .sk-circle .sk-circle11:before {
            -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s;
        }

        .sk-circle .sk-circle12:before {
            -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s;
        }

        @-webkit-keyframes sk-circleBounceDelay {
            0%, 80%, 100% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }
            40% {
                -webkit-transform: scale(1);
                transform: scale(1);
            }
        }

        @keyframes sk-circleBounceDelay {
            0%, 80%, 100% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }
            40% {
                -webkit-transform: scale(1);
                transform: scale(1);
            }
        }
    </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom"
         style="z-index: 1000 !important;display: block">
        <div class="row">
            <div class="col-sm-6">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                    </li>
                    <li class="nav-item mr-3">
                        <h1 class="m-0 text-dark">{{$page_name ?? 'خانه'}}</h1>
                    </li>

                </ul>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left ml-5">
                    @if(isset($page_name))
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                    @endif
                    @yield('path')
                </ol>
            </div><!-- /.col -->
            <!-- Left navbar links -->

        </div>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{url('/')}}" class="brand-link">
            {{--<img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"--}}
            {{--class="brand-image img-circle elevation-3"--}}
            {{--style="opacity: .8">--}}
            <span class="brand-text font-weight-light mr-5">پنل مدیریت</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar" style="direction: ltr">
            <div style="direction: rtl">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3">
                    <div class="d-flex">
                        <div class="info mr-3">
                            <a class="d-block">{{Request()->user->first_name . ' ' . Request()->user->last_name}}</a>
                        </div>
                        <div class="info mr-3">
                            <a class="d-block">کد&nbsp;{{en2fa(Request()->user->code)}} </a>
                        </div>
                    </div>
                    <div class="info mr-3">
                        <a class="d-block">{{Request()->user->rank}}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->
                        @if(Request()->user->access_keys['base_data'] != 0)
                            <li class="nav-item has-treeview @if(isset($page_name)) @if($page_name == 'تعیین پارامتر‌های موثر'or $page_name == 'اطلاعات مدیریت مصرف بار‬' or $page_name == 'وضعیت فیدرها' or $page_name == 'اطلاعات مشترکین همکار') menu-open active @endif @endif">
                                <a class="nav-link" href="#">
                                    <i class="nav-icon fa fa-database"></i>
                                    <p>
                                        اطلاعات پایه
                                        <i class="right fa fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('/panel/detParams')}}"
                                           class="nav-link @if(isset($page_name) and $page_name == 'تعیین پارامتر‌های موثر') active @endif">
                                            <i class="fa fa-edit nav-icon"></i>
                                            <p>تعیین پارامتر های موثر</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('/panel/feedersStatus')}}"
                                           class="nav-link @if(isset($page_name) and $page_name == 'وضعیت فیدرها') active @endif">
                                            <i class="fa fa-line-chart nav-icon"></i>
                                            <p>وضعیت فیدرها</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('/panel/contributers')}}"
                                           class="nav-link @if(isset($page_name) and $page_name == 'اطلاعات مشترکین همکار') active @endif">
                                            <i class="fa fa-users nav-icon"></i>
                                            <p>اطلاعات مشترکین همکار</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('/panel/barManageConsumption')}}"
                                           class="nav-link @if(isset($page_name) and $page_name == 'اطلاعات مدیریت مصرف بار‬') active @endif">
                                            <i class="fa fa-bolt nav-icon"></i>
                                            <p>اطلاعات مدیریت مصرف بار‬</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        @if(Request()->user->access_keys['create_off_list'] != 0)
                            <li class="nav-item">
                                <a href="{{url('/panel/createOffList')}}"
                                   class="nav-link @if(isset($page_name) and $page_name == 'ایجاد جدول خاموشی') active @endif">
                                    <i class="nav-icon fa fa-calendar-plus-o"></i>
                                    <p>
                                        ایجاد جدول خاموشی
                                    </p>
                                </a>
                            </li>
                        @endif
                        @if(Request()->user->access_keys['observe_off_list'] != 0)
                            <li class="nav-item ">
                                <a href="{{url('/panel/viewOffList')}}"
                                   class="nav-link @if(isset($page_name) and $page_name == 'مشاهده جدول خاموشی') active @endif">
                                    <i class="nav-icon fa fa-table"></i>
                                    <p>
                                        مشاهده جدول خاموشی
                                    </p>
                                </a>
                            </li>
                        @endif
{{--                        @if(Request()->user->access_keys['view_map'] != 0)--}}
{{--                            <li class="nav-item ">--}}
{{--                                <a href="{{url('/panel/viewMap')}}"--}}
{{--                                   class="nav-link @if(isset($page_name) and $page_name == 'نقشه‌ی فیدر ها') active @endif">--}}
{{--                                    <i class="nav-icon fa fa-map-marker"></i>--}}
{{--                                    <p>--}}
{{--                                        نقشه‌ي فیدر ها--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endif--}}
                        <li class="nav-item ">
                            <a href="{{url('/panel/login')}}"
                               class="nav-link">
                                <i class="nav-icon fa fa-lock"></i>
                                <p>
                                    خروج
                                </p>
                            </a>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button>
                        {{ Session::get('message') }}</p>
                @endif
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <p class="alert alert-class alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                &times;
                            </button>
                            {{$error}}</p>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Content Header (Page header) -->
    {{--        <div class="content-header">--}}
    {{--            <div class="container-fluid">--}}
    {{--                <div class="row mb-2">--}}
    {{--                    <div class="col-sm-6">--}}
    {{--                        <h1 class="m-0 text-dark">{{$page_name ?? 'خانه'}}</h1>--}}
    {{--                    </div><!-- /.col -->--}}
    {{--                    <div class="col-sm-6">--}}
    {{--                        <ol class="breadcrumb float-sm-left">--}}
    {{--                            @if(isset($page_name))--}}
    {{--                                <li class="breadcrumb-item"><a href="/">خانه</a></li>--}}
    {{--                            @endif--}}
    {{--                            @yield('path')--}}
    {{--                        </ol>--}}
    {{--                    </div><!-- /.col -->--}}
    {{--                </div><!-- /.row -->--}}
    {{--            </div><!-- /.container-fluid -->--}}
    {{--        </div>--}}
    <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @section('body')
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h5 class="m-0 text-success" style="font-size: 40px">به پنل کاربری خوش آمدید</h5>
                                </div>
                                <div class="card-body">
                                    <p class="card-text" style="font-size: 25px">لطفا صفحه‌ی مورد نظر خود را از منو کنار
                                        انتخاب نمایید.</p>
                                </div>
                            </div>
                        </div>
                    </div>
            @show
            <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
{{--<footer class="main-footer fixed-bottom">--}}
{{--<strong>CopyRight &copy; ۲۰۱۸ <a href="#">شرکت برنامه نویسی</a></strong>--}}
{{--</footer>--}}

<!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('dist/js/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('dist/js/plugins/raphael/raphael-min.js')}}"></script>
<script src="{{asset('plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('dist/js/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
{{--<script src="{{asset('dist/js/demo.js')}}"></script>--}}
<!-- Persian Data Picker -->
<script src="{{asset('dist/js/persian-date.min.js')}}"></script>
<script src="{{asset('dist/js/persian-datepicker.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/datatables.min.js')}}"></script>
Sugar js
<script src="{{asset('dist/js/plugins/sugar.min.js')}}"></script>
<script src="{{asset('dist/js/plugins/weather.min.js')}}"></script>
<script src="{{asset('dist/js/plugins/persian.min.js')}}"></script>
<script src="{{asset('dist/js/plugins/popper.js')}}"></script>
<script src="{{asset('dist/js/plugins/bootstrap-confirmation.min.js')}}"></script>
<script type="text/javascript" src="http://portal.meedc.net/gis/webapi/wgapi.js"></script>
@yield('scripts')
<script>
    zer_replace();

    function zer_replace() {
        $(':input[type="number"]').on('input propertychange paste', function (e) {
            if (this.value == 0)
                return
            var reg = /^0+/gi;
            if (this.value.match(reg)) {
                this.value = this.value.replace(reg, '');
            }
        });
    }

    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 8000);

    $(document).ready(function() {
    $('.modal').on('shown.bs.modal', function() {
    $('input:visible:first', this).trigger('focus');
    });
    });
</script>
</body>
</html>
