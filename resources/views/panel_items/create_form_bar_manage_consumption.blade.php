@extends('layout')
@section('path')
    <li class="breadcrumb-item active">اطلاعات مدیریت مصرف بار‬</li>
    <li class="breadcrumb-item">ثبت اطلاعات مدیریت مصرف</li>
@endsection
@section('body')
    <form action="{{url('/panel/barManageConsumption')}}" method="post">
        @csrf
        <input type="hidden" name="start_date" value="{{$start_date}}"/>
        <input type="hidden" name="end_date" value="{{$end_date}}"/>
        <div class="row mb-5">
            <div class="col-12 mb-2">
                <div class="card">
                    <div class="card-header">
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از تاریخ :</span>
                                        <input class="form-control col-lg-5 col-md-6 col-sm-6"
                                               name="start_date"
                                               value="{{$start_date?? 0 ? verta_date_formatter($start_date) : ''}}"
                                               disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا تاریخ :</span>
                                        <input class="form-control col-lg-5 col-md-6 col-sm-6"
                                               name="end_date"
                                               value="{{$end_date?? 0 ? verta_date_formatter($end_date) : ''}}"
                                               disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               name="start_time" value="{{en2fa($start_time)}}" readonly/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6" name="end_time"
                                               value="{{en2fa($end_time)}}" readonly/>
                                    </label>
                                </div>
                            </div>
                        {{--                            <div class="col-md-4">--}}
                        {{--                                <div class="form-group">--}}
                        {{--                                    <label class="input-group">--}}
                        {{--                                        <span class="input-group-text">تاریخ :</span>--}}
                        {{--                                        <select class="form-control" name="date" onchange="HandleSelect(this)">--}}
                        {{--                                            @foreach($dates as $dt)--}}
                        {{--                                                <option value="{{$dt}}"--}}
                        {{--                                                        @if ($date == $dt)selected @endif>{{$dt}}</option>--}}
                        {{--                                            @endforeach--}}
                        {{--                                        </select>--}}
                        {{--                                    </label>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                            <div class="col-md-4" style="max-width: 250px">--}}
                        {{--                                <div class="form-group">--}}
                        {{--                                    <label class="input-group row" style="font-size: 1.2rem">--}}
                        {{--                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">روز</span>--}}
                        {{--                                        <input class="form-control col-lg-5 col-md-6 col-sm-6" value="{{$day_name}}"--}}
                        {{--                                               disabled/>--}}
                        {{--                                    </label>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        <!-- /.card -->
                        </div>
                        <div class="col-md-4 mb-3 mt-2">
                            <h6 class="text-info"><i class="fa fa-dot-circle-o"></i>&nbsp;تمامی فیلد ها به
                                مگاوات میباشند</h6>
                        </div>
                    </div>
                    <div class="sk-circle">
                        <div class="sk-circle1 sk-child"></div>
                        <div class="sk-circle2 sk-child"></div>
                        <div class="sk-circle3 sk-child"></div>
                        <div class="sk-circle4 sk-child"></div>
                        <div class="sk-circle5 sk-child"></div>
                        <div class="sk-circle6 sk-child"></div>
                        <div class="sk-circle7 sk-child"></div>
                        <div class="sk-circle8 sk-child"></div>
                        <div class="sk-circle9 sk-child"></div>
                        <div class="sk-circle10 sk-child"></div>
                        <div class="sk-circle11 sk-child"></div>
                        <div class="sk-circle12 sk-child"></div>
                    </div>

                    <!-- /.card-header -->
                    <div class="card-body hide-disp" style="direction: rtl" id="mycard-body">
                        <table id="myTable" class="table table-bordered table-striped" style="direction: rtl">
                            <thead>
                            <tr>
                                <th>طرح</th>
                                @foreach($times as $tm)
                                    <th>{{$tm < 10 ? en2fa(str_replace('0','',$tm)) : en2fa($tm)}}</th>
                                @endforeach
                                {{--                                <th>1</th>--}}
                                {{--                                <th>2</th>--}}
                                {{--                                <th>3</th>--}}
                                {{--                                <th>4</th>--}}
                                {{--                                <th>5</th>--}}
                                {{--                                <th>6</th>--}}
                                {{--                                <th>7</th>--}}
                                {{--                                <th>8</th>--}}
                                {{--                                <th>9</th>--}}
                                {{--                                <th>10</th>--}}
                                {{--                                <th>11</th>--}}
                                {{--                                <th>12</th>--}}
                                {{--                                <th>13</th>--}}
                                {{--                                <th>14</th>--}}
                                {{--                                <th>15</th>--}}
                                {{--                                <th>16</th>--}}
                                {{--                                <th>17</th>--}}
                                {{--                                <th>18</th>--}}
                                {{--                                <th>19</th>--}}
                                {{--                                <th>20</th>--}}
                                {{--                                <th>21</th>--}}
                                {{--                                <th>22</th>--}}
                                {{--                                <th>23</th>--}}
                                {{--                                <th>24</th>--}}

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($plans as $plan)
                                <tr child="{{$loop->index}}">
                                    <td>{{$plan->name}}</td>
                                    @php
                                        $lp = $loop->index;
                                    @endphp
                                    @foreach($times as $tm)
                                        <td><input name="{{$plan->id}}-{{$tm < 10 ? str_replace('0','',$tm) : $tm}}" class="form-control hour-{{$tm}}"
                                                   tabindex="{{$lp}}0{{$tm}}"
                                                   style="min-width: 80px !important;"
                                                   type="number"
                                                   value="{{$day_info->where('plan_id',$plan->id)->first()->hour_{$tm} ?? 0}}"
                                                   min="0"/></td>
                                    @endforeach
{{--                                    <td><input name="{{$plan->id}}-1" class="form-control hour-1"--}}
{{--                                               tabindex="{{$loop->index}}01"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_1 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-2" class="form-control hour-2"--}}
{{--                                               tabindex="{{$loop->index}}02"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_2 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-3" class="form-control hour-3"--}}
{{--                                               tabindex="{{$loop->index}}03"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_3 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-4" class="form-control hour-4"--}}
{{--                                               tabindex="{{$loop->index}}04"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_4 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-5" class="form-control hour-5"--}}
{{--                                               tabindex="{{$loop->index}}05"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_5 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-6" class="form-control hour-6"--}}
{{--                                               tabindex="{{$loop->index}}06"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_6 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-7" class="form-control hour-7"--}}
{{--                                               tabindex="{{$loop->index}}07"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_7 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-8" class="form-control hour-8"--}}
{{--                                               tabindex="{{$loop->index}}08"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_8 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-9" class="form-control hour-9"--}}
{{--                                               tabindex="{{$loop->index}}09"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_9 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-10" class="form-control hour-10"--}}
{{--                                               tabindex="{{$loop->index}}10"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_10 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-11" class="form-control hour-11"--}}
{{--                                               tabindex="{{$loop->index}}11"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_11 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-12" class="form-control hour-12"--}}
{{--                                               tabindex="{{$loop->index}}12"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_12 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-13" class="form-control hour-13"--}}
{{--                                               tabindex="{{$loop->index}}13"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_13 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-14" class="form-control hour-14"--}}
{{--                                               tabindex="{{$loop->index}}14"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_14 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-15" class="form-control hour-15"--}}
{{--                                               tabindex="{{$loop->index}}15"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_15 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-16" class="form-control hour-16"--}}
{{--                                               tabindex="{{$loop->index}}16"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_16 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-17" class="form-control hour-17"--}}
{{--                                               tabindex="{{$loop->index}}17"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_17 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-18" class="form-control hour-18"--}}
{{--                                               tabindex="{{$loop->index}}18"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_18 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-19" class="form-control hour-19"--}}
{{--                                               tabindex="{{$loop->index}}19"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_19 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-20" class="form-control hour-20"--}}
{{--                                               tabindex="{{$loop->index}}20"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_20 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-21" class="form-control hour-21"--}}
{{--                                               tabindex="{{$loop->index}}21"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_21 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-22" class="form-control hour-22"--}}
{{--                                               tabindex="{{$loop->index}}22"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_22 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-23" class="form-control hour-23"--}}
{{--                                               tabindex="{{$loop->index}}23"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_23 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
{{--                                    <td><input name="{{$plan->id}}-24" class="form-control hour-24"--}}
{{--                                               tabindex="{{$loop->index}}24"--}}
{{--                                               style="min-width: 80px !important;"--}}
{{--                                               type="number"--}}
{{--                                               value="{{$day_info->where('plan_id',$plan->id)->first()->hour_24 ?? 0}}"--}}
{{--                                               min="0"/></td>--}}
                                </tr>
                            @endforeach
                            <tr>
                                <td>جمع</td>
                                @foreach($times as $tm)
                                    <td><p id="total-{{$tm}}" class="form-control" style="min-width: 80px !important;"
                                           type="number">0</td>
                                @endforeach
{{--                                <td><p id="total-1" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-2" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-3" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-4" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-5" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-6" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-7" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-8" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-9" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-10" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-11" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-12" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-13" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-14" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-15" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-16" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-17" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-18" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-19" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-20" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-21" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-22" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-23" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
{{--                                <td><p id="total-24" class="form-control" style="min-width: 80px !important;"--}}
{{--                                       type="number">0</td>--}}
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-outline-success mb-3">تایید</button>
                        <a href="{{url('/panel/barManageConsumption')}}" class="btn btn-outline-danger mb-3">بازگشت</a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    </form>
    <style>
        .min-wd {
            min-width: 80px;
        }
    </style>
@endsection

@section('scripts')
    <script>

        $(function () {

            $("#myTable").DataTable({
                "language": {
                    "paginate": {
                        "next": "بعدی",
                        "previous": "قبلی"
                    }
                },
                "scrollY": 480,
                "scrollX": true,
                "info": false,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "autoWidth": false,
                "paging": false,
                // "colReorder": {
                //     order: [24, 23, 22, 21, 20,
                //         19, 18, 17, 16, 15, 14, 13, 12, 11, 10,
                //         9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
                // },

            });
            $('#mycard-body').removeClass('hide-disp');
            $('.sk-circle').addClass('no-disp');
            // $('.dt-picker').persianDatepicker({
            //     observer: true,
            //     format: 'YYYY/MM/DD',
            //     altField: '.observer-example-alt'
            // });
        });

        function calsum() {
            var h1sum = 0;
            var h2sum = 0;
            var h3sum = 0;
            var h4sum = 0;
            var h5sum = 0;
            var h6sum = 0;
            var h7sum = 0;
            var h8sum = 0;
            var h9sum = 0;
            var h10sum = 0;
            var h11sum = 0;
            var h12sum = 0;
            var h13sum = 0;
            var h14sum = 0;
            var h15sum = 0;
            var h16sum = 0;
            var h17sum = 0;
            var h18sum = 0;
            var h19sum = 0;
            var h20sum = 0;
            var h21sum = 0;
            var h22sum = 0;
            var h23sum = 0;
            var h24sum = 0;

            $('.hour-01').each(function () {
                h1sum += parseInt($(this).val());
            });
            $('.hour-02').each(function () {
                h2sum += parseInt($(this).val());
            });
            $('.hour-03').each(function () {
                h3sum += parseInt($(this).val());
            });
            $('.hour-04').each(function () {
                h4sum += parseInt($(this).val());
            });
            $('.hour-05').each(function () {
                h5sum += parseInt($(this).val());
            });
            $('.hour-06').each(function () {
                h6sum += parseInt($(this).val());
            });
            $('.hour-07').each(function () {
                h7sum += parseInt($(this).val());
            });
            $('.hour-08').each(function () {
                h8sum += parseInt($(this).val());
            });
            $('.hour-09').each(function () {
                h9sum += parseInt($(this).val());
            });
            $('.hour-10').each(function () {
                h10sum += parseInt($(this).val());
            });
            $('.hour-11').each(function () {
                h11sum += parseInt($(this).val());
            });
            $('.hour-12').each(function () {
                h12sum += parseInt($(this).val());
            });
            $('.hour-13').each(function () {
                h13sum += parseInt($(this).val());
            });
            $('.hour-14').each(function () {
                h14sum += parseInt($(this).val());
            });
            $('.hour-15').each(function () {
                h15sum += parseInt($(this).val());
            });
            $('.hour-16').each(function () {
                h16sum += parseInt($(this).val());
            });
            $('.hour-17').each(function () {
                h17sum += parseInt($(this).val());
            });
            $('.hour-18').each(function () {
                h18sum += parseInt($(this).val());
            });
            $('.hour-19').each(function () {
                h19sum += parseInt($(this).val());
            });
            $('.hour-20').each(function () {
                h20sum += parseInt($(this).val());
            });
            $('.hour-21').each(function () {
                h21sum += parseInt($(this).val());
            });
            $('.hour-22').each(function () {
                h22sum += parseInt($(this).val());
            });
            $('.hour-23').each(function () {
                h23sum += parseInt($(this).val());
            });
            $('.hour-24').each(function () {
                h24sum += parseInt($(this).val());
            });

            $('#total-01').html(h1sum);
            $('#total-02').html(h2sum);
            $('#total-03').html(h3sum);
            $('#total-04').html(h4sum);
            $('#total-05').html(h5sum);
            $('#total-06').html(h6sum);
            $('#total-07').html(h7sum);
            $('#total-08').html(h8sum);
            $('#total-09').html(h9sum);
            $('#total-10').html(h10sum);
            $('#total-11').html(h11sum);
            $('#total-12').html(h12sum);
            $('#total-13').html(h13sum);
            $('#total-14').html(h14sum);
            $('#total-15').html(h15sum);
            $('#total-16').html(h16sum);
            $('#total-17').html(h17sum);
            $('#total-18').html(h18sum);
            $('#total-19').html(h19sum);
            $('#total-20').html(h20sum);
            $('#total-21').html(h21sum);
            $('#total-22').html(h22sum);
            $('#total-23').html(h23sum);
            $('#total-24').html(h24sum);
        }

        calsum();
        $('input').change(function () {
            calsum();
        });

        function HandleSelect(elm) {
            let start_date = persianJs($('input[name="start_date"]').val()).persianNumber().toString();
            let end_date = persianJs($('input[name="end_date"]').val()).persianNumber().toString();
            let date = persianJs(elm.value).persianNumber().toString();
            window.location.href = "{{url('/panel/barManageConsumption/create?start_date=')}}" + start_date + '&end_date=' + end_date + '&date=' + date;
        }
    </script>
    <script>
        var inputs = document.querySelectorAll('input');

        inputs.forEach(input => {
            input.addEventListener('invalid', function (e) {
                e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")
            })

            input.addEventListener('input', function (e) {
                e.target.setCustomValidity("");
            })
        })
    </script>
@endsection