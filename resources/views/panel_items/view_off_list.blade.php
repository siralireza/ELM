@extends('layout')
@section('path')
    <li class="breadcrumb-item">مشاهده جدول خاموشی</li>
@endsection
@section('styles')
    <style>
        .table-row {
            cursor: pointer;
        }
    </style>
@endsection
@section('body')
    <div class="row mb-5">
        <div class="col-12 mb-2">
            <div class="card">
                <div class="card-header">
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="form-group">
                                        <label class="input-group row" style="font-size: 1.2rem">
                                            <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6 mx-auto"
                                                   disabled/>
                                        </label>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7 mb-3">
                                            <div style="float: right;">
                                                <button type="button" class="btn btn-dark" data-widget="collapse">
                                                    اطلاعات کلی (روزانه)
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="input-group row" style="font-size: 1.2rem">
                                                <span class="input-group-text col-lg-9 col-md-9 col-sm-9">ورژن استفاده
                                                    شده برای این جدول خاموشی</span>
                                                    <p class="form-control col-lg-3 col-md-6 col-sm-6">
                                                        @if(isset($off_table))
                                                            {{ $off_table->FeedersOff->first()->Feeder->history_version ?? 'موجود نیست' }}
                                                        @else
                                                            موجود نیست
                                                        @endif</p>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-4 mb-3 mt-2">
                                        <h6 class="text-info"><i class="fa fa-dot-circle-o"></i>&nbsp;تمامی فیلد ها به
                                            مگاوات میباشند</h6>
                                    </div>
                                </div>
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-bordered">
                                        @php
                                            $peak_times = [
                                                "8:00 - 09:00",
                                                "9:00 - 10:00",
                                                "10:00 - 11:00",
                                                "11:00 - 12:00",
                                                "12:00 - 13:00",
                                                "13:00 - 14:00",
                                                "14:00 - 15:00",
                                                "15:00 - 16:00",
                                                "16:00 - 17:00",
                                                "17:00 - 18:00",
                                                ];
                                        @endphp
                                        <thead>
                                        <tr>
                                            <th></th>
                                            @foreach($peak_times as $tm)
                                                <th class="text-center" style="white-space: nowrap">
                                                    <b>{{explode('-',$tm)[0]}}</b>
                                                </th>
                                            @endforeach
                                            <th>جمع کل</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <form action="{{url('/panel/viewOffList/dayInfo/barPeak')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="date" value="{{den2dfa($today)}}">
                                            <input type="hidden" name="off_table_id" value="{{$off_table->id ?? -1}}">

                                            <tr>
                                                <td>پیک بار روز</td>
                                                @foreach($peak_times as $tm)
                                                    <td><input type="number" class="form-control bar-peak" min="0"
                                                               @php
                                                                   try{
                                                                   $v= \App\DailyInfo::where([
                                                                   'date' => $today,
                                                                    'time' => str_replace(' ','',explode('-',$tm)[0]),])
                                                                    ->first()->bar_peak_mw/32;
                                                                   }catch (Exception $exception){
                                                                   $v = 0;
                                                                   }
                                                               @endphp
                                                               name="bar_peak[]" value="{{$v}}"
                                                               style="width:80px!important;"/></td>
                                                @endforeach
                                                <td>
                                                    <p id="total-peak">0</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                {{-- <td colspan="{{ count($peak_times)+2 }}"> --}}
                                                <td colspan="{{ 3 }}" style="border-left: none!important">
                                                    <div class="text-center my-3">
                                                        <button type="submit"
                                                                class="btn btn-lg btn-outline-success">
                                                            تایید
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </form>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-bordered" style="border-top: 1px solid #dee2e6">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            @foreach($times as $tm)
                                                <th class="text-center"
                                                    style="white-space: nowrap; border-top: solid!important">
                                                    <b>{{ explode('-',$tm)[0]}}</b>
                                                </th>
                                            @endforeach
                                            <th>جمع کل</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <form action="{{url('/panel/viewOffList/dayInfo/tavRealReq')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="date" value="{{den2dfa($today)}}">
                                            <input type="hidden" name="off_table_id" value="{{$off_table->id ?? -1}}">
                                            <tr>
                                                <td>بار خاموشی درخواست توانیر</td>
                                                @foreach($times as $tm)
                                                    <td><input type="number" class="form-control tav-req" min="0"
                                                               @php
                                                                   try{
                                                                   $v= \App\DailyInfo::where([
                                                               'date' => $today,
                                                                'time' => str_replace(' ','',explode('-',$tm)[0]),])
                                                                ->first()->tavanir_request/32;
                                                                   }catch (Exception $exception){
                                                                   $v = $off_table->OffItems->where('date',$today)->where('start_time',str_replace(' ','',explode('-',$tm)[0]).":00")->first() ? $off_table->OffItems->where('date',$today)->where('start_time',str_replace(' ','',explode('-',$tm)[0]).":00")->first()->predict_tavanir_request - $off_table->OffItems->where('date',$today)->where('start_time',str_replace(' ','',explode('-',$tm)[0]).":00")->first()->manage_consumption : 0;
                                                                   }
                                                               @endphp
                                                               name="tavanir_real_req[]" value="{{$v}}"
                                                               style="width:80px!important;"/></td>
                                                @endforeach
                                                <td>
                                                    <p id="total-tav-req">0</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                {{-- <td colspan="{{ count($times)+2 }} }}"> --}}

                                                <td colspan="{{ 2 }} }}" style="border-left: none!important">
                                                    <div class="text-center my-3">
                                                        <button type="submit"
                                                                class="btn btn-lg btn-outline-success">
                                                            تایید
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </form>
                                        <tr>
                                            <td style="white-space: nowrap">پیش بینی بار مدیریت مصرف</td>
                                            @php
                                                $s = 0
                                            @endphp

                                            @foreach($off_table->OffItems ?? false ?
                                            $off_table->OffItems->where('date',$today) : [] ?? [] as $mc)
                                                <td>{{$mc->manage_consumption ? $mc->manage_consumption/32 : 0 }}</td>
                                                @php
                                                    $s += $mc->manage_consumption;
                                                @endphp
                                            @endforeach
                                            <td>{{$s}}</td>
                                        </tr>
                                        <form action="{{url('/panel/viewOffList/dayInfo/realConsumption')}}"
                                              method="post">
                                            @csrf
                                            <input type="hidden" name="date" value="{{den2dfa($today)}}">
                                            <input type="hidden" name="off_table_id" value="{{$off_table->id ?? -1}}">
                                            <tr>
                                                <td>مقدار مدیریت بار ساعتی درخواستی</td>
                                                @foreach($times as $tm)
                                                    <td><input type="number" class="form-control real-cons" min="0"
                                                               @php
                                                                   try{
                                                                   $val = App\DailyInfo::where([
                                                                          'date' => $today,
                                                                           'time' => str_replace(' ','',explode('-',$tm)[0]),])
                                                                           ->first()->real_cons/32 ?? 0;
                                                                   }catch (Exception $exception){
                                                                   $val = false;
                                                                   }
                                                               @endphp
                                                               name="real_cons[]"
                                                               value="@php
                                                                    if ($val)
                                                                        echo $val;
                                                                    elseif($off_table->OffItems ?? false){
                                                                        echo $off_table->OffItems->where('date',$today)->where('start_time',str_replace(' ','',explode('-',$tm)[0]).":00")->first() ? $off_table->OffItems->where('date',$today)->where('start_time',str_replace(' ','',explode('-',$tm)[0]).":00")->first()->manage_consumption/32 : 0;
                                                                    }
                                                                    else
                                                                        echo 0;
                                                                    @endphp"
                                                               style="width:80px!important;"/></td>
                                                @endforeach
                                                <td>
                                                    <p id="total-real-cons">0</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="{{ 2 }}" style="border-left: none!important">
                                                    <div class="text-center my-3">
                                                        <button type="submit"
                                                                class="btn btn-lg btn-outline-success">
                                                            تایید
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </form>

                                        {{-- <tr>
                                            <td style="white-space: nowrap">مقدار بار قطع شده</td>
                                            @foreach($times as $tm)
                                            <td>0</td>
                                            @endforeach
                                            <td>0</td>
                                        </tr> --}}
                                        <tr>
                                            <td style="white-space: nowrap">انرژی توزیع نشده</td>
                                            @foreach($times as $tm)
                                            <td>0</td>
                                            @endforeach
                                            <td>0</td>
                                        </tr>
                                        {{--<tr>
                                            <td style="white-space: nowrap">تعداد فیدر های قطع شده</td>
                                            @foreach($times as $tm)
                                            <td>0</td>
                                            @endforeach
                                            <td>0</td>

                                        </tr> --}}
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <form action="{{url('/panel/viewOffList/dayInfo/dayPeak')}}" method="post">
                                        <div class="row">
                                            <div class="col-md-3" style="max-width: 400px;min-width: 200px">
                                                <div class="form-group">
                                                    @csrf
                                                    <input type="hidden" name="date" value="{{den2dfa($today)}}">
                                                    <label class="input-group row" style="font-size: 1.2rem">
                                                    <span class="input-group-text col-lg-8 col-md-8 col-sm-6">پیک
                                                        روز<br>(پیش بینی بازار برق)</span>
                                                        <input class="form-control" type="number" min="0" name="peak"
                                                               value="{{(\App\DayPeak::where('date',$today)->first()->peak ?? 0) ? (\App\DayPeak::where('date',$today)->first()->peak ?? 0)/32 : 0}}">
                                                    </label>

                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-4">
                                                <button type="submit" class="mt-2 btn btn-lg btn-outline-success">تایید
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="input-group row" style="font-size: 1.2rem">
                                                <span class="input-group-text col-lg-6 col-md-6 col-sm-6">حداکثر
                                                    دما</span>
                                                    <p class="form-control col-lg-5 col-md-6 col-sm-6" id="max-temp">
                                                        0</p>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="input-group row" style="font-size: 1.2rem">
                                                <span class="input-group-text col-lg-6 col-md-6 col-sm-6">حداقل
                                                    دما</span>
                                                    <p class="form-control col-lg-5 col-md-6 col-sm-6" id="min-temp">
                                                        0</p>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="input-group row" style="font-size: 1.2rem">
                                                <span class="input-group-text col-lg-6 col-md-6 col-sm-6">میزان
                                                    رطوبت</span>
                                                    <p class="form-control col-lg-5 col-md-6 col-sm-6" id="humidity">
                                                        0</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="input-group row" style="font-size: 1.2rem">
                                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تعداد فیدر های قطع شده</span>
                                                    <p class="form-control col-lg-5 col-md-6 col-sm-6" id="max-temp">
                                                        {{ $off_feeders ?? 0 }}</p>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="input-group row" style="font-size: 1.2rem">
                                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">خاموشی امروز</span>
                                                    <p class="form-control col-lg-5 col-md-6 col-sm-6" id="min-temp">
                                                        {{ isset($still_off_bar) ? $still_off_bar/32 : 0 }}</p>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="input-group row" style="font-size: 1.2rem">
                                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">خاموشی اعلام شده امروز</span>
                                                    <p class="form-control col-lg-5 col-md-6 col-sm-6" id="humidity">
                                                        {{ isset($today_offed_bar) ? $today_offed_bar/32 : 0 }}</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>

                </div>
                <!-- /.card-header -->
                <div class="text-center mt-3">
                    <button type="submit" class="btn btn-primary">بروز رسانی جدول</button>
                </div>
                <div class="card-body" style="direction: rtl">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-2">
                                    <table id="myTable" class="table table-bordered table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            {{--                                            <th>موقعیت</th>--}}
                                            <th>توضیحات واقعی قطع</th>
                                            <th>وضعیت خاموشی</th>
                                            <th>امور</th>
                                            <th>پست فوق توزیع</th>
                                            <th>فیدر</th>
                                            {{--                                            <th>بار پیش بینی فیدر</th>--}}
                                            <th>آدرس</th>
                                            <th>بازه قطع</th>
                                            <th>محدودیت</th>
                                            <th>زمان واقعی قطع</th>
                                            <th>بار واقعی قطع</th>
                                            <th>زمان باقی مانده خاموشی</th>
                                            <th>زمان وصل</th>
                                            <th>مدت خاموشی</th>
                                            <th>انرژی توزیع نشده</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($today_feeders as $f)
                                            <tr @if(!$f->allowed and $f->status == 'off') class="bg-danger table-row"
                                                @elseif(!$f->allowed) class="bg-dark table-row"
                                                @elseif($f->status == 'off' and
                                                \Carbon\Carbon::now('Asia/Tehran')->diffInMinutes($f->off_time_end) < 300)
                                                class="flasher table-row"
                                                @elseif($f->status == 'off') class="bg-danger table-row"
                                                @else class="table-row" @endif data-content="{{$f->Feeder->guid}}">
                                                <td>{{$loop->index +1 }}</td>
                                                {{--                                                <td><button class="btn btn-danger mapBtn" data-content="{{$f->Feeder->guid}}"><i class="fa fa-map-marker"></i></button></td>--}}
                                                <td>{{$f->description}}</td>
                                                <td>{{$f->status == 'on' ? 'روشن' : 'خاموش'}}</td>
                                                <td>{{ en2fa($f->Feeder->zone_name) }}</td>
                                                <td>{{$f->Feeder->post}}</td>
                                                <td>{{$f->Feeder->name}}</td>
                                                {{--                                                <td>--}}
                                                {{--                                                    --}}{{--                                                        {{$f->Feeder->Bar->hour}}--}}
                                                {{--                                                </td>--}}
                                                <td>{{$f->Feeder->address}}</td>
                                                <td style="white-space: nowrap">
                                                    {{en2fa(\Carbon\Carbon::createFromTimeString($f->off_time_end)->format('H:i')) .' - '.en2fa(\Carbon\Carbon::createFromTimeString($f->off_time_start)->format('H:i'))}}
                                                </td>
                                                <td>{{en2fa($f->Feeder->limit)}}<br>
                                                    {{en2fa($f->Feeder->both)}}
                                                </td>
                                                @php
                                                    try {
                                                        $off_time = en2fa(\Carbon\Carbon::createFromTimeString($f->off_real_time)->format('H:i'));
                                                    } catch (\Throwable $th) {
                                                        $off_time = '';
                                                    }

                                                    try {
                                                        $on_time = en2fa(\Carbon\Carbon::createFromTimeString($f->on_real_time)->format('H:i'));
                                                    } catch (\Throwable $th) {
                                                        $on_time = '';
                                                    }
                                                @endphp
                                                <td>{{ $off_time}}</td>
                                                <td>{{$f->off_real_bar ? $f->off_real_bar/32 : ''}}</td>
                                                <td>@if($f->status == 'off')
                                                        {{en2fa( \Carbon\Carbon::now('Asia/Tehran')->diffInMinutes($f->off_time_end)) }}
                                                        دقیقه @endif</td>
                                                <td>{{ $on_time }}</td>
                                                <td>{{en2fa($f->OffTable->period)}}</td>
                                                <td></td>
                                                <td>
                                                    @if($f->status == 'off')
                                                        <button class="btn btn-sm btn-warning" data-toggle="modal"
                                                                data-target="#myOnModal{{$loop->index}}">روشن
                                                        </button>
                                                    @else
                                                        <button class="btn btn-sm btn-warning"
                                                                @if(!$f->allowed or $f->off_real_time)
                                                                data-conf="confirmation"
                                                                data-title=" فیدر {{$f->Feeder->name}} طی
                                                        {{en2fa(\App\Setting::where('name','days')->latest()->first()->value ?? 0) ?? '0'}}
                                                                        روز گذشته خاموش شده است آیا مایل به خاموش کردن آن می باشید؟"
                                                                data-toggle="modal"
                                                                @if($f->Feeder->LimitedPartners->isEmpty() and $f->Feeder->Contributers->isEmpty())
                                                                data-target="#myOffModal{{$loop->index}}">خاموش
                                                            @else
                                                                data-target="#cont-Modal{{$loop->index}}">خاموش
                                                            @endif

                                                            @else data-toggle="modal"
                                                            @if($f->Feeder->LimitedPartners->isEmpty() and $f->Feeder->Contributers->isEmpty())
                                                                data-target="#myOffModal{{$loop->index}}">درنوبت قطع
                                                            @else
                                                                data-target="#cont-Modal{{$loop->index}}">درنوبت قطع
                                                            @endif
                                                            @endif
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div style="width: 100%;height:500px;" id="mapWebGis"></div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

    @foreach($today_feeders as $f)
        <div class="modal" id="myOffModal{{$loop->index}}">
            <div class="modal-dialog modal-dialog-centered">
                <form action="{{url('/panel/viewOffList/toggleFeeder/off/'.$f->id)}}" method="post">
                    @csrf
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">خاموش کردن فیدر</h4>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="input-group row mx-auto" style="font-size: 1.2rem">
                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">بار واقعی قطع :</span>
                                    <input type="number" min="0" value="0" name="off_real_bar"
                                           class="form-control col-lg-5 col-md-6 col-sm-6"/>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="input-group" style="font-size: 1.2rem">
                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">ساعت واقعی قطع :</span>
                                    <input name="off_real_time"
                                           class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"/>
                                </label>
                            </div>
                            <div class="form-group">
                                <label style="font-size: 1.2rem">
                                    <span class="input-group-text">توضیحات</span>
                                </label>
                                <textarea name="description" class="form-control" rows="3"
                                          placeholder="وارد کردن اطلاعات ..."></textarea>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success mx-1">تایید</button>
                            <button type="button" class="btn btn-danger mx-1" data-dismiss="modal">بستن</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <div class="modal" id="myOnModal{{$loop->index}}">
            <div class="modal-dialog modal-dialog-centered">
                <form action="{{url('/panel/viewOffList/toggleFeeder/on/'.$f->id)}}" method="post">
                    @csrf
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">روشن کردن فیدر</h4>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            {{-- <div class="form-group">
                                <label class="input-group row mx-auto" style="font-size: 1.2rem">
                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">بار واقعی وصل :</span>
                                    <input type="number" min="0" value="0" name="on_real_bar"
                                        class="form-control col-lg-5 col-md-6 col-sm-6" />
                                </label>
                            </div> --}}
                            <div class="form-group">
                                <label class="input-group" style="font-size: 1.2rem">
                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">ساعت واقعی وصل :</span>
                                    <input name="on_real_time"
                                           class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"/>
                                </label>
                            </div>
                            <div class="form-group">
                                <label style="font-size: 1.2rem">
                                    <span class="input-group-text"> توضیحات <span class="text-danger">(الزامی)</span></span>
                                </label>
                                <textarea name="description" class="form-control" rows="3"
                                          placeholder="وارد کردن اطلاعات ..." required></textarea>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success mx-1">تایید</button>
                            <button type="button" class="btn btn-danger mx-1" data-dismiss="modal">بستن</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    @endforeach
    <!-- The Modal -->
    @foreach($today_feeders as $f)
        <div class="modal" id="cont-Modal{{$loop->index}}">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                @csrf
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">لیست مشترکین</h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-group">
                                    <li class="list-group-item">لیست مشترکین با محدودیت قطع</li>
                                    @foreach($f->Feeder->LimitedPartners ? $f->Feeder->LimitedPartners : [] as $lp)
                                        <li class="list-group-item">{{$lp->limited_partner}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group">
                                    <li class="list-group-item">لیست مشترکین همکار</li>
                                    @foreach($f->Feeder->Contributers ? $f->Feeder->Contributers : [] as $c)
                                        <li class="list-group-item">{{$c->contributer}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" data-toggle="modal" data-target="#myOffModal{{$loop->index}}"
                                data-dismiss="modal" class="btn btn-success mx-1">ادامه
                        </button>
                        <button type="button" class="btn btn-danger mx-1" data-dismiss="modal">بستن</button>
                    </div>

                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')
    <script>
        $(function () {
            var options = {
                "language": {
                    "paginate": {
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                    "sSearch": "جست و جو : ",

                },
                "scrollY": 480,
                "scrollX": true,
                "info": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "autoWidth": true,
                // "processing": true,
                // "colReorder": {
                //     order: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
                // ,}
            };
            const table = $("#myTable").DataTable(options);
            $('div.col-sm-12.col-md-6:first').css('display', 'none');

            $('[data-conf=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                btnOkLabel: 'بله',
                btnOkClass: 'btn btn-danger',
                btnCancelLabel: 'خیر',
                btnCancelClass: 'btn btn-success',
                popout: true,
            });
            $('.dt-picker').persianDatepicker();
            $('.bar-peak').change(function () {
                CalcSum();
            });
            $('.tav-req').change(function () {
                CalcSum();
            });
            $('.real-cons').change(function () {
                CalcSum();
            });

            function CalcSum() {
                var sum1 = 0;
                var sum2 = 0;
                var sum3 = 0;

                $('.bar-peak').each(function () {
                    sum1 += parseInt($(this).val());
                });
                $('.tav-req').each(function () {
                    sum2 += parseInt($(this).val());
                });
                $('.real-cons').each(function () {
                    sum3 += parseInt($(this).val());
                });
                $('#total-peak').html(sum1.toString());
                $('#total-tav-req').html(sum2.toString());
                $('#total-real-cons').html(sum3.toString());
            }

            CalcSum();

            window.webgis = new webgisApi('mapWebGis', {
                system: '121'
            });
            window.webgis.Load();

            window.webgis.on('afterMapLoad', function () {
                window.webgis.listeners.afterFindFFT = function () {
                };
                @foreach($reses ?? [] as $r)
                window.webgis.showGeoJson({!! $r !!},
                    {
                        clear: false,
                        extent: true,
                        color: [79, 248, 18, 0.72],
                        borderColor: [250, 0, 0, 1],
                    });
                @endforeach
            });

            $('.table-row').on('click', function () {
                $('html, body').animate({
                    scrollTop: $(document).height()
                }, 'slow');
                $guid = $(this).attr('data-content');
                window.webgis.findFFT($guid, 'hv_feeder', {}, function (success, data) {
                    console.log(data[0]);
                    window.webgis.showGeoJson(data[0].shape,
                        {
                            clear: false,
                            extent: true,
                        })
                });


            });
        });
        $('.tm-picker').persianDatepicker({
            onlyTimePicker: true,
            timePicker: {
                second: {
                    enabled: false,
                },
                minute: {
                    enabled: true,
                    step: 15,
                }
            },
            format: 'H:m',
        });

        //todo comment this on production
        Weather.setApiKey('{{env('WEATHER_API_KEY')}}');
        Weather.getCurrent("mashhad", function (current) {
            $('#max-temp').html(current.max_temperature());
            $('#min-temp').html(current.min_temperature());
            $('#humidity').html(current.humidity());
        });

        setInterval(function () {
            if ($('.flasher').hasClass('bg-danger'))
                $('.flasher').removeClass('bg-danger').addClass('bg-warning');
            else if ($('.flasher').hasClass('bg-warning'))
                $('.flasher').removeClass('bg-warning').addClass('bg-danger');
            else
                $('.flasher').addClass('bg-danger');
        }, 500);
        $(document).ready(function () {
            setInterval('window.location.reload()', 60000);
        });

    </script>
    <script>
        var inputs = document.querySelectorAll('input');

        inputs.forEach(input => {
            input.addEventListener('invalid', function (e) {
                e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")
            })

            input.addEventListener('input', function (e) {
                e.target.setCustomValidity("");
            })
        })
    </script>
@endsection
