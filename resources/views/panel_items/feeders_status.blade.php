@extends('layout')
@section('path')
<li class="breadcrumb-item">وضعیت فیدر‌ها</li>
@endsection
@section('styles')
<style>
    .non-disp {
        display: none;
    }

    .hid-disp {
        visibility: hidden;
    }
    .limit-inputs {
        margin: 8px 0px;
    }
</style>
@endsection
@section('body')
<form action="{{url('/panel/feedersStatus')}}" id="my-form" method="post">
    @csrf
    <div class="row mb-5">
        <div class="col-12 mb-2">
            <div class="card">
                <div class="card-header">
                    <div class="row mt-2">
                        <div class="col-md-2 mb-3" style="max-width: 210px;min-width: 200px">
                            <div class="btn-group">
                                <button type="button" class="btn btn-info disabled">ویرایش
                                    اطلاعات {{$current_version ?? 0}}<br>
                                </button>
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" role="menu">
                                    @foreach($versions as $dt => $version)
                                    <a class="dropdown-item" href="{{url('/panel/feedersStatus/'.$version)}}">ویرایش
                                        {{$version}}<br>
                                        {{ $dt ? '('.verta_date_formatter(Verta($dt)->formatDate()).')' ?? '' : '' }}
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="max-width: 350px;min-width: 350px">
                            <div class="form-group">
                                <label class="input-group row" style="font-size: 1.2rem">
                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تاریخ ثبت ویرایش :</span>
                                    <input type="text" class="form-control col-lg-4 col-md-6 col-sm-6 date-input
                                               @if (isset($date))
                                                " value="{{en2fa(den2dfa($date))}}" @else dt-picker" @endif name="date" disabled />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3" style="max-width: 350px;min-width: 350px">
                            <div class="form-group">
                                <label class="input-group row" style="font-size: 1.2rem">
                                    <span class="input-group-text col-xl-9 col-lg-8 col-md-10 col-sm-6">تعداد فیدر‌هایی
                                        که امکان قطع دارند :</span>
                                    <input class="form-control col-xl-2 col-lg-3 col-md-2 col-sm-4" disabled
                                        value="{{$breakable_feeders ?? $feeders['total']}}" id="breakable-feeders" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3" style="max-width: 340px;min-width: 350px">
                            <div class="form-group">
                                <label class="input-group row" style="font-size: 1.2rem">
                                    <span class="input-group-text col-xl-10 col-lg-8 col-md-10 col-sm-6">تعداد فیدر هایی
                                        که امکان قطع ندارند :</span>
                                    <input class="form-control col-xl-2 col-lg-3 col-md-2 col-sm-4" disabled
                                        value="{{$unbreakable_feeders ?? 0}}"
                                        id="unbreakable-feeders" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1" style="max-width: 340px;min-width: 340px">
                            <div class="form-group">
                                <label class="input-group row" style="font-size: 1.2rem">
                                    <span class="input-group-text col-xl-5 col-lg-6 col-md-9 col-sm-8">تعداد کل فیدر
                                        :</span>
                                    <input class="form-control col-xl-2 col-lg-3 col-md-3 col-sm-4" disabled
                                        value="{{ $total_feeders??$feeders['total'] }}" id="all-feeders" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>

                <!-- /.card-header -->
                <div class="card-body hide-disp" style="direction:rtl" id="mycard-body">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ردیف</th>
                                <th>امور</th>
                                <th>پست فوق‌ توزیع</th>
                                <th>فیدر</th>
                                <th>مسیر تغذیه</th>
                                <th>آدرس</th>
                                <th>امکان قطع</th>
                                <th>نوع قطع</th>
                                <th>محدودیت</th>
                                <th>نحوه بارگیری</th>
                                <th>توضیحات</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($feeders['data'] ?? $feeders as $feeder)
                            <tr @if($feeder->is_deleted ?? 0) class="bg-danger" @endif>
                                <input type="hidden" name="feeder_id[]" value="{{$feeder->id ?? $loop->index}}" />
                                <input type="hidden" name="global_feeder_id[]"
                                    value="{{$feeder->global_id ?? $feeder['feeder_id'] ?? ''}}" />
                                <input type="hidden" name="zone_name[]"
                                    value="{{$feeder['zone_name'] ?? $feeder->zone_name ?? ''}}" />
                                <input type="hidden" name="zone_id[]"
                                    value="{{$feeder['zone_id'] ?? $feeder->zone_id ?? ''}}" />
                                <input type="hidden" name="post[]"
                                    value="{{$feeder['post_name'] ?? $feeder->post ?? ''}}" />
                                <input type="hidden" name="name[]"
                                    value="{{$feeder['feeder_name'] ?? $feeder->name ?? ''}}" />
                                <input type="hidden" name="guid[]"
                                    value="{{$feeder['guid'] ?? $feeder->guid ?? ''}}" />
                                <input type="hidden" name="path[]" value="{{$feeder['path'] ?? $feeder->path ?? ''}}" />
                                {{-- <input type="hidden" name="address[]"
                                    value="{{$feeder['address'] == null ?? $feeder->address ?? ''}}" /> --}}

                                <th>{{$loop->index +1 }}</th>
                                <th>{{en2fa($feeder['zone_name']) ?? en2fa($feeder->zone_name)}}</th>
                                <th>{{$feeder['post_name'] ?? $feeder->post}}</th>
                                <th>{{$feeder['feeder_name'] ?? $feeder->name}}</th>

                                {{--<th>{{$feeder['feeder_bar'] ?? $feeder->name}}</th>--}}
                                <th>{{$feeder['path'] ?? $feeder->path}}</th>
                                <th><input type="text" name="address[]" style="min-width: 150px;" value="{{ $feeder->address ?? '' }}" class="form-control des-tooltip" data-toggle="tooltip"></th>
                                <th>
                                    <div class="form-group">
                                        <select class="form-control break-input" name="breakable[]"
                                            data-id="{{$loop->index}}">
                                            @if(isset($feeder->breakable) and !$feeder->breakable)
                                                <option value="0" selected>ندارد</option>
                                                <option value="1">دارد</option>
                                            @else
                                                <option value="0">ندارد</option>
                                                <option value="1" selected>دارد</option>
                                            @endif
                                        </select>
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                        <select class="form-control break-type @if(isset($feeder->breakable) and $feeder->breakable == 0) unclickable
                                                    {{-- @elseif(!isset($feeder->breakable)) unclickable  --}}
                                                    @endif"
                                            name="break_type[]" data-id="{{$loop->index}}"
                                            style="min-width: 150px!important;">

                                            @if(isset($feeder->break_type) and $feeder->break_type == 'none')
                                            {{-- <option class="tmp-dis" value="none" selected disabled></option> --}}
                                            <option value="no_limit">بدون محدودیت</option>
                                            <option value="time_limit">محدودیت زمانی</option>
                                            <option value="maneuver">از طریق مانور</option>
                                            <option value="both">هردو محدودیت</option>
                                            @elseif(isset($feeder->break_type) and $feeder->break_type == 'no_limit')
                                            {{-- <option class="tmp-dis" value="none" disabled></option> --}}
                                            <option value="no_limit" selected>بدون محدودیت</option>
                                            <option value="time_limit">محدودیت زمانی</option>
                                            <option value="maneuver">از طریق مانور</option>
                                            <option value="both">هردو محدودیت</option>
                                            @elseif(isset($feeder->break_type) and $feeder->break_type == 'time_limit')
                                            {{-- <option class="tmp-dis" value="none" disabled></option> --}}
                                            <option value="no_limit">بدون محدودیت</option>
                                            <option value="time_limit" selected>محدودیت زمانی</option>
                                            <option value="maneuver">از طریق مانور</option>
                                            <option value="both">هردو محدودیت</option>
                                            @elseif(isset($feeder->break_type) and $feeder->break_type == 'maneuver')
                                            {{-- <option class="tmp-dis" value="none" disabled></option> --}}
                                            <option value="no_limit">بدون محدودیت</option>
                                            <option value="time_limit">محدودیت زمانی</option>
                                            <option value="maneuver" selected>از طریق مانور</option>
                                            <option value="both">هردو محدودیت</option>
                                            @elseif(isset($feeder->break_type) and $feeder->break_type == 'both')
                                            {{-- <option class="tmp-dis" value="none" disabled></option> --}}
                                            <option value="no_limit">بدون محدودیت</option>
                                            <option value="time_limit">محدودیت زمانی</option>
                                            <option value="maneuver">از طریق مانور</option>
                                            <option value="both" selected>هردو محدودیت</option>
                                            @else
                                            {{-- <option class="tmp-dis" value="none" disabled></option> --}}
                                            <option value="no_limit" selected>بدون محدودیت</option>
                                            <option value="time_limit">محدودیت زمانی</option>
                                            <option value="maneuver">از طریق مانور</option>
                                            <option value="both">هردو محدودیت</option>
                                            @endif

                                        </select>
                                    </div>
                                </th>
                                <th>
                                    @if(isset($feeder->break_type) and ($feeder->break_type == 'time_limit'))
                                    <div id="other">
                                        <input class="form-control break-description" name="limit[]"
                                            data-id="{{$loop->index}}" type="text"
                                            value="{{en2fa($feeder->limit) ?? ''}}" readonly>
                                    </div>
                                    <div class="tm-limit-div mt-3" data-id="{{$loop->index}}">
                                        <label class="input-group">
                                            <span class="input-group-text">از</span>
                                            <input class="form-control tm-limit tm-active tm-limit-from tm-picker"
                                                style="max-width: 100px;" data-id="{{$loop->index}}" id="from">
                                        </label>
                                        <label class="input-group">
                                            <span class="input-group-text">تا</span>
                                            <input class="form-control tm-limit tm-active tm-limit-to tm-picker"
                                                style="max-width: 100px;" data-id="{{$loop->index}}" id="to">
                                        </label>
                                    </div>
                                    @elseif(isset($feeder->break_type) and ($feeder->break_type == 'both'))
                                    <div id="other">
                                            <input class="form-control break-description des-tooltip" style="min-width: 120px" name="limit[]"
                                                data-id="{{$loop->index}}" type="text"
                                                value="{{en2fa($feeder->limit) ?? ''}}" readonly data-toggle="tooltip">
                                                <input class="form-control both-break-description des-tooltip" name="both[]"
                                                data-id="{{$loop->index}}" type="text" value="{{$feeder->both ?? ''}}" data-toggle="tooltip">
                                            </div>
                                            <div class="tm-limit-div mt-3" data-id="{{$loop->index}}">
                                                <label class="input-group">
                                                    <span class="input-group-text">از</span>
                                                    <input class="form-control tm-limit tm-active tm-limit-from tm-picker"
                                                        style="max-width: 100px;" data-id="{{$loop->index}}" id="from">
                                                </label>
                                                <label class="input-group">
                                                    <span class="input-group-text">تا</span>
                                                    <input class="form-control tm-limit tm-active tm-limit-to tm-picker"
                                                        style="max-width: 100px;" data-id="{{$loop->index}}" id="to">
                                                </label>
                                            </div>
                                    @else
                                    <div id="other">
                                        <input class="form-control break-description des-tooltip" style="min-width: 120px" name="limit[]"
                                            data-id="{{$loop->index}}" type="text" value="{{$feeder->limit ?? ''}}"
                                            @if(isset($feeder->break_type) and ($feeder->break_type == 'none' or
                                        $feeder->break_type == 'no_limit')) readonly
                                        @elseif(!isset($feeder->break_type))
                                        readonly
                                        @endif data-toggle="tooltip">
                                        <input class="form-control both-break-description non-disp des-tooltip" name="both[]"
                                                data-id="{{$loop->index}}" type="text"
                                                value="{{en2fa($feeder->limit ?? '') ?? ''}}" data-toggle="tooltip">
                                    </div>
                                    <div class="tm-limit-div non-disp" data-id="{{$loop->index}}">
                                        <label class="input-group">
                                            <span class="input-group-text">از</span>
                                            <input class="form-control tm-limit tm-limit-from tm-picker"
                                                style="max-width: 100px;" data-id="{{$loop->index}}" id="from">
                                        </label>
                                        <label class="input-group">
                                            <span class="input-group-text">تا</span>
                                            <input class="form-control tm-limit tm-limit-to tm-picker"
                                                style="max-width: 100px;" data-id="{{$loop->index}}" id="to">
                                        </label>
                                    </div>
                                    @endif
                                </th>
                                <th>
                                    <div class="form-group">


                                        {{--                                            @if(isset($feeder->bar_type) and $feeder->bar_type == 'non_system')--}}
                                        {{--                                            <input type="text" class="form-control bar-type unclickable"--}}
                                        {{--                                                   name="bar_type[]"--}}
                                        {{--                                                   data-id="{{$loop->index}}"
                                        readonly--}}
                                        {{--                                                   style="min-width: 120px!important;" value="غیرسیستمی">--}}
                                        {{--                                            @else--}}
                                        {{--                                                <input type="text" class="form-control bar-type unclickable"--}}
                                        {{--                                                       name="bar_type[]"--}}
                                        {{--                                                       data-id="{{$loop->index}}"
                                        readonly--}}
                                        {{--                                                       style="min-width: 120px!important;" value="سیستمی">--}}
                                        {{--                                            @endif--}}
                                        <select class="form-control bar-type unclickable" name="bar_type[]"
                                            data-id="{{$loop->index}}" readonly style="min-width: 120px!important;">
                                            @if(isset($feeder->bar_type) and $feeder->bar_type == 'non_system')
                                            <option value="system">سیستمی</option>
                                            <option value="non_system" selected>غیر سیستمی</option>
                                            @else
                                            <option value="system" selected>سیستمی</option>
                                            <option value="non_system">غیر سیستمی</option>
                                            @endif

                                        </select>
                                    </div>
                                </th>
                                <th><input type="text" name="description[]" class="form-control des-tooltip"
                                        value="{{$feeder->description ?? ''}}" data-toggle="tooltip"></th>
                                <th>
                                    <button type="button"
                                        class="btn btn-sm btn-primary mb-3 @if(!isset($feeder->id)) disabled" disabled
                                        @else " @endif data-toggle="modal" data-target="#Modal-{{$loop->index}}">تعیین
                                        بار فیدر
                                        @if(!isset($feeder->Bar))
                                        <br>
                                        <span class="badge badge-warning">تعیین نشده</span>
                                        @endif
                                    </button>
                                </th>
                                <th>
                                    <button type="button"
                                        class="btn btn-sm btn-warning mb-3 @if(!isset($feeder->id)) disabled" disabled
                                        @else " @endif data-toggle="modal"
                                        data-target="#Limit-Modal-{{$loop->index}}">مشترکین با محدودیت قطع
                                        @if(isset($feeder) and isset($feeder->LimitedPartners) and $feeder->LimitedPartners->isEmpty())
                                        <br>
                                        <span class="badge badge-danger">ندارد</span>
                                        @endif
                                    </button>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="text-center">
                        <button type="submit" class="btn btn-success">تایید تغییرات جهت ایجاد ویرایش جدید</button>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
</form>
@foreach($feeders['data'] ?? $feeders as $feeder)

<div class="modal" id="Modal-{{$loop->index}}">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form action="{{url('/panel/feedersStatus/setFeederBar')}}" method="post">
                @csrf
                <input type="hidden" name="feeder_id" value="{{$feeder->id ?? -1}}">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h2 class="modal-title">وارد کردن بار فیدر</h2>
                </div>

                <!-- Modal body -->
                <div class="modal-body table-responsive">
                    <h4 class="text-info text-center my-3"><span class="bg-dark rounded p-2"> نام فیدر:
                            {{$feeder['feeder_name'] ?? $feeder->name}}</span>
                    </h4>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ساعت</th>
                                <th>مقدار (آمپر)</th>
                                <th>ساعت</th>
                                <th>مقدار (آمپر)</th>
                                <th>ساعت</th>
                                <th>مقدار (آمپر)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span>1</span><button type="button" class="btn btn-sm btn-info"
                                        onclick="copy_first_row(this)">کپی در بقیه                                 </button>
                                </td>
                                <td><input name="hour_1" class="form-control hour-1" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_1 ?? 0}}" min="0" /></td>
                                <td>2</td>
                                <td><input name="hour_2" class="form-control hour-2" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_2 ?? 0}}" min="0" /></td>
                                <td>3</td>
                                <td><input name="hour_3" class="form-control hour-3" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_3 ?? 0}}" min="0" /></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><input name="hour_4" class="form-control hour-4" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_4 ?? 0}}" min="0" /></td>
                                <td>5</td>
                                <td><input name="hour_5" class="form-control hour-5" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_5 ?? 0}}" min="0" /></td>
                                <td>6</td>
                                <td><input name="hour_6" class="form-control hour-6" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_6 ?? 0}}" min="0" /></td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td><input name="hour_7" class="form-control hour-7" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_7 ?? 0}}" min="0" /></td>
                                <td>8</td>
                                <td><input name="hour_8" class="form-control hour-8" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_8 ?? 0}}" min="0" /></td>
                                <td>9</td>
                                <td><input name="hour_9" class="form-control hour-9" style="min-width: 80px !important;"
                                        type="number" value="{{$feeder->Bar->hour_9 ?? 0}}" min="0" /></td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td><input name="hour_10" class="form-control hour-10"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_10 ?? 0}}" min="0" /></td>
                                <td>11</td>
                                <td><input name="hour_11" class="form-control hour-11"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_11 ?? 0}}" min="0" /></td>
                                <td>12</td>
                                <td><input name="hour_12" class="form-control hour-12"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_12 ?? 0}}" min="0" /></td>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td><input name="hour_13" class="form-control hour-13"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_13 ?? 0}}" min="0" /></td>
                                <td>14</td>
                                <td><input name="hour_14" class="form-control hour-14"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_14 ?? 0}}" min="0" /></td>
                                <td>15</td>
                                <td><input name="hour_15" class="form-control hour-15"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_15 ?? 0}}" min="0" /></td>
                            </tr>
                            <tr>
                                <td>16</td>
                                <td><input name="hour_16" class="form-control hour-16"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_16 ?? 0}}" min="0" /></td>
                                <td>17</td>
                                <td><input name="hour_17" class="form-control hour-17"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_17 ?? 0}}" min="0" /></td>
                                <td>18</td>
                                <td><input name="hour_18" class="form-control hour-18"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_18 ?? 0}}" min="0" /></td>
                            </tr>
                            <tr>
                                <td>19</td>
                                <td><input name="hour_19" class="form-control hour-19"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_19 ?? 0}}" min="0" /></td>
                                <td>20</td>
                                <td><input name="hour_20" class="form-control hour-20"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_20 ?? 0}}" min="0" /></td>
                                <td>21</td>
                                <td><input name="hour_21" class="form-control hour-21"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_21 ?? 0}}" min="0" /></td>
                            </tr>
                            <tr>
                                <td>22</td>
                                <td><input name="hour_22" class="form-control hour-22"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_22 ?? 0}}" min="0" /></td>
                                <td>23</td>
                                <td><input name="hour_23" class="form-control hour-23"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_23 ?? 0}}" min="0" /></td>
                                <td>24</td>
                                <td><input name="hour_24" class="form-control hour-24"
                                        style="min-width: 80px !important;" type="number"
                                        value="{{$feeder->Bar->hour_24 ?? 0}}" min="0" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>


                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success mx-1">تایید</button>
                    <button type="button" class="btn btn-danger mx-1" data-dismiss="modal">بستن</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endforeach
@foreach($feeders['data'] ?? $feeders as $feeder)

<div class="modal" id="Limit-Modal-{{$loop->index}}">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form action="{{url('/panel/feedersStatus/setFeederLimitedPartners')}}" method="post">
                @csrf
                <input type="hidden" name="feeder_id" value="{{$feeder->id ?? -1}}">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h2 class="modal-title">مشترکین با محدودیت قطع</h2>
                </div>

                <!-- Modal body -->
                <div class="modal-body table-responsive">
                    <h4 class="text-info text-center my-3"><span class="bg-dark rounded p-2"> نام فیدر:
                            {{$feeder['feeder_name'] ?? $feeder->name}}</span>
                    </h4>
                    <div class="row mt-5">
                        <div class="col-md-1" style="text-align: center">
                            <button type="button" class="btn btn-primary add-input" data-mid="{{ $loop->index }}"><i
                                    class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-md-10 inputs-container-{{ $loop->index }}">
                            @if(isset($feeder) and isset($feeder->LimitedPartners) and $feeder->LimitedPartners->isEmpty())
                            <input class="form-control" style="margin-bottom: 8px" type="text" name="names[]" placeholder="نام مشترک را وارد کنید">
                            @elseif(isset($feeder) and isset($feeder->LimitedPartners))
                            @foreach ($feeder->LimitedPartners as $lm)
                            <input class="form-control" style="margin-bottom: 8px" type="text" name="names[]" value="{{ $lm->limited_partner }}" placeholder="نام مشترک را وارد کنید">
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>


                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success mx-1">تایید</button>
                    <button type="button" class="btn btn-danger mx-1" data-dismiss="modal">بستن</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endforeach
<style>
    select {

        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
        text-overflow: '';
    }

    select::-ms-expand {
        /*background: white;*/
        display: none;
    }
</style>
@endsection

@section('scripts')
<script>
    @isset($date)
        let new_date = persianJs($('.date-input').val()).englishNumber().toString()
        $('.date-input').val(new_date.replace('-', '/').replace('-', '/'));
        @endisset
        @if(!isset($date))
        $('.dt-picker').persianDatepicker({
            observer: true,
            format: 'YYYY/MM/DD',
        });
        @endif
        function copy_first_row(t) {
            let first_row = $(t).closest('td').next('td');
            var val = first_row.find('input').val();
            let tbody = first_row.closest('tr').closest('tbody');
            tbody.find('input[type=number]').each(function(){
                $(this).val(val);
            });
        }
        $(function () {

            var options = {
                "language": {
                    "paginate": {
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                    "sSearch": "جست و جو : ",

                },
                "scrollY": 480,
                "scrollX": true,
                "info": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "autoWidth": false,
                // "processing": true,
                // "colReorder": {
                //     order: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
                // ,}
            };
            const table = $("#myTable").DataTable(options);
            function input_checks() {
                $('.break-input').each(function () {
                    var id = $(this).data('id');
                    var my_selector = '.break-type[data-id=' + id + ']';
                    var tm_picker_div = '.tm-limit-div[data-id=' + id + ']';
                    var tm_picker = '.tm-limit[data-id=' + id + ']';
                    var description_selector = '.break-description[data-id=' + id + ']';
                    var both_description_selector = '.both-break-description[data-id=' + id + ']';

                    if ($(this).val() == 1) {
                        $(my_selector).removeProp('readonly');
                        $(my_selector).val('no_limit');
                        $(my_selector).removeClass('unclickable');

                        $('span[data-id=' + id + ']').html('دارد');
                    } else {
                        $(my_selector).prop('readonly', true);
                        $(my_selector).addClass('unclickable');
                        $(my_selector).val('none');
                        var limit_type_selector = '.bar-type[data-id=' + id + ']';
                        $(limit_type_selector).val('system');

                        $(description_selector).removeClass('non-disp');
                        $(tm_picker_div).addClass('non-disp');
                        $(tm_picker).removeClass('tm-active');
                        $(description_selector).prop('placeholder', '');
                        $(description_selector).val('');
                        $(description_selector).prop('readonly', true);
                        $(both_description_selector).addClass('non-disp');

                        $('span[data-id=' + id + ']').html('ندارد');
                    }
                });
            }
            $('#myTable').on('draw.dt', function () {
                input_checks();
            });

            input_checks();
            $("#myModalTable").DataTable(options);
            $('#mycard-body').removeClass('hide-disp');
            $('.sk-circle').addClass('no-disp');
            $('div.col-sm-12.col-md-6:first').css('display', 'none');

            // $('.dataTables_filter').addClass('pull-left');
            $('.break-input').change(function () {
                    var id = $(this).data('id');
                    var my_selector = '.break-type[data-id=' + id + ']';
                    var tm_picker_div = '.tm-limit-div[data-id=' + id + ']';
                    var tm_picker = '.tm-limit[data-id=' + id + ']';
                    var description_selector = '.break-description[data-id=' + id + ']';
                    var both_description_selector = '.both-break-description[data-id=' + id + ']';

                    if ($(this).val() == 1) {
                        $(my_selector).removeProp('readonly');
                        $(my_selector).val('no_limit');
                        $(my_selector).removeClass('unclickable');
                        $('#breakable-feeders').val(parseInt($('#breakable-feeders').val()) + 1);
                        $('#unbreakable-feeders').val(parseInt($('#unbreakable-feeders').val()) - 1);

                        $('span[data-id=' + id + ']').html('دارد');
                    } else {
                        $(my_selector).prop('readonly', true);
                        $(my_selector).addClass('unclickable');
                        $(my_selector).val('none');
                        var limit_type_selector = '.bar-type[data-id=' + id + ']';
                        $(limit_type_selector).val('system');
                        $('#breakable-feeders').val(parseInt($('#breakable-feeders').val()) - 1);
                        $('#unbreakable-feeders').val(parseInt($('#unbreakable-feeders').val()) + 1);

                        $(description_selector).removeClass('non-disp');
                        $(tm_picker_div).addClass('non-disp');
                        $(tm_picker).removeClass('tm-active');
                        $(description_selector).prop('placeholder', '');
                        $(description_selector).val('');
                        $(description_selector).prop('readonly', true);
                        $(both_description_selector).addClass('non-disp');

                        $('span[data-id=' + id + ']').html('ندارد');
                    }
                });
            $('.break-type').change(function () {
                    var id = $(this).data('id');
                    var limit_type_selector = '.bar-type[data-id=' + id + ']';
                    var description_selector = '.break-description[data-id=' + id + ']';
                    var both_description_selector = '.both-break-description[data-id=' + id + ']';
                    var tm_picker_div = '.tm-limit-div[data-id=' + id + ']';
                    var tm_picker = '.tm-limit[data-id=' + id + ']';
                    if ($(this).val() === 'maneuver') {
                        $(limit_type_selector).val('non_system');
                        $(description_selector).removeClass('tm-picker');
                        $(description_selector).prop('readonly', false);
                        $(description_selector).val('');
                        $(description_selector).prop('placeholder', 'توضیحات مانور');
                        $(description_selector).removeClass('non-disp');
                        $(tm_picker_div).addClass('non-disp');
                        $(tm_picker).removeClass('tm-active');
                        $(both_description_selector).addClass('non-disp');


                    } else if ($(this).val() === 'time_limit') {
                        $(limit_type_selector).val('system');
                        $(description_selector).prop('readonly', false);
                        $(description_selector).addClass('non-disp');
                        $(tm_picker_div).removeClass('non-disp');
                        $(tm_picker).addClass('tm-active');
                        $(both_description_selector).addClass('non-disp');


                    } else if ($(this).val() === 'both') {
                        $(both_description_selector).prop('placeholder', 'توضیحات مانور');
                        $(description_selector).prop('placeholder', '');
                        $(description_selector).prop('readonly', 'true');
                        $(description_selector).removeClass('non-disp');
                        $(tm_picker_div).removeClass('non-disp');
                        $(both_description_selector).removeClass('non-disp');
                        $(tm_picker).addClass('tm-active');
                        $(limit_type_selector).val('non_system');
                    }
                    else {
                        $(both_description_selector).addClass('non-disp');
                        $(limit_type_selector).val('system');
                        $(description_selector).prop('readonly', true);
                        $(description_selector).val('');
                        $(description_selector).prop('placeholder', '');
                        $(description_selector).removeClass('non-disp');
                        $(tm_picker_div).addClass('non-disp');
                        $(tm_picker).removeClass('tm-active');
                    }
                });
            $(".unclickable").click(function (event) {
                    event.preventDefault();
                });
                $(".add-input").click(function (event) {
                    let id = $(this).attr("data-mid");
                    $(".inputs-container-"+ id).append('<input class="form-control limit-inputs" type="text" name="names[]" placeholder="نام مشترک را وارد کنید">');
                });
                $('.des-tooltip').change(function(){
                    $(this).attr('title',$(this).val());
                });
                $('.des-tooltip').each(function() {
                    $(this).attr('title',$(this).val());
                });
            $('.tm-picker').persianDatepicker({
                // initialValue: true,
                // initialValueType: 'persian',
                onlyTimePicker: true,
                timePicker: {
                    second: {
                        enabled: false,
                    }
                },
                onHide: function () {
                    $('.tm-active').change(function () {
                        var id = $(this).data('id');
                        var from = '.tm-limit-from[data-id=' + id + ']';
                        var to = '.tm-limit-to[data-id=' + id + ']';
                        var description_selector = '.break-description[data-id=' + id + ']';
                        var val = $(from).val() + '-' + $(to).val();
                        $(description_selector).val(val);
                        // console.log($(description_selector).val());
                    });
                    $('.tm-active').trigger('change');
                },
                onShow: function () {
                    $('.tm-active').change(function () {
                        var id = $(this).data('id');
                        var from = '.tm-limit-from[data-id=' + id + ']';
                        var to = '.tm-limit-to[data-id=' + id + ']';
                        var description_selector = '.break-description[data-id=' + id + ']';
                        var val = $(from).val() + '-' + $(to).val();
                        $(description_selector).val(val);
                        // console.log($(description_selector).val());
                    });
                    $('.tm-active').trigger('change');

                },
                format: 'H:m',
            });

            $('#my-form').submit(function (e) {
                e.preventDefault();
                $('#mycard-body').addClass('no-disp');
                $('.sk-circle').removeClass('no-disp');
                table.destroy();
                options.paging = false;
                $("#myTable").DataTable(options);
                $('.tmp-dis').prop('disabled', false);
                $('.date-input').val(persianJs($('.date-input').val()).persianNumber());
                $(this).unbind('submit').submit();

            });
        });

</script>
{{--<script>--}}
{{--var inputs = document.querySelectorAll('input');--}}

{{--inputs.forEach(input => {--}}
{{--input.addEventListener('invalid', function(e) {--}}
{{--e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")--}}
{{--})--}}

{{--input.addEventListener('input', function(e) {--}}
{{--e.target.setCustomValidity("");--}}
{{--})--}}
{{--})--}}
{{--</script>--}}
@endsection
