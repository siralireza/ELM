@extends('layout')
@section('path')
    <li class="breadcrumb-item">تعیین پارامتر‌های موثر</li>
@endsection
@section('body')
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">مشخص کردن پارامتر ها</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form role="form" action="{{url('/panel/detParams')}}" method="post">
                @csrf
                <div class="form-group">
                    <label>حد‌فاصل مجاز بین دو خاموشی متوالی (برحسب روز)</label>
                    <div class="row mt-2 mb-2">
                        <input type="number" class="form-control col-md-6" name="days"
                               required placeholder="تعداد روز در حال حاظر {{$days}} میباشد">
                    </div>
                    <div class="form-group">
                        <label>تلورانس بار فیدر (درصد)</label>
                        <div class="row">
                            <input type="number" class="form-control col-md-6" name="tolerance"
                                   required placeholder="درصد کنونی {{$tolerance}} میباشد">
                        </div>

                    </div>
                    <button type="submit" class="btn btn-outline-success">دخیره</button>
                </div>
            </form>
            <hr/>
            <div class="row my-2">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                    <a href="{{url('/panel/detParams/uptodate')}}" class="btn btn-lg btn-primary">بروز رسانی کلی</a>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

@endsection

@section('scripts')
    <script>
        var inputs = document.querySelectorAll('input');

        inputs.forEach(input => {
            input.addEventListener('invalid', function (e) {
                e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")
            })

            input.addEventListener('input', function (e) {
                e.target.setCustomValidity("");
            })
        })
    </script>
@endsection