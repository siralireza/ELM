@extends('layout')
@section('path')
    <li class="breadcrumb-item">ایجاد قالب خاموشی</li>
@endsection
@section('styles')
    <style>
        .non-disp {
            display: none;
        }
        .hid-disp {
            visibility: hidden;
        }
        .limit-inputs {
            margin: 8px 0px;
        }
    </style>
@endsection
@section('body')

    @csrf
    <div class="row mb-5">
        <div class="col-12 mb-2">
            <div class="card">
                <div class="card-header">
                    <form action="{{url('/panel/createOffList/createForm')}}" method="post">
                        @csrf
                        <input type="hidden" name="confirm" value="0">
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از تاریخ :</span>
                                        <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               name="start_date"/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا تاریخ :</span>
                                        <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               name="end_date"/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               name="start_time" value="{{request()->old('start_time')}}" readonly/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6" name="end_time"
                                               value="{{request()->old('end_time')}}" readonly/>
                                    </label>
                                </div>
                            </div>
                            <!-- /.card -->

                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="input-group">
                                        <span class="input-group-text">مدت زمان خاموشی هر فیدر (دقیقه) :</span>
                                        <input type="number" class="form-control" style="min-width: 200px" name="period"
                                               value="30" min="1"
                                               max="60">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary mb-3">تهیه قالب خاموشی</button>
                        </div>
                    </form>
                </div>

                <!-- /.card-header -->
                <div class="card-body" style="direction: rtl">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست جداول خاموشی</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="sk-circle non-disp">
                                    <div class="sk-circle1 sk-child"></div>
                                    <div class="sk-circle2 sk-child"></div>
                                    <div class="sk-circle3 sk-child"></div>
                                    <div class="sk-circle4 sk-child"></div>
                                    <div class="sk-circle5 sk-child"></div>
                                    <div class="sk-circle6 sk-child"></div>
                                    <div class="sk-circle7 sk-child"></div>
                                    <div class="sk-circle8 sk-child"></div>
                                    <div class="sk-circle9 sk-child"></div>
                                    <div class="sk-circle10 sk-child"></div>
                                    <div class="sk-circle11 sk-child"></div>
                                    <div class="sk-circle12 sk-child"></div>
                                </div>
                                <div class="card-body table-responsive div-tab" style="direction: rtl;">
                                    <table class="table table-hover" id="myTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>تاریخ شروع</th>
                                            <th>تاریخ پایان</th>
                                            <th>ساعت شروع</th>
                                            <th>ساعت پایان</th>
                                            <th>وضعیت</th>
                                            {{--                                            <th>توضیحات</th>--}}
                                            <th></th>
                                            <th>مشاهده</th>
                                            <th>ویرایش</th>
                                            <th>خذف</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($off_tables as $ot)
                                            <tr>
                                                <td>{{$loop->index + 1}}</td>
                                                <td>{{en2fa(den2dfa($ot->start_date))}}</td>
                                                <td>{{en2fa(den2dfa($ot->end_date))}}</td>
                                                <td>{{en2fa($ot->start_time)}}</td>
                                                <td>{{en2fa($ot->end_time)}}</td>

                                                <td>
                                                    <span class="badge badge-{{status_class($ot->status)}}">{{persian_status($ot->status)}}</span>
                                                </td>
                                                {{--                                            <td>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ</td>--}}
                                                <td><a id="call-link" data-conf="confirmation"
                                                       data-toggle=confirmation
                                                       data-title="ارسال ممکن چند دقیقه طول بکشد آیا مطمئن هستید؟"
                                                       href="{{url('/panel/off_feeders/call_avanak?off_table_id='.$ot->id)}}"
                                                       class="btn btn-secondary">ارسال
                                                        برای آوانک</a>
                                                </td>
                                                <td><a href="{{url('/panel/createOffList/show/'.$ot->id)}}"
                                                       class="btn btn-primary fa fa-eye  @if($ot->status == 'deactive') unclickable disabled @endif"></a>
                                                </td>
                                                <td><a href="{{url('/panel/createOffList/'.$ot->id)}}"
                                                       class="btn btn-secondary fa fa-edit @if($ot->status == 'applied') unclickable disabled @endif"></a>
                                                </td>
                                                <form action="{{url('/panel/createOffList/'.$ot->id)}}" method="post">
                                                    @method('DELETE')
                                                    @csrf
                                                    <td>
                                                        <button type="submit"
                                                                class="btn btn-danger fa fa-trash-o @if($ot->status == 'applied') unclickable disabled @endif"
                                                                data-toggle="confirmation"
                                                                data-title="آیا مطمئن هستید؟">
                                                        </button>
                                                    </td>
                                                </form>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div><!-- /.row -->

                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <style>
        .pull-left {
            float: left !important;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(':input[readonly]').css({'background-color': '#ffffff'});

        $(function () {
            $(".unclickable").click(function (event) {
                event.preventDefault();
            });
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                btnOkLabel: 'بله',
                btnOkClass: 'btn btn-danger',
                btnCancelLabel: 'خیر',
                btnCancelClass: 'btn btn-success',
                popout: true,
            });
            $("#myTable").DataTable({
                "language": {
                    "paginate": {
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                    "sSearch": "جست و جو : ",

                },
                "scrollY": 300,
                "scrollX": true,
                "info": false,
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "autoWidth": false,
                // "colReorder": {
                //     order: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
                // },

            });
            $('.dataTables_filter').addClass('pull-left');
            $('.dt-picker').persianDatepicker({
                initialValue: true,
                observer: true,
                format: 'YYYY/MM/DD',
            });
            $('.tm-picker').persianDatepicker({
                initialValue: true,
                onlyTimePicker: true,
                timePicker: {
                    second: {
                        enabled: false,
                    },
                    minute: {
                        enabled: true,
                        step: 15,
                    }
                },
                format: 'H:m',
            });
            $('[data-conf=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                btnOkLabel: 'بله',
                btnOkClass: 'btn btn-danger',
                btnCancelLabel: 'خیر',
                btnCancelClass: 'btn btn-success',
                popout: true,
            });
        });

    </script>
    <script>
        var inputs = document.querySelectorAll('input');

        inputs.forEach(input => {
            input.addEventListener('invalid', function (e) {
                e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")
            })

            input.addEventListener('input', function (e) {
                e.target.setCustomValidity("");
            })
        });
        window.onbeforeunload = function(e) {
            $('.sk-circle').removeClass('non-disp');
            $('.div-tab').addClass('non-disp');
        };

    </script>
@endsection
