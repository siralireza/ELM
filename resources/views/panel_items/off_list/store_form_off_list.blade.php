@extends('layout')
@section('path')
    <li class="breadcrumb-item active">ایجاد قالب خاموشی</li>
    <li class="breadcrumb-item">ایجاد جدول خاموشی</li>
@endsection
@section('styles')
    <style>
        .no-border {
            border: none !important;
        }

        .tp-border {
            border-bottom: none !important;
            border-left: none !important;
            border-right: none !important;
        }
    </style>
@endsection
@section('body')
    <form action="{{url('/panel/createOffList')}}" method="post">
        @csrf
        <input type="hidden" name="off_table_id" value="{{$off_table->id}}">
        <div class="row mb-5">
            <div class="col-12 mb-2">
                <div class="card">
                    <div class="card-header">
                        <div class="row mt-2">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از تاریخ :</span>
                                        <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               value="{{$off_table->start_date}}" disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا تاریخ :</span>
                                        <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               value="{{$off_table->end_date}}" disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               value="{{$off_table->start_time}}" disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               value="{{$off_table->end_time}}" disabled/>
                                    </label>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="input-group">
                                        <span class="input-group-text">مدت زمان خاموشی هر فیدر (دقیقه) :</span>
                                        <input type="text" class="form-control" name="period"
                                               value="{{$off_table->period}}" min="1" max="60" disabled>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 mt-2">
                            <h6 class="text-info"><i class="fa fa-dot-circle-o"></i>&nbsp;تمامی فیلد ها به
                                آمپر میباشند</h6>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="direction: rtl">
                        <div class="row mb-5">
                            <div class="col-12">
                                <div class="card">
                                    <!-- /.card-header -->
                                    <div class="card-body table-responsive p-0">
                                        <table id="myTable" class="table table-bordered ">
                                            <thead class="text-center">
                                            <tr>
                                                <th>ردیف</th>
                                                <th>تاریخ</th>
                                                <th>روز</th>
                                                <th>نوع</th>
                                                @foreach($times as $time)
                                                    <th style="white-space: nowrap;direction: ltr">{{$time}}</th>
                                                @endforeach
                                                <th>جمع</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($dates as $dt)
                                                <tr>
                                                    <td class="tp-border"></td>
                                                    <td class="tp-border"></td>
                                                    <td class="tp-border"></td>
                                                    <td style="white-space: nowrap">پیش بینی درخواست توانیر @if(!$loop->index)
                                                            <button type="button" class="btn btn-sm btn-info float-left"
                                                                    onclick="copy_first_row()"> کپی در بقیه
                                                            </button>@endif</td>
                                                    @if(isset($off_items) and !$off_items->isEmpty())
                                                        @foreach($off_items->where('date',$dt) as $oi)
                                                            <td><input type="number"
                                                                       class="form-control predict-tavanir"
                                                                       style="width: 80px !important;"
                                                                       value="{{$oi->predict_tavanir_request}}" min="0"
                                                                       max="10000" name="tavanir_req[]"
                                                                       data-date="{{$dt}}"
                                                                       data-time="{{$oi->start_time}}"/></td>
                                                        @endforeach
                                                    @else
                                                        @foreach($times as $tm)
                                                            <td><input type="number"
                                                                       class="form-control predict-tavanir"
                                                                       style="width: 80px !important;" value="0" min="0"
                                                                       max="10000" name="tavanir_req[]"
                                                                       data-date="{{$dt}}" data-time="{{$tm}}"/></td>
                                                        @endforeach
                                                    @endif

                                                    <td>0</td>

                                                </tr>
                                                <tr>
                                                    <td class="no-border">{{$loop->index +1 }}</td>
                                                    <td class="no-border" style="white-space: nowrap">{{en2fa(den2dfa($dt))}}</td>
                                                    <td class="no-border" style="white-space: nowrap">{{which_day(fa2en($dt))}}</td>
                                                    <td style="white-space: nowrap">مدریریت مصرف</td>
                                                    @php
                                                        $date_index = $loop->index;
                                                        $sum = 0;
                                                    @endphp
                                                    <input type="hidden" name="date[]" value="{{$dt}}"
                                                           style="width: 110px;">
                                                    @if(isset($off_items) and !$off_items->isEmpty())
                                                        @php
                                                            $ind = 0;
                                                        @endphp
                                                        @foreach($off_items->where('date',$dt) as $oi)
                                                            <td class="manage-consumption" data-date="{{$dt}}"
                                                                data-time="{{$oi->start_time}}">
                                                                @php
                                                                    $ind += 1;
                                                                    if ($ind >= count($times))
                                                                        $ind -= count($times);

                                                                    $tm = $times[$ind];
                                                                    $tms = time_for_off_list($tm);
                                                                    $val = 0;
                                                                    if ($tms[0] == $tms[1]){
                                                                        $val = $mc->where('date',$dt)
                                                                        ->pluck('hour_'.$tms[0])->sum();
                                                                        }
                                                                    else
                                                                    {
                                                                        $q = $mc->where('date',$dt);
                                                                        $val1 = $q->pluck('hour_'.$tms[0])->sum();
                                                                        $val2 = $q->pluck('hour_'.$tms[1])->sum();
                                                                        $val = max($val1,$val2);
                                                                    }
                                                                    $sum += $val;
                                                                @endphp
                                                                {{$val}}
                                                            </td>
                                                        @endforeach
                                                    @else
                                                        @foreach($times as $tm)
                                                            <td class="manage-consumption" data-date="{{$dt}}"
                                                                data-time="{{$tm}}">
                                                                @php
                                                                    $tms = time_for_off_list($tm);
                                                                    $val = 0;
                                                                    if ($tms[0] == $tms[1]){
                                                                        $tms[0] = change_0_to_24($tms[0]);
                                                                        $val = $mc->where('date',$dates[$date_index])
                                                                        ->pluck('hour_'.$tms[0])->sum();
                                                                        }
                                                                    else
                                                                    {
                                                                        $q = $mc->where('date',$dates[$date_index]);
                                                                        $tms[0] = change_0_to_24($tms[0]);
                                                                        $val1 = $q->pluck('hour_'.$tms[0])->sum();
                                                                        $val2 = $q->pluck('hour_'.$tms[1])->sum();
                                                                        $val = max($val1,$val2);
                                                                    }
                                                                    $sum += $val;
                                                                @endphp
                                                                {{$val}}
                                                            </td>
                                                        @endforeach
                                                    @endif
                                                    <td>{{$sum}}</td>

                                                </tr>
                                                <tr>

                                                    <td class="no-border"></td>
                                                    <td class="no-border"></td>
                                                    <td class="no-border"></td>
                                                    <td style="white-space: nowrap">مقدار خاموشی مجاز</td>
                                                    @if(isset($off_items) and !$off_items->isEmpty())
                                                        @foreach($off_items->where('date',$dt) as $oi)
                                                            <td class="allowed-off" data-date="{{$dt}}"
                                                                data-time="{{$oi->start_time}}">
                                                                {{$oi->allowed_off_value}}
                                                            </td>
                                                        @endforeach
                                                    @else
                                                        @foreach($times as $tm)
                                                            <td class="allowed-off" data-date="{{$dt}}"
                                                                data-time="{{$tm}}">
                                                                0
                                                            </td>
                                                        @endforeach
                                                    @endif
                                                    <td>0</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                                <div class="text-center mt-5">
                                    <button type="submit" class="btn btn-success">ایجاد لیست خاموشی</button>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    </form>
    <style>
        .min-wd {
            min-width: 80px;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(function () {
            $("#myTable").DataTable({
                "language": {
                    "paginate": {
                        "next": "بعدی",
                        "previous": "قبلی"
                    }
                },
                "scrollY": 500,
                "scrollX": true,
                "info": false,
                "lengthChange": false,
                "searching": false,
                "autoWidth": false,
                "paging": false,
                'ordering': false,

            });
            $('.dt-picker').persianDatepicker({
                observer: true,
                format: 'YYYY/MM/DD',
            });
            $('.tm-picker').persianDatepicker({
                onlyTimePicker: true,
                timePicker: {
                    second: {
                        enabled: false,
                    }
                },
                format: 'H:m',
            });
        });
        $('.predict-tavanir').change(function () {
            var date = $(this).data('date');
            var time = $(this).data('time');
            var predict_tavanir = $(this).val();
            var manager_cosumption = $('.manage-consumption[data-date="' + date + '"][data-time="' + time + '"]').text();
            manager_cosumption = persianJs(manager_cosumption).persianNumber();
            var allowed_off = predict_tavanir - manager_cosumption;
            if (allowed_off <= 0)
                allowed_off = '0';
            $('.allowed-off[data-date="' + date + '"][data-time="' + time + '"]').html(persianJs(allowed_off).toString());
        });

        function CalcSum() {
            $('tr:has(input.predict-tavanir)').each(function () {
                var sum = 0;
                $(this).find('input').each(function () {
                    sum += parseInt($(this).val());
                });
                $(this).find('td:last').html(sum);
            });
            $('tr:has(td.allowed-off)').each(function () {
                var sum = 0;
                $(this).find('td.allowed-off').each(function () {
                    sum += parseInt($(this).html());
                });
                $(this).find('td:last').html(sum);
            });
        }

        function copy_first_row() {
            var flag = true;
            var val = [];

            $('tr:has(input.predict-tavanir)').each(function () {
                if (flag) {
                    let i = 0;
                    $(this).find('input').each(function () {
                        val[i] = parseInt($(this).val());
                        i++;
                    });
                    flag = false;
                } else {
                    let i = 0;
                    $(this).find('input').each(function () {
                        $(this).val(val[i]);
                        i++;
                    });
                }
            });
            $('.predict-tavanir').change();
            CalcSum();
        }

        CalcSum();
        $('.predict-tavanir').change(function () {
            CalcSum();
        });
    </script>
    <script>
        var inputs = document.querySelectorAll('input');

        inputs.forEach(input => {
            input.addEventListener('invalid', function (e) {
                e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")
            })

            input.addEventListener('input', function (e) {
                e.target.setCustomValidity("");
            })
        })
    </script>
@endsection