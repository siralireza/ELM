@extends('layout')
@section('path')
    <li class="breadcrumb-item active">ایجاد قالب خاموشی</li>
    <li class="breadcrumb-item active">ایجاد جدول خاموشی</li>
    <li class="breadcrumb-item">تایید جدول خاموشی</li>
@endsection
@section('styles')
    <style>
        tr > th, tr > td {
            white-space: nowrap !important;
        }

        .no-rl-border {
            border-right: none !important;
            border-left: none !important;
        }

        .no-border {
            border: none;
        }

        .tp-border {
            border-bottom: none !important;
            border-left: none !important;
            border-right: none !important;
        }
    </style>
@endsection
@section('body')
    <form action="{{url('/panel/createOffList/create')}}" method="get">
        @csrf
        <input type="hidden" name="off_table" value="{{$off_table->id}}">
        <div class="row mb-5">
            <div class="col-12 mb-2">
                <div class="card">
                    <div class="card-header">
                        <div class="row mt-2">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از تاریخ :</span>
                                        <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               value="{{$off_table->start_date}}" disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا تاریخ :</span>
                                        <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               value="{{$off_table->end_date}}" disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               value="{{$off_table->start_time}}" disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               value="{{$off_table->end_time}}" disabled/>
                                    </label>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="input-group">
                                        <span class="input-group-text">مدت زمان خاموشی هر فیدر (دقیقه) :</span>
                                        <input type="text" class="form-control" name="period"
                                               value="{{$off_table->period}}" min="1" max="60" disabled>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.card-header -->
                    <div class="card-body" style="direction: rtl">
                        <div class="row mb-5">
                            <div class="col-12">
                                <div class="card">
                                    <!-- /.card-header -->
                                    <div class="card-body table-responsive p-0">
                                        <table class="table table-bordered">
                                            <thead>

                                            <tr>
                                                <th class="no-rl-border"></th>
                                                <th class="no-rl-border"></th>
                                                <th style="border-right: none !important;"></th>
                                                @foreach($times as $tm)
                                                    <th class="no-rl-border"></th>
                                                    <th class="no-rl-border"></th>
                                                    <th class="no-rl-border"
                                                        style="white-space: nowrap;direction: ltr">{{$tm}}</th>
                                                    <th style="border-right: none !important;"></th>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <th>ردیف</th>
                                                <th>تاریخ</th>
                                                <th>روز</th>
                                                @foreach($times as $tm)
                                                    <th>فیدر</th>
                                                    <th>امور</th>
                                                    <th style="white-space: nowrap;">تاریخ آخرین قطع</th>
                                                    <th>بار</th>
                                                @endforeach

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($dates as $dt)
                                                <tr><td class="no-border"></td></tr>
                                                <tr>
                                                    <td class="tp-border">{{$loop->index + 1 }}</td>
                                                    <td class="tp-border">{{en2fa(den2dfa($dt))}}</td>
                                                    <td class="tp-border">{{which_day(fa2en($dt))}}</td>
                                                    @php
                                                        $dt_en = $dates_unformated[$loop->index];
                                                        $max_row = 1;
                                                        $c_row = 0;
                                                    @endphp
                                                    @while($c_row < $max_row)
                                                        @if($c_row != 0)
                                                            <tr>
                                                                <td class="no-border"></td>
                                                                <td class="no-border"></td>
                                                                <td class="no-border"></td>
                                                        @endif
                                                        @foreach($times as $period)
                                                            @php
                                                                $p = str_replace(' ', '', explode('-', $period));
                                                                $start_time = $p[0];
                                                                $end_time = $p[1];
                                                                $hour = change_0_to_24(time_for_off_list($period)[0]);
                                                                $feeders = \App\FeederOffSchedule::where([
                                                                    'date' => $dt,
                                                                    'off_time_start' => $start_time,
                                                                    'off_time_end' => $end_time,
                                                                ])->get();
                                                                $row_count = $feeders->count();
                                                                if ($max_row < $row_count)
                                                                    $max_row = $row_count;
                                                            @endphp
                                                            @if(!$feeders->isEmpty())
                                                                @php
                                                                    $f = $feeders[$c_row] ?? '';
                                                                @endphp
                                                                <td>{{$f->Feeder->name ?? ''}}</td>
                                                                <td>{{$f->Feeder->zone_name ?? ''}}</td>
                                                                <td>{{$f->last_off_time ?? ''}}</td>
                                                                <td>{{$f->Feeder->Bar->{'hour_'.$hour} ?? '' }}</td>
                                                            @else
                                                                <td></td>
                                                                <td></td>
                                                                <td class="text-danger">در این بازه فیدری وجود ندارد</td>
                                                                <td></td>
                                                            @endif
                                                        @endforeach
                                                </tr>

                                                        @php
                                                            $c_row += 1;
                                                        @endphp
                                                    @endwhile

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                                <div class="text-center mt-5">
                                    <a href="{{url('/panel/createOffList/back/'.$off_table->id)}}" class="btn btn-danger">مرحله
                                        قبل</a>
                                    <button type="submit" class="btn btn-success">تایید</button>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    </form>
    <style>
        .min-wd {
            min-width: 80px;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('.dt-picker').persianDatepicker({
                observer: true,
                format: 'YYYY/MM/DD',
            });
            $('.tm-picker').persianDatepicker({
                onlyTimePicker: true,
                timePicker: {
                    second: {
                        enabled: false,
                    }
                },
                format: 'H:m',
            });
        });

    </script>
    <script>
        // var inputs = document.querySelectorAll('input');
        //
        // inputs.forEach(input => {
        //     input.addEventListener('invalid', function(e) {
        //         e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")
        //     })
        //
        //     input.addEventListener('input', function(e) {
        //         e.target.setCustomValidity("");
        //     })
        // })
    </script>
@endsection
