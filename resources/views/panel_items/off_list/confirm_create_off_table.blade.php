@extends('layout')
@section('path')
    <li class="breadcrumb-item">تایید ایجاد قالب خاموشی</li>
    <li class="breadcrumb-item active">ایجاد قالب خاموشی</li>
@endsection
@section('body')
    <div class="card bg-warning">
        <div class="card-body">
            <form action="{{url('/panel/createOffList/createForm')}}" method="post">
                @csrf
                <input type="hidden" name="start_date" value="{{den2dfa(Request()->get('start_date'))}}">
                <input type="hidden" name="end_date" value="{{den2dfa(Request()->get('end_date'))}}">
                <input type="hidden" name="start_time" value="{{Request()->get('start_time')}}">
                <input type="hidden" name="end_time" value="{{Request()->get('end_time')}}">
                <input type="hidden" name="period" value="{{Request()->get('period')}}">
                <input type="hidden" name="confirm" value="1">
                <div class="form-group text-center mt-5">
                    <h3>در بازه انتخاب شده جدول دیگری وجود دارد آیا مایل به ادامه هستید؟</h3>
                </div>
                <div class="my-5" style="text-align: center">
                    <button type="submit" class="btn btn-primary btn-lg mx-2">بله</button>
                    <a href="{{url('/panel/createOffList')}}" class="btn btn-success btn-lg mx-2">خیر</a>
                </div>
            </form>
        </div>
    </div>
@endsection