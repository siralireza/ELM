@extends('layout')
@section('path')
<li class="breadcrumb-item">لیست فیدر ها</li>
@endsection
@section('styles')
<style>
    .non-disp {
        display: none;
    }

    .hid-disp {
        visibility: hidden;
    }
    .limit-inputs {
        margin: 8px 0px;
    }
</style>
@endsection
@section('body')
<form action="{{url('/panel/feedersStatus')}}" id="my-form" method="post">
    @csrf
    <div class="row mb-5">
        <div class="col-12 mb-2">
            <div class="card">
                <div class="card-header">
                    <div class="row mt-2">
                        <div class="col-md-2 mb-3" style="max-width: 210px;min-width: 200px">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary disabled">ویرایش
                                    اطلاعات {{$current_version ?? 0}}<br>
                                    {{ $feeders ? '('.verta_date_formatter(Verta($feeders->first()->created_at)->formatDate()).')' ?? '' : '' }}
                                </button>
                                {{-- <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                </button> --}}
                                {{-- <div class="dropdown-menu dropdown-menu-right" role="menu">
                                    @foreach($versions as $version)
                                    <a class="dropdown-item" href="{{url('/panel/feedersStatus/'.$version)}}">ویرایش
                                        {{$version}}</a>
                                    @endforeach
                                </div> --}}
                            </div>
                        </div>

                        {{-- <div class="col-md-2" style="max-width: 350px;min-width: 350px">
                            <div class="form-group">
                                <label class="input-group row" style="font-size: 1.2rem">
                                    <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تاریخ ثبت ویرایش :</span>
                                    <input type="text" class="form-control col-lg-4 col-md-6 col-sm-6 date-input
                                               @if (isset($date))
                                                " value="{{$date}}" @else dt-picker" @endif name="date" readonly />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3" style="max-width: 350px;min-width: 350px">
                            <div class="form-group">
                                <label class="input-group row" style="font-size: 1.2rem">
                                    <span class="input-group-text col-xl-9 col-lg-8 col-md-10 col-sm-6">تعداد فیدر‌هایی
                                        که امکان قطع دارند :</span>
                                    <input class="form-control col-xl-2 col-lg-3 col-md-2 col-sm-4" disabled
                                        value="{{$breakable_feeders ?? $feeders['total']}}" id="breakable-feeders" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3" style="max-width: 340px;min-width: 350px">
                            <div class="form-group">
                                <label class="input-group row" style="font-size: 1.2rem">
                                    <span class="input-group-text col-xl-10 col-lg-8 col-md-10 col-sm-6">تعداد فیدر هایی
                                        که امکان قطع ندارند :</span>
                                    <input class="form-control col-xl-2 col-lg-3 col-md-2 col-sm-4" disabled
                                        value="{{$unbreakable_feeders ?? 0}}"
                                        id="unbreakable-feeders" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1" style="max-width: 340px;min-width: 340px">
                            <div class="form-group">
                                <label class="input-group row" style="font-size: 1.2rem">
                                    <span class="input-group-text col-xl-5 col-lg-6 col-md-9 col-sm-8">تعداد کل فیدر
                                        :</span>
                                    <input class="form-control col-xl-2 col-lg-3 col-md-3 col-sm-4" disabled
                                        value="{{ $total_feeders??$feeders['total'] }}" id="all-feeders" />
                                </label>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>

                <!-- /.card-header -->
                <div class="card-body hide-disp" style="direction:rtl" id="mycard-body">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ردیف</th>
                                <th>امور</th>
                                <th>پست فوق‌ توزیع</th>
                                <th>فیدر</th>
                                <th>مسیر تغذیه</th>
                                <th>آدرس</th>
                                {{-- <th>امکان قطع</th>
                                <th>نوع قطع</th>
                                <th>محدودیت</th>
                                <th>نحوه بارگیری</th> --}}
                                <th>توضیحات</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($feeders['data'] ?? $feeders as $feeder)
                            <tr @if($feeder->is_deleted ?? 0) class="bg-danger" @endif>
                                <input type="hidden" name="feeder_id[]" value="{{$feeder->id ?? $loop->index}}" />
                                <input type="hidden" name="global_feeder_id[]"
                                    value="{{$feeder->global_id ?? $feeder['feeder_id'] ?? ''}}" />
                                <input type="hidden" name="zone_name[]"
                                    value="{{$feeder['zone_name'] ?? $feeder->zone_name ?? ''}}" />
                                <input type="hidden" name="zone_id[]"
                                    value="{{$feeder['zone_id'] ?? $feeder->zone_id ?? ''}}" />
                                <input type="hidden" name="post[]"
                                    value="{{$feeder['post_name'] ?? $feeder->post ?? ''}}" />
                                <input type="hidden" name="name[]"
                                    value="{{$feeder['feeder_name'] ?? $feeder->name ?? ''}}" />
                                <input type="hidden" name="path[]" value="{{$feeder['path'] ?? $feeder->path ?? ''}}" />
                                {{-- <input type="hidden" name="address[]"
                                    value="{{$feeder['address'] == null ?? $feeder->address ?? ''}}" /> --}}

                                <th>{{$loop->index +1 }}</th>
                                <th>{{en2fa($feeder['zone_name']) ?? en2fa($feeder->zone_name)}}</th>
                                <th>{{$feeder['post_name'] ?? $feeder->post}}</th>
                                <th>{{$feeder['feeder_name'] ?? $feeder->name}}</th>

                                {{--<th>{{$feeder['feeder_bar'] ?? $feeder->name}}</th>--}}
                                <th>{{$feeder['path'] ?? $feeder->path}}</th>
                                <th>{{ $feeder->address ?? '' }}</th>


                                <th>{{ $feeder->description ?? ''}}</th>
                                <th>
                                    <button type="button"
                                        class="btn btn-sm btn-warning mb-3 @if(!isset($feeder->id)) disabled" disabled
                                        @else " @endif data-toggle="modal"
                                        data-target="#Cont-Modal-{{$loop->index}}">مشخص کردن مشترکین همکار
                                        @if($feeder->Contributers->isEmpty())
                                        <br>
                                        <span class="badge badge-danger">ندارد</span>
                                        @endif
                                    </button>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="text-center">
                        <button type="submit" class="btn btn-success">تایید تغییرات جهت ایجاد ویرایش جدید</button>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
</form>
@foreach($feeders['data'] ?? $feeders as $feeder)

<div class="modal" id="Cont-Modal-{{$loop->index}}">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form action="{{url('/panel/contributers/setFeederContributers')}}" method="post">
                @csrf
                <input type="hidden" name="feeder_id" value="{{$feeder->id ?? -1}}">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h2 class="modal-title">مشترکین همکار</h2>
                </div>

                <!-- Modal body -->
                <div class="modal-body table-responsive">
                    <h4 class="text-info text-center my-3"><span class="bg-dark rounded p-2"> نام فیدر:
                            {{$feeder['feeder_name'] ?? $feeder->name}}</span>
                    </h4>
                    <div class="row mt-5">
                        <div class="col-md-1" style="text-align: center">
                            <button type="button" class="btn btn-primary add-input" data-mid="{{ $loop->index }}"><i
                                    class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-md-10 inputs-container-{{ $loop->index }}">
                            @if(isset($feeder) and $feeder->Contributers->isEmpty())
                            <input class="form-control" style="margin-bottom: 8px" type="text" name="names[]" placeholder="نام مشترک را وارد کنید">
                            @else
                            @foreach ($feeder->Contributers as $lm)
                            <input class="form-control" style="margin-bottom: 8px" type="text" name="names[]" value="{{ $lm->contributer }}" placeholder="نام مشترک را وارد کنید">
                            @endforeach
                            @endisset
                        </div>
                    </div>
                </div>


                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success mx-1">تایید</button>
                    <button type="button" class="btn btn-danger mx-1" data-dismiss="modal">بستن</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endforeach
<style>
    select {

        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
        text-overflow: '';
    }

    select::-ms-expand {
        /*background: white;*/
        display: none;
    }
</style>
@endsection

@section('scripts')
<script>
    // @isset($date)
    //     let new_date = persianJs($('.date-input').val()).englishNumber().toString()
    //     $('.date-input').val(new_date.replace('-', '/').replace('-', '/'));
    //     @endisset
    //     @if(!isset($date))
    //     $('.dt-picker').persianDatepicker({
    //         observer: true,
    //         format: 'YYYY/MM/DD',
    //     });
    //     @endif
    //     function copy_first_row(t) {
    //         let first_row = $(t).closest('td').next('td');
    //         var val = first_row.find('input').val();
    //         let tbody = first_row.closest('tr').closest('tbody');
    //         tbody.find('input[type=number]').each(function(){
    //             $(this).val(val);
    //         });
    //     }
        $(function () {

            var options = {
                "language": {
                    "paginate": {
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                    "sSearch": "جست و جو : ",

                },
                "scrollY": 480,
                "scrollX": true,
                "info": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "autoWidth": false,
                // "processing": true,
                // "colReorder": {
                //     order: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
                // ,}
            };
            const table = $("#myTable").DataTable(options);
            // function input_checks() {
            //     $('.break-input').each(function () {
            //         var id = $(this).data('id');
            //         var my_selector = '.break-type[data-id=' + id + ']';
            //         var tm_picker_div = '.tm-limit-div[data-id=' + id + ']';
            //         var tm_picker = '.tm-limit[data-id=' + id + ']';
            //         var description_selector = '.break-description[data-id=' + id + ']';
            //         var both_description_selector = '.both-break-description[data-id=' + id + ']';

            //         if ($(this).val() == 1) {
            //             $(my_selector).removeProp('readonly');
            //             $(my_selector).val('no_limit');
            //             $(my_selector).removeClass('unclickable');

            //             $('span[data-id=' + id + ']').html('دارد');
            //         } else {
            //             $(my_selector).prop('readonly', true);
            //             $(my_selector).addClass('unclickable');
            //             $(my_selector).val('none');
            //             var limit_type_selector = '.bar-type[data-id=' + id + ']';
            //             $(limit_type_selector).val('system');

            //             $(description_selector).removeClass('non-disp');
            //             $(tm_picker_div).addClass('non-disp');
            //             $(tm_picker).removeClass('tm-active');
            //             $(description_selector).prop('placeholder', '');
            //             $(description_selector).val('');
            //             $(description_selector).prop('readonly', true);
            //             $(both_description_selector).addClass('non-disp');

            //             $('span[data-id=' + id + ']').html('ندارد');
            //         }
            //     });
            // }
            // $('#myTable').on('draw.dt', function () {
            //     // input_checks();
            // });

            // input_checks();
            $("#myModalTable").DataTable(options);
            $('#mycard-body').removeClass('hide-disp');
            $('.sk-circle').addClass('no-disp');
            $('div.col-sm-12.col-md-6:first').css('display', 'none');

            // $('.dataTables_filter').addClass('pull-left');
            // $('.break-input').change(function () {
            //         var id = $(this).data('id');
            //         var my_selector = '.break-type[data-id=' + id + ']';
            //         var tm_picker_div = '.tm-limit-div[data-id=' + id + ']';
            //         var tm_picker = '.tm-limit[data-id=' + id + ']';
            //         var description_selector = '.break-description[data-id=' + id + ']';
            //         var both_description_selector = '.both-break-description[data-id=' + id + ']';

            //         if ($(this).val() == 1) {
            //             $(my_selector).removeProp('readonly');
            //             $(my_selector).val('no_limit');
            //             $(my_selector).removeClass('unclickable');
            //             $('#breakable-feeders').val(parseInt($('#breakable-feeders').val()) + 1);
            //             $('#unbreakable-feeders').val(parseInt($('#unbreakable-feeders').val()) - 1);

            //             $('span[data-id=' + id + ']').html('دارد');
            //         } else {
            //             $(my_selector).prop('readonly', true);
            //             $(my_selector).addClass('unclickable');
            //             $(my_selector).val('none');
            //             var limit_type_selector = '.bar-type[data-id=' + id + ']';
            //             $(limit_type_selector).val('system');
            //             $('#breakable-feeders').val(parseInt($('#breakable-feeders').val()) - 1);
            //             $('#unbreakable-feeders').val(parseInt($('#unbreakable-feeders').val()) + 1);

            //             $(description_selector).removeClass('non-disp');
            //             $(tm_picker_div).addClass('non-disp');
            //             $(tm_picker).removeClass('tm-active');
            //             $(description_selector).prop('placeholder', '');
            //             $(description_selector).val('');
            //             $(description_selector).prop('readonly', true);
            //             $(both_description_selector).addClass('non-disp');

            //             $('span[data-id=' + id + ']').html('ندارد');
            //         }
            //     });
            // $('.break-type').change(function () {
            //         var id = $(this).data('id');
            //         var limit_type_selector = '.bar-type[data-id=' + id + ']';
            //         var description_selector = '.break-description[data-id=' + id + ']';
            //         var both_description_selector = '.both-break-description[data-id=' + id + ']';
            //         var tm_picker_div = '.tm-limit-div[data-id=' + id + ']';
            //         var tm_picker = '.tm-limit[data-id=' + id + ']';
            //         if ($(this).val() === 'maneuver') {
            //             $(limit_type_selector).val('non_system');
            //             $(description_selector).removeClass('tm-picker');
            //             $(description_selector).prop('readonly', false);
            //             $(description_selector).val('');
            //             $(description_selector).prop('placeholder', 'توضیحات مانور');
            //             $(description_selector).removeClass('non-disp');
            //             $(tm_picker_div).addClass('non-disp');
            //             $(tm_picker).removeClass('tm-active');
            //             $(both_description_selector).addClass('non-disp');


            //         } else if ($(this).val() === 'time_limit') {
            //             $(limit_type_selector).val('system');
            //             $(description_selector).prop('readonly', false);
            //             $(description_selector).addClass('non-disp');
            //             $(tm_picker_div).removeClass('non-disp');
            //             $(tm_picker).addClass('tm-active');
            //             $(both_description_selector).addClass('non-disp');


            //         } else if ($(this).val() === 'both') {
            //             $(both_description_selector).prop('placeholder', 'توضیحات مانور');
            //             $(description_selector).prop('placeholder', '');
            //             $(description_selector).prop('readonly', 'true');
            //             $(description_selector).removeClass('non-disp');
            //             $(tm_picker_div).removeClass('non-disp');
            //             $(both_description_selector).removeClass('non-disp');
            //             $(tm_picker).addClass('tm-active');
            //             $(limit_type_selector).val('non_system');
            //         }
            //         else {
            //             $(both_description_selector).addClass('non-disp');
            //             $(limit_type_selector).val('system');
            //             $(description_selector).prop('readonly', true);
            //             $(description_selector).val('');
            //             $(description_selector).prop('placeholder', '');
            //             $(description_selector).removeClass('non-disp');
            //             $(tm_picker_div).addClass('non-disp');
            //             $(tm_picker).removeClass('tm-active');
            //         }
            //     });
            $(".unclickable").click(function (event) {
                    event.preventDefault();
                });
                $(".add-input").click(function (event) {
                    let id = $(this).attr("data-mid");
                    $(".inputs-container-"+ id).append('<input class="form-control limit-inputs" type="text" name="names[]" placeholder="نام مشترک را وارد کنید">');
                });
                $('.des-tooltip').change(function(){
                    $(this).attr('title',$(this).val());
                });
                $('.des-tooltip').each(function() {
                    $(this).attr('title',$(this).val());
                });
            $('.tm-picker').persianDatepicker({
                // initialValue: true,
                // initialValueType: 'persian',
                onlyTimePicker: true,
                timePicker: {
                    second: {
                        enabled: false,
                    }
                },
                onHide: function () {
                    $('.tm-active').change(function () {
                        var id = $(this).data('id');
                        var from = '.tm-limit-from[data-id=' + id + ']';
                        var to = '.tm-limit-to[data-id=' + id + ']';
                        var description_selector = '.break-description[data-id=' + id + ']';
                        var val = $(from).val() + '-' + $(to).val();
                        $(description_selector).val(val);
                        // console.log($(description_selector).val());
                    });
                    $('.tm-active').trigger('change');
                },
                onShow: function () {
                    $('.tm-active').change(function () {
                        var id = $(this).data('id');
                        var from = '.tm-limit-from[data-id=' + id + ']';
                        var to = '.tm-limit-to[data-id=' + id + ']';
                        var description_selector = '.break-description[data-id=' + id + ']';
                        var val = $(from).val() + '-' + $(to).val();
                        $(description_selector).val(val);
                        // console.log($(description_selector).val());
                    });
                    $('.tm-active').trigger('change');

                },
                format: 'H:m',
            });

            $('#my-form').submit(function (e) {
                e.preventDefault();
                $('#mycard-body').addClass('no-disp');
                $('.sk-circle').removeClass('no-disp');
                table.destroy();
                options.paging = false;
                $("#myTable").DataTable(options);
                $('.tmp-dis').prop('disabled', false);
                $('.date-input').val(persianJs($('.date-input').val()).persianNumber());
                $(this).unbind('submit').submit();

            });
        });

</script>
{{--<script>--}}
{{--var inputs = document.querySelectorAll('input');--}}

{{--inputs.forEach(input => {--}}
{{--input.addEventListener('invalid', function(e) {--}}
{{--e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")--}}
{{--})--}}

{{--input.addEventListener('input', function(e) {--}}
{{--e.target.setCustomValidity("");--}}
{{--})--}}
{{--})--}}
{{--</script>--}}
@endsection
