@extends('layout')
@section('path')
    <li class="breadcrumb-item">تعیین پارامتر‌های موثر</li>
@endsection
@section('body')

    <div class="row mb-5">
        <div class="col-12 mb-2">
            <div class="card">
                <div class="card-header">
                    <form action="{{url('/panel/barManageConsumption/0')}}" method="get">
                        @csrf
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از تاریخ :</span>
                                        <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               name="start_date"  value="{{$start_date ?? 0 ? verta_date_formatter(den2dfa($start_date)) : ''}}" readonly/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا تاریخ :</span>
                                        <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               name="end_date" value="{{$end_date ?? 0 ? verta_date_formatter(den2dfa($end_date)) : ''}}" readonly/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6"
                                               name="start_time" value="{{request()->old('start_time') ?? '۱'}}" readonly/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="input-group row" style="font-size: 1.2rem">
                                        <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا ساعت :</span>
                                        <input class="tm-picker form-control col-lg-5 col-md-6 col-sm-6" name="end_time"
                                               value="{{request()->old('end_time') ?? '۲۴'}}" readonly/>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3 p-0">
                                <button type="submit" class="btn btn-outline-info mb-3">مشاهده اطلاعات</button>
                                <a href="" class="btn btn-outline-info mb-3" id="mybtn">ثبت</a>
                            </div>
                            <div class="col-md-4 mb-3 mt-2">
                                <h6 class="text-info"><i class="fa fa-dot-circle-o"></i>&nbsp;تمامی فیلد ها به
                                    مگاوات میباشند</h6>
                            </div>
                            <!-- /.card -->
                        </div>
                    </form>
                </div>
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>

                <!-- /.card-header -->
                <div class="card-body hide-disp" id="mycard-body">
                    <div>
                        <table id="myTable" class="table table-bordered table-striped" style="direction: ltr">
                            <thead>
                            <tr>
                                <th></th>
                                <th>ردیف</th>
                                <th>تاریخ</th>
                                <th>روز</th>
                                <th>طرح</th>
                                <th>1</th>
                                <th>2</th>
                                <th>3</th>
                                <th>4</th>
                                <th>5</th>
                                <th>6</th>
                                <th>7</th>
                                <th>8</th>
                                <th>9</th>
                                <th>10</th>
                                <th>11</th>
                                <th>12</th>
                                <th>13</th>
                                <th>14</th>
                                <th>15</th>
                                <th>16</th>
                                <th>17</th>
                                <th>18</th>
                                <th>19</th>
                                <th>20</th>
                                <th>21</th>
                                <th>22</th>
                                <th>23</th>
                                <th>24</th>

                            </tr>
                            </thead>
                            <tbody>
                            @isset($dates)
                                @foreach($dates as $dt)
                                    <tr child="{{$loop->index +1 }}" data-date="{{den2dfa($dt)}}">
                                        <td @if($exist_dates->contains($dt)) class="details-control"><i
                                                    class="fa fa-plus"></i> @else > @endif </td>
                                        <td>{{$loop->index +1 }}</td>
                                        <td>{{verta_date_formatter(den2dfa($dt))}}</td>
                                        <td style="white-space: nowrap">{{which_day($dt)}}</td>
                                        <td style="white-space: nowrap;">@if(!$exist_dates->contains($dt)) داده‌ای‌ ثبت‌
                                            نشده‌ است @endif</td>
                                        @php($mg = $items->where('date',$dt))
                                        @if($exist_dates->contains($dt))
                                            <td>{{$mg->sum('hour_1') ? $mg->sum('hour_1')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_2') ? $mg->sum('hour_2')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_3') ? $mg->sum('hour_3')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_4') ? $mg->sum('hour_4')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_5') ? $mg->sum('hour_5')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_6') ? $mg->sum('hour_6')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_7') ? $mg->sum('hour_7')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_8') ? $mg->sum('hour_8')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_9') ? $mg->sum('hour_9')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_10') ? $mg->sum('hour_10')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_11') ? $mg->sum('hour_11')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_12') ? $mg->sum('hour_12')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_13') ? $mg->sum('hour_13')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_14') ? $mg->sum('hour_14')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_15') ? $mg->sum('hour_15')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_16') ? $mg->sum('hour_16')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_17') ? $mg->sum('hour_17')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_18') ? $mg->sum('hour_18')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_19') ? $mg->sum('hour_19')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_20') ? $mg->sum('hour_20')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_21') ? $mg->sum('hour_21')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_22') ? $mg->sum('hour_22')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_23') ? $mg->sum('hour_23')/32 : 0}}</td>
                                            <td>{{$mg->sum('hour_24') ? $mg->sum('hour_24')/32 : 0}}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endisset
                            </tbody>
                        </table>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    </div>


    <style>
        td.details-control {
            cursor: pointer;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(':input[readonly]').css({'background-color': '#ffffff'});

        $(':input[readonly]').css({'background-color':'#ffffff'});
        $(function () {
            function format(header_format, date) {
                var resp = 0;
                $.post("{{url('/panel/barManageConsumption/ajax/subInfo')}}", {
                    'date': date,
                    'header_format': header_format.innerText.toString().replace(/\t/g, '-'),
                    '_token': '{{csrf_token()}}'
                }, function (data) {
                    var resp = data;
                    // console.log(data);
                }, false);
                // console.log('my response');
                // console.log(resp);
                // var data = [['0', '0', '0', '0',
                //     '0', '0', '0', '0', '0', '0', '0',
                //     '0', '0', '0', '0', '0', '0', '0',
                //     '0', '0', '0', '0', '0', '1', 'صنایع', '', '', '', ''],
                //     ['0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '1', 'کشاورزی', '', '', '', ''],
                //     ['0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '1', 'دیزل', '', '', '', ''],
                //     ['0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '1', 'ادارات', '', '', '', ''],
                //     ['0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '1', 'تجاری', '', '', '', ''],
                //     ['0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '0', '0',
                //         '0', '0', '0', '0', '0', '1', 'CNG', '', '', '', ''],
                // ];
                // console.log(data);
                return resp;
            }

            var table = $("#myTable").DataTable({
                "language": {
                    "paginate": {
                        "next": "بعدی",
                        "previous": "قبلی"
                    }
                },
                "scrollY": 500,
                "scrollX": true,
                "info": false,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "autoWidth": false,
                "paging": false,
                "colReorder": {
                    order: [28, 27, 26, 25, 24, 23, 22, 21, 20,
                        19, 18, 17, 16, 15, 14, 13, 12, 11, 10,
                        9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
                },

            });
            $('#mycard-body').removeClass('hide-disp');
            $('.sk-circle').addClass('no-disp');
            $('#myTable tbody').on('click', 'td.details-control', function () {
                var tis = $(this);
                var tr = $(this).closest('tr');
                var child_name = tr.attr('child');
                var date = tr.attr('data-date');
                //here
                var tmpRow = null
                if ($(this).find('i').hasClass('fa-minus')) {
                    table.rows('.tmp' + child_name).remove().draw();
                    $(this).find('i').removeClass('fa-minus').addClass('fa-plus');
                } else {
                    var deleted = table.rows($(tr).nextAll()).nodes();
                    table.rows($(tr).nextAll()).remove();

                    $.post("{{url('/panel/barManageConsumption/ajax/subInfo')}}", {
                        'date': date,
                        'header_format': table.table().header().innerText.toString().replace(/\t/g, '-'),
                        '_token': '{{csrf_token()}}'
                    }, function (data) {
                        tmpRow = table.rows.add(data).draw().nodes();
                        $(tmpRow).addClass('tmp' + child_name);
                        tis.find('i').removeClass('fa-plus').addClass('fa-minus');
                        table.rows.add(deleted).draw();
                    }, false);

                }
            });

            $('.dt-picker').persianDatepicker({
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: true,
                initialValueType: 'persian',
            });
            $('.tm-picker').persianDatepicker({
                initialValue: false,
                onlyTimePicker: true,
                timePicker: {
                    second: {
                        enabled: false,
                    },
                    minute: {
                        enabled: false,
                        // step: 15,
                    }
                },
                format: 'H',
            });
            $('#mybtn').click(function (e) {
                e.preventDefault();
                let start_date = persianJs($('input[name="start_date"]').val()).persianNumber().toString();
                let end_date = persianJs($('input[name="end_date"]').val()).persianNumber().toString();
                let start_time = persianJs($('input[name="start_time"]').val()).persianNumber().toString();
                let end_time = persianJs($('input[name="end_time"]').val()).persianNumber().toString();
                window.location.href = "{{url('/panel/barManageConsumption/create?start_date=')}}" + start_date + '&end_date=' + end_date + '&start_time=' + start_time + '&end_time=' + end_time;
            });
        });

    </script>
    {{--<script>--}}
    {{--var inputs = document.querySelectorAll('input');--}}

    {{--inputs.forEach(input => {--}}
    {{--input.addEventListener('invalid', function(e) {--}}
    {{--e.target.setCustomValidity("لطفا عدد مورد نظر را وارد نمایید")--}}
    {{--})--}}

    {{--input.addEventListener('input', function(e) {--}}
    {{--e.target.setCustomValidity("");--}}
    {{--})--}}
    {{--})--}}
    {{--</script>--}}
@endsection