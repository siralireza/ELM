<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::middleware('DateChecker')->group(function () {
    Route::post('off_feeders/address', 'apiController@address');
    Route::post('off_feeders/polygon', 'apiController@polygon');
    Route::post('off_feeders/partners', 'apiController@partners');
    Route::post('off_feeders/pass', 'apiController@pass');
});
//Route::get('off_feeders/call_avanak', 'apiController@call_avanak');
