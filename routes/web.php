<?php

Route::view('', 'layout')->middleware('AuthChecker');

Route::group(['prefix' => 'panel','middleware' => ['AuthChecker', 'DateChecker']],function () {
    Route::get('detParams/uptodate', 'detParamsController@uptodate');
    Route::resource('detParams', 'detParamsController');
    Route::resource('feedersStatus', 'feederStatusController');
    Route::resource('contributers', 'contributersController');
    Route::post('contributers/setFeederContributers', 'contributersController@setFeederContributers');
    Route::post('feedersStatus/setFeederBar', 'feederStatusController@setFeederBar');
    Route::post('feedersStatus/setFeederLimitedPartners', 'feederStatusController@setFeederLimitedPartners');
//    Route::resource('contInfo', 'contributorsInformation');
    Route::resource('barManageConsumption', 'barManageConsumption');
    Route::post('barManageConsumption/ajax/subInfo', 'barManageConsumption@subInfo');
    Route::get('createOffList/createForm', 'createOffListController@index');
    Route::resource('createOffList', 'createOffListController');
    Route::post('createOffList/createForm', 'createOffListController@createForm');
    Route::get('createOffList/back/{table_id}', 'createOffListController@back');
    Route::get('createOffList/show/{table_id}', 'createOffListController@view_off_list');
    Route::resource('viewOffList', 'viewOffListController');
    Route::post('viewOffList/dayInfo/barPeak', 'viewOffListController@barPeak');
    Route::post('viewOffList/dayInfo/tavRealReq', 'viewOffListController@tavRealReq');
    Route::post('viewOffList/dayInfo/realConsumption', 'viewOffListController@realConsumption');
    Route::post('viewOffList/dayInfo/dayPeak', 'viewOffListController@dayPeak');
    Route::post('viewOffList/toggleFeeder/on/{off_sch_id}', 'viewOffListController@feeder_on');
    Route::post('viewOffList/toggleFeeder/off/{off_sch_id}', 'viewOffListController@feeder_off');
    Route::resource('viewMap', 'MapPageController');
    Route::get('off_feeders/call_avanak', 'createOffListController@call_avanak');

});

